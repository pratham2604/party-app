import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';

class FeedListScreen extends StatefulWidget {
  final List<String> images;
  final int initialPage;

  const FeedListScreen({Key key, @required this.images, this.initialPage = 0})
      : super(key: key);
  @override
  _FeedListScreenState createState() => _FeedListScreenState();
}

class _FeedListScreenState extends State<FeedListScreen>
    with TickerProviderStateMixin<FeedListScreen> {
  AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 100));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              if (animationController.value < 1)
                animationController.forward();
              else {
                animationController.reverse();
              }
            },
            child: PhotoViewGallery.builder(
              scrollPhysics: const BouncingScrollPhysics(),
              builder: (BuildContext context, int index) {
                return PhotoViewGalleryPageOptions(
                  imageProvider: NetworkImage(widget.images[index]),
                );
              },
              enableRotation: true,
              itemCount: widget.images.length,
              loadingBuilder: (context, event) => Center(
                child: Container(
                  width: 20.0,
                  height: 20.0,
                  child: CircularProgressIndicator(
                    value: event == null
                        ? 0
                        : event.cumulativeBytesLoaded /
                            event.expectedTotalBytes,
                  ),
                ),
              ),
              pageController: PageController(initialPage: widget.initialPage),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              AppBar(
                backgroundColor: Colors.black38,
              ),
              // AnimatedBuilder(
              //   animation: animationController,
              //   builder: (context, child) {
              //     return SlideTransition(
              //       position: Tween(begin: Offset(0, 1), end: Offset(0, 0))
              //           .animate(animationController),
              //       child: child,
              //     );
              //   },
              //   child: InkWell(
              //     onTap: () {},
              //     child: Container(
              //       width: MediaQuery.of(context).size.width,
              //       color: Colors.black38,
              //       padding: EdgeInsets.all(16),
              //       child: Column(
              //         mainAxisSize: MainAxisSize.min,
              //         children: <Widget>[
              //           Icon(
              //             Icons.share,
              //           ),
              //           SizedBox(
              //             height: 12,
              //           ),
              //           Text(
              //             "Share",
              //             style: TextStyle(fontSize: 16),
              //           )
              //         ],
              //       ),
              //     ),
              //   ),
              // )
            ],
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    animationController?.dispose();
    super.dispose();
  }
}
