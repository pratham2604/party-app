import 'package:flutter/material.dart';
import 'package:party/controllers/feed_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/models/app_response.dart';
import 'package:party/screens/feed/feed_list_screen.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';

class FeedScreen extends StatefulWidget {
  final Function() onPopBack;

  const FeedScreen({Key key, @required this.onPopBack}) : super(key: key);
  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen> {
  // Variables
  bool isLoading;
  List<String> instagramFeed;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    instagramFeed = [];
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      refresh();
    });
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    AppResponse<List<String>> response =
        await FeedController.getInstagramFeed("12");
    setState(() {
      isLoading = false;
    });
    if (response.isSuccess) {
      setState(() {
        instagramFeed = response.data;
      });
    } else {
      UIHelper.showErrorDialog(context, "Sorry there is no feed");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: GradientAppBar(
        title: "Feed",
        showLeading: true,
        onPopBack: widget.onPopBack,
      ),
      body: isLoading
          ? Center(
              child: AppProgressIndicator(),
            )
          : GridView.builder(
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
              itemBuilder: (context, index) => InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => FeedListScreen(
                            images: instagramFeed,
                            initialPage: index,
                          )));
                },
                child: GridTile(
                  child: Container(
                    child: Image.network(
                      instagramFeed[index],
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              itemCount: instagramFeed.length,
              shrinkWrap: true,
            ),
    );
  }
}
