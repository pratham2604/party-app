import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:party/controllers/event_controller.dart';
import 'package:party/helpers/input_helper.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_location.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/event.dart';
import 'package:party/screens/location/location_search_Screen.dart';
import 'package:party/widgets/common/app_gradient_button.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';
import 'package:party/widgets/common/image_input_widget.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';

class EventCreateScreen extends StatefulWidget {
  @override
  _EventCreateScreenState createState() => _EventCreateScreenState();
}

class _EventCreateScreenState extends State<EventCreateScreen> {
  AppLocation location;
  List<String> hashtags = [];
  List<File> files;
  DateTime dateTime;
  Event event;

  @override
  void initState() {
    super.initState();
    hashtags = [];
    controller = TextEditingController();
    event = Event.named();
    files = [];
  }

  TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: GradientAppBar(title: "Create Party", showLeading: true),
        backgroundColor: Colors.black,
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppParams.appPartyBackground),
                  fit: BoxFit.cover)),
          child: SingleChildScrollView(
            child: Center(
              child: Container(
                width: MediaQuery.of(context).size.width - 32,
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                margin: EdgeInsets.symmetric(vertical: 18),
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(8)),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextField(
                      style: TextStyle(fontSize: 14),
                      maxLength: 100,
                      onChanged: (val) {
                        setState(() {
                          event = event.copyWith(Event.named(name: val));
                        });
                      },
                      decoration: InputDecoration(
                          labelText: "Event Title".toUpperCase(),
                          hintText: "eg Yoga Event",
                          hintStyle: TextStyle(fontSize: 12),
                          border: UnderlineInputBorder()),
                    ),
                    TextField(
                      style: TextStyle(fontSize: 14),
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                      onChanged: (val) {
                        setState(() {
                          event = event.copyWith(
                              Event.named(capacity: int.tryParse(val)));
                        });
                      },
                      maxLength: 4,
                      keyboardType: TextInputType.numberWithOptions(),
                      decoration: InputDecoration(
                          labelText: "Event Capacity".toUpperCase(),
                          hintText: "eg 4",
                          hintStyle: TextStyle(fontSize: 12),
                          border: UnderlineInputBorder()),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    ImageInputWidget(onImagesAdded: (val) {
                      setState(() {
                        files = val;
                      });
                    }),
                    SizedBox(
                      height: 12,
                    ),
                    DateTimeField(
                      style: TextStyle(fontSize: 14),
                      format: DateFormat.yMMMMd('en_US').add_jm(),
                      onChanged: (val) {
                        setState(() {
                          dateTime = val;
                        });
                      },
                      decoration: InputDecoration(
                          hintStyle: TextStyle(fontSize: 12),
                          labelText: "Event Date & Time".toUpperCase()),
                      onShowPicker: (context, currentValue) async {
                        DateTime date = await showDatePicker(
                            context: context,
                            firstDate: DateTime.now(),
                            initialDate: currentValue ??
                                DateTime.now().add(Duration(seconds: 10)),
                            lastDate: DateTime(DateTime.now().year + 2));
                        TimeOfDay time = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay(hour: 0, minute: 0));
                        return date.add(
                            Duration(hours: time.hour, minutes: time.minute));
                      },
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    hashtags.length == 0
                        ? Container()
                        : SizedBox(
                            height: 36,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) {
                                String hashtag = hashtags[index];
                                return Padding(
                                  padding: const EdgeInsets.only(right: 8),
                                  child: RaisedButton(
                                    color: Colors.white,
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 8),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(24)),
                                    onPressed: () {
                                      hashtags.remove(hashtag);
                                      setState(() {});
                                    },
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text(
                                          "$hashtag",
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w300),
                                        ),
                                        SizedBox(
                                          width: 6,
                                        ),
                                        Icon(
                                          Icons.cancel,
                                          size: 22,
                                          color: Colors.black,
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              },
                              itemCount: hashtags.length,
                            ),
                          ),
                    TextField(
                      style: TextStyle(fontSize: 14),
                      onSubmitted: (val) {
                        if (hashtags.indexOf(val) == -1) hashtags.add(val);
                        setState(() {});
                        controller.text = '';
                      },
                      textInputAction: TextInputAction.done,
                      controller: controller,
                      decoration: InputDecoration(
                          labelText: "Hashtags".toUpperCase(),
                          hintText: "eg #party",
                          hintStyle: TextStyle(fontSize: 12),
                          border: UnderlineInputBorder()),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    TextField(
                      onTap: () async {
                        AppResponse<AppLocation> response =
                            await Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => LocationSearchScreen()));
                        if (response != null && response.isSuccess) {
                          setState(() {
                            location = response.data;
                            event =
                                event.copyWith(Event.named(location: location));
                          });
                        }
                      },
                      style: TextStyle(fontSize: 14),
                      readOnly: true,
                      controller:
                          TextEditingController(text: location?.fullAddress),
                      decoration: InputDecoration(
                          labelText: "Location".toUpperCase(),
                          hintText: "eg Delhi, India",
                          hintStyle: TextStyle(fontSize: 12),
                          border: UnderlineInputBorder()),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      "Event Detail".toUpperCase(),
                      style: TextStyle(
                        color: Colors.white70,
                        fontSize: 11,
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    TextField(
                      style: TextStyle(fontSize: 14, color: Colors.black),
                      maxLength: 1000,
                      maxLines: 4,
                      onChanged: (val) {
                        setState(() {
                          event = event.copyWith(Event.named(description: val));
                        });
                      },
                      decoration: InputDecoration(
                          fillColor: Colors.white,
                          filled: true,
                          hintStyle: TextStyle(fontSize: 12),
                          border: UnderlineInputBorder()),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    AppGradientButton(
                        label: "Create",
                        isOutline: false,
                        gradient: AppParams.appBtnGradient,
                        labelStyle: TextStyle(fontSize: 15),
                        padding:
                            EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        onTap: () async {
                          String error;
                          if (InputHelper.isBlank(event.name)) {
                            error =
                                InputHelper.getBlankValidation("Event Name");
                          } else if (event.capacity == null ||
                              InputHelper.isBlank("${event.capacity}")) {
                            error = InputHelper.getBlankValidation(
                                "Event Capacity");
                          } else if (event.capacity <= 2) {
                            error = "Capacity must be greater than 2";
                          } else if (hashtags.length == 0) {
                            error = "Please enter at least one hashtag";
                          } else if (dateTime == null) {
                            error = "Please enter valid date & time of event";
                          } else if (InputHelper.isBlank(event.description)) {
                            error = InputHelper.getBlankValidation(
                                "Event Description");
                          } else if (event.location == null) {
                            error = "Please enter event location";
                          } else if (files.length == 0) {
                            error = "Please select at least one image";
                          }
                          if (error != null) {
                            UIHelper.showErrorDialog(context, error);
                            return;
                          }
                          UIHelper.showLoadingDialog(context, "Please wait...");
                          event = event.copyWith(Event.named(
                              hashtags: hashtags, performanceAt: dateTime));
                          AppResponse response =
                              await EventController.postEvent(event, files);
                          Navigator.of(context).pop();
                          if (response.isSuccess) {
                            await UIHelper.showAlertDialog(context, "Success",
                                "You have created an event");
                            Navigator.of(context).pop();
                          } else {
                            UIHelper.showErrorDialog(context, response.error);
                          }
                        })
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
