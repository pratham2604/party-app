import 'package:flutter/material.dart';
import 'package:party/controllers/contact_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/user.dart';
import 'package:party/widgets/event/event_contact_widget.dart';

class EventContactsScreen extends StatefulWidget {
  @override
  _EventContactsScreenState createState() => _EventContactsScreenState();
}

class _EventContactsScreenState extends State<EventContactsScreen> {
  // Variables
  bool isLoading;
  List<User> phoneContacts;
  String query;
  Set<User> contactsInvited;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    phoneContacts = [];
    contactsInvited = Set<User>();
    query = '';
    refresh();
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    AppResponse<List<User>> contactResponse =
        await ContactController.getPhoneContacts();
    isLoading = false;
    if (contactResponse.isSuccess) {
      setState(() {
        phoneContacts = contactResponse.data;
      });
    } else {}
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (contactsInvited.length > 0) {
          bool canPop = await UIHelper.showConfirmDialog(context,
              "You have added ${contactsInvited.length} contact. Do you want to exit?");
          return canPop ?? false;
        } else {
          return true;
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text("Contacts"),
        ),
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 12),
                    child: TextField(
                      textInputAction: TextInputAction.search,
                      onChanged: (val) {
                        setState(() {
                          query = val;
                        });
                      },
                      onSubmitted: (val) {
                        setState(() {
                          query = val;
                        });
                      },
                      decoration: InputDecoration(
                          labelText: "Contacts", hintText: "Search Contacts"),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Expanded(
                      child: ListView.builder(
                    itemBuilder: (context, index) {
                      User contact = filteredContacts[index];
                      return EventContactWidget(
                        onInvited: (contact) {
                          setState(() {
                            contactsInvited.add(contact);
                          });
                        },
                        contact: contact,
                        isAdded: contactsInvited
                                .where((e) => e.mobileNo == contact.mobileNo)
                                .length ==
                            1,
                      );
                    },
                    itemCount: filteredContacts.length,
                  ))
                ],
              ),
        bottomNavigationBar: BottomAppBar(
          child: RaisedButton(
            onPressed: () async {
              Navigator.of(context)
                  .pop(contactsInvited.map((e) => e.userMeta).toList());
            },
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
            child: Text(
              "Invite",
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
      ),
    );
  }

  List<User> get filteredContacts =>
      phoneContacts
          ?.where((element) =>
              element.name.toLowerCase().contains(query.toLowerCase()))
          ?.toList() ??
      [];
}
