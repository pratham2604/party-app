import 'package:flutter/material.dart';
import 'package:party/controllers/event_controller.dart';
import 'package:party/helpers/input_helper.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/event.dart';
import 'package:party/models/review.dart';
import 'package:party/widgets/common/app_gradient_button.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class EventReviewScreen extends StatefulWidget {
  final Event event;

  const EventReviewScreen({Key key, this.event}) : super(key: key);
  @override
  _EventReviewScreenState createState() => _EventReviewScreenState();
}

class _EventReviewScreenState extends State<EventReviewScreen> {
  EventReview eventReview;
  bool isLoading;
  TextEditingController titleController, descriptionController;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    eventReview = EventReview.named();
    titleController = TextEditingController();
    descriptionController = TextEditingController();
    WidgetsBinding.instance.addPostFrameCallback((duration) {
      refresh();
    });
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    AppResponse<EventReview> response =
        await EventController.getReview(widget.event.id);

    if (response.data != null) {
      eventReview = response.data;
      UIHelper.showErrorDialog(
          context, "You have already posted a review for this event");
      titleController.text = eventReview.title;
      descriptionController.text = eventReview.description;
    } else {}
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: GradientAppBar(title: "Write Review", showLeading: true),
        backgroundColor: Colors.black,
        body: isLoading
            ? Center(
                child: AppProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Center(
                  child: Container(
                    width: MediaQuery.of(context).size.width - 32,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 12),
                    margin: EdgeInsets.symmetric(vertical: 18),
                    decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(8)),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Rating".toUpperCase(),
                          style: TextStyle(
                            color: Colors.white70,
                            fontSize: 11,
                          ),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        SmoothStarRating(
                          size: 32,
                          color: Colors.yellow,
                          borderColor: Colors.yellow,
                          onRatingChanged: (rating) {
                            if (isReviewed) {
                              return;
                            }
                            setState(() {
                              eventReview = eventReview
                                  .copyWith(EventReview.named(rating: rating));
                            });
                          },
                          rating: eventReview.rating ?? 0,
                        ),
                        TextField(
                          controller: titleController,
                          style: TextStyle(fontSize: 14),
                          maxLength: 100,
                          onChanged: (val) {
                            setState(() {
                              eventReview = eventReview
                                  .copyWith(EventReview.named(title: val));
                            });
                          },
                          readOnly: isReviewed,
                          decoration: InputDecoration(
                              labelText: "Title".toUpperCase(),
                              hintText: "eg Superb",
                              hintStyle: TextStyle(fontSize: 12),
                              border: UnderlineInputBorder()),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Text(
                          "Description".toUpperCase(),
                          style: TextStyle(
                            color: Colors.white70,
                            fontSize: 11,
                          ),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        TextField(
                          style: TextStyle(fontSize: 14, color: Colors.black),
                          maxLength: 100,
                          controller: descriptionController,
                          maxLines: 4,
                          readOnly: isReviewed,
                          onChanged: (val) {
                            setState(() {
                              eventReview = eventReview.copyWith(
                                  EventReview.named(description: val));
                            });
                          },
                          decoration: InputDecoration(
                              fillColor: Colors.white,
                              filled: true,
                              hintStyle: TextStyle(fontSize: 12),
                              border: UnderlineInputBorder()),
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        Opacity(
                          opacity: isReviewed ? 0.5 : 1,
                          child: AppGradientButton(
                              label: "Create",
                              isOutline: false,
                              gradient: AppParams.appBtnGradient,
                              labelStyle: TextStyle(fontSize: 15),
                              padding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              onTap: isReviewed
                                  ? null
                                  : () async {
                                      String error;
                                      if (eventReview.rating == null) {
                                        error = "Please enter a valid rating";
                                      } else if (InputHelper.isBlank(
                                          eventReview.title)) {
                                        error = InputHelper.getBlankValidation(
                                            "title");
                                      } else if (InputHelper.isBlank(
                                          eventReview.description)) {
                                        error = InputHelper.getBlankValidation(
                                            "description");
                                      }
                                      if (error != null) {
                                        UIHelper.showErrorDialog(
                                            context, error);
                                        return;
                                      }
                                      UIHelper.showLoadingDialog(
                                          context, "Please wait...");
                                      eventReview = eventReview.copyWith(
                                          EventReview.named(
                                              eventId: widget.event.id));
                                      AppResponse<bool> response =
                                          await EventController.postReview(
                                              eventReview);
                                      Navigator.of(context).pop();
                                      if (response.isSuccess) {
                                        await UIHelper.showAlertDialog(context,
                                            "Success", "Review Posted");
                                        Navigator.of(context).pop();
                                      } else {
                                        UIHelper.showErrorDialog(
                                            context, response.error);
                                      }
                                    }),
                        )
                      ],
                    ),
                  ),
                ),
              ));
  }

  bool get isReviewed => eventReview?.id != null;
}
