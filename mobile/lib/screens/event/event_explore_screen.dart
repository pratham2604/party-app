import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:party/controllers/event_controller.dart';
import 'package:party/controllers/location_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/event.dart';
import 'package:party/screens/event/event_create_screen.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/custom_map.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';
import 'package:party/widgets/event/event_background_widget.dart';
import 'package:party/widgets/event/event_widget.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'event_detail_screen.dart';

class EventExploreScreen extends StatefulWidget {
  const EventExploreScreen({Key key}) : super(key: key);
  @override
  _EventExploreScreenState createState() => _EventExploreScreenState();
}

class _EventExploreScreenState extends State<EventExploreScreen> {
  bool isExpanded;
  bool showFab;

  // Variables
  List<Event> nearByEvents;
  bool isLoading;
  bool hasPermission;
  LatLng currentPosition;
  StreamSubscription<AppResponse<List<Event>>> subscription;

  // Map
  CustomMapController mapController;
  PanelController panelController;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    isExpanded = false;
    hasPermission = true;
    showFab = true;
    panelController = PanelController();
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    if (hasPermission) {
      UIHelper.showLoadingDialog(context, "Getting Location...");
      LatLng positon = await LocationController.getOldLocation();
      if (positon == null) {
        AppResponse<LocationData> response =
            await LocationController.getLocation();
        if (response.isSuccess) {
          currentPosition =
              LatLng(response.data.latitude, response.data.longitude);
          LocationController.saveOldLocation(currentPosition);
        } else {
          UIHelper.showErrorDialog(context, "Can not get current location");
        }
      } else {
        currentPosition = positon;
      }
      Navigator.of(context).pop();
    }
    loadEvents(currentPosition);
    mapController.moveToLocation(currentPosition);
  }

  Future<void> loadEvents(LatLng position) async {
    if (subscription != null) {
      subscription.cancel();
    }
    subscription = EventController.getNearByEvents(position).listen((result) {
      if (mounted) {
        if (result.isSuccess)
          setState(() {
            nearByEvents = result.data;
            isLoading = false;
          });
        if (nearByEvents != null &&
            nearByEvents.length > 0 &&
            mapController != null) {
          this.mapController.addData(nearByEvents ?? []);
          this.mapController.addData(nearByEvents.toList());
        }
      }
    });
  }

  Future<void> switchFab() async {
    setState(() {
      showFab = false;
    });
    await Future.delayed(Duration(milliseconds: 25));
    setState(() {
      showFab = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    double minHeight = MediaQuery.of(context).size.height / 3;
    BorderRadius borderRadius = !isExpanded
        ? BorderRadius.only(
            topLeft: Radius.circular(20), topRight: Radius.circular(20))
        : BorderRadius.circular(0);
    return WillPopScope(
      onWillPop: () async {
        if (panelController.isPanelOpen) {
          panelController.close();
          return false;
        }
        bool check =
            await UIHelper.showConfirmDialog(context, "Do you want to exit");
        if (check) {
          exit(0);
        }

        return false;
      },
      child: Scaffold(
          body: SlidingUpPanel(
            onPanelOpened: () {
              if (!isExpanded) switchFab();

              setState(() {
                isExpanded = true;
              });
            },
            onPanelClosed: () {
              if (isExpanded) switchFab();
              setState(() {
                isExpanded = false;
              });
            },
            controller: panelController,
            borderRadius: borderRadius,
            backdropEnabled: false,
            backdropColor: Colors.white,
            boxShadow: [],
            minHeight: minHeight,
            maxHeight: MediaQuery.of(context).size.height,
            body: CustomMap(
                onControllerCompleted: (controller) {
                  this.mapController = controller;
                  refresh();
                },
                onTap: (val) async {}),
            collapsed: ClipRRect(
              borderRadius: borderRadius,
              child: buildExplore(),
            ),
            panel: ClipRRect(
                borderRadius:
                    isExpanded ? BorderRadius.circular(0) : borderRadius,
                child: buildExplore(canScroll: true)),
          ),
          floatingActionButton: showFab
              ? FloatingActionButton(
                  onPressed: () async {
                    if (isExpanded)
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => EventCreateScreen()));
                    else {
                      refresh();
                    }
                  },
                  child: isExpanded
                      ? Image.asset(
                          AppParams.appAddButton,
                          width: 24,
                          height: 24,
                        )
                      : Icon(Icons.my_location),
                  backgroundColor: Colors.white,
                )
              : null),
    );
  }

  Widget buildExplore({bool canScroll = false}) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: GradientAppBar(
          title: "Near Me",
          showLeading: canScroll,
          onPopBack: () {
            panelController.close();
          },
        ),
        body: SizedBox(
          height: viewHeight * 0.8,
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              EventBackgroundWidget(),
              Opacity(
                opacity: 0.85,
                child: Container(
                  decoration: BoxDecoration(color: Colors.black),
                ),
              ),
              isLoading
                  ? Center(
                      child: AppProgressIndicator(),
                    )
                  : ListView.builder(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      itemBuilder: (context, index) => EventWidget(
                        event: nearByEvents[index],
                        onTap: (event) {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  EventDetailScreen(event: event)));
                        },
                        onLongTap: (event) async {
                          String checkCase = await showModalBottomSheet(
                              context: context,
                              builder: (context) => Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      ListTile(
                                        leading: Icon(Icons.my_location),
                                        title: Text("Go to location"),
                                        trailing: Icon(Icons.chevron_right),
                                        onTap: () {
                                          Navigator.of(context).pop("LOCATION");
                                        },
                                      )
                                    ],
                                  ));
                          if (checkCase != null) {
                            if (checkCase == "LOCATION") {
                              mapController
                                  .moveToLocation(event.location.latLng);
                            }
                          }
                        },
                      ),
                      shrinkWrap: true,
                      itemCount: nearByEvents.length,
                      physics: canScroll
                          ? BouncingScrollPhysics()
                          : NeverScrollableScrollPhysics(),
                    ),
            ],
          ),
        ));
  }

  double get viewHeight => MediaQuery.of(context).size.height;

  double get viewWidth => MediaQuery.of(context).size.width;

  @override
  void dispose() {
    super.dispose();
    subscription?.cancel();
  }
}
