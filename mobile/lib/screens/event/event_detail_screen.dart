import 'dart:developer';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:party/controllers/event_controller.dart';
import 'package:party/controllers/user_controller.dart';
import 'package:party/helpers/data_helper.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/event.dart';
import 'package:party/models/event_invite.dart';
import 'package:party/models/user.dart';
import 'package:party/screens/event/event_contacts_screen.dart';
import 'package:party/widgets/common/app_gradient_button.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';
import 'package:party/widgets/event/event_background_widget.dart';
import 'package:party/widgets/event/event_reviews_list_widget.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class EventDetailScreen extends StatefulWidget {
  final Event event;

  const EventDetailScreen({Key key, @required this.event}) : super(key: key);
  @override
  _EventDetailScreenState createState() => _EventDetailScreenState();
}

class _EventDetailScreenState extends State<EventDetailScreen> {
  User currentUser;
  bool isLoading;
  EventInvite eventInvite;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    refresh();
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    await Future.wait([loadUser(), loadInvite()]);
    if (currentUser != null && mounted)
      setState(() {
        isLoading = false;
      });
  }

  Future<void> loadUser() async {
    AppResponse<User> response = await UserController.getCurrentUser();
    if (response.isSuccess) {
      currentUser = response.data;
    } else {
      UIHelper.showErrorDialog(context, "You are not logged in");
    }
  }

  Future<void> loadInvite() async {
    AppResponse<EventInvite> response =
        await EventController.getInvite(widget.event.id);
    if (response.isSuccess) {
      eventInvite = response.data;
    } else {
      eventInvite = EventInvite.named();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        title: "Details",
        showLeading: true,
      ),
      body: Stack(
        children: <Widget>[
          EventBackgroundWidget(),
          SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 8,
                ),
                SizedBox(
                  width: viewWidth,
                  child: CarouselSlider(
                    items: event.images
                        .map(
                          (val) => Image.network(
                            val,
                            fit: BoxFit.cover,
                            width: viewWidth,
                          ),
                        )
                        .toList(),
                    height: viewHeight / 3.5,
                    aspectRatio: 16 / 9,
                    viewportFraction: 1.0,
                    initialPage: 0,
                    enableInfiniteScroll: false,
                    reverse: false,
                    autoPlay: false,
                    autoPlayInterval: Duration(seconds: 3),
                    autoPlayAnimationDuration: Duration(milliseconds: 800),
                    autoPlayCurve: Curves.fastOutSlowIn,
                    pauseAutoPlayOnTouch: Duration(seconds: 10),
                    enlargeCenterPage: true,
                    scrollDirection: Axis.horizontal,
                  ),
                ),
                buildDetails(),
                buildInterestOptions(),
                EventReviewsListWidget(
                  event: widget.event,
                  canReview: (eventInvite?.isGoing ?? false)
                      ? DateTime.now().compareTo(event.performanceAt) <= 0
                      : false,
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  // Build Details
  Widget buildDetails() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 18),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            height: 8,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GradientText(
                event.hashtagText,
                gradient: AppParams.appTxtGradient,
                style: TextStyle(fontWeight: FontWeight.normal, fontSize: 15),
              ),
              SizedBox(
                width: 16,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SmoothStarRating(
                    rating: event.rating,
                    size: 15,
                    color: Colors.yellow.withOpacity(0.8),
                    borderColor: Colors.yellow.withOpacity(0.6),
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Text(
                    "${DataHelper.getShortCount(event.ratingCount)}",
                    style: TextStyle(color: Colors.white54, fontSize: 13),
                  )
                ],
              )
            ],
          ),
          SizedBox(
            height: 12,
          ),
          Row(
            children: <Widget>[
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    "${event.name}",
                    style: TextStyle(
                      color: Colors.white.withOpacity(1),
                      fontSize: 16,
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Text(
                    "${event.location.fullAddress}",
                    style: TextStyle(
                      color: Colors.white.withOpacity(0.7),
                      fontSize: 14,
                    ),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Text(
                    "${event.attendeeCount} / ${event.capacity} attending",
                    style: TextStyle(
                      color: Colors.white.withOpacity(0.7),
                      fontSize: 13,
                    ),
                  ),
                ],
              ))
            ],
          )
        ],
      ),
    );
  }

// Interest Options
  Widget buildInterestOptions() {
    return isLoading
        ? Container(
            height: 100,
            child: Center(
              child: AppProgressIndicator(),
            ))
        : currentUser.id == event.host.id ||
                event.performanceAt.compareTo(DateTime.now()) <= 0
            ? Container()
            : !eventInvite.isGoing
                ? SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Container(
                      width: viewWidth,
                      padding: const EdgeInsets.symmetric(vertical: 12),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Expanded(
                            child: Container(),
                            flex: 1,
                          ),
                          AppGradientButton(
                            label: "Interested",
                            gradient: AppParams.appTxtGradient,
                            labelStyle: TextStyle(fontSize: 16),
                            isOutline: !eventInvite.isInterested,
                            onTap: () async {
                              if (eventInvite.isInterested) {
                                return;
                              }
                              UIHelper.showLoadingDialog(
                                  context, "Sending Invite...");
                              AppResponse appResponse =
                                  await EventController.sendInvite(widget.event,
                                      isInterested: true);
                              Navigator.of(context).pop();
                              if (appResponse.isSuccess) {
                                UIHelper.showAlertDialog(
                                    context, "Success", "Invite Sent");
                                eventInvite =
                                    EventInvite.named(isInterested: true);
                                setState(() {});
                              } else {
                                UIHelper.showErrorDialog(
                                    context, appResponse.error);
                              }
                            },
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          AppGradientButton(
                            label: "Not Interested",
                            gradient: AppParams.appTxtGradient,
                            isOutline: !eventInvite.isNotInterested,
                            labelStyle:
                                TextStyle(fontSize: 16, color: Colors.black),
                            onTap: () async {
                              if (eventInvite.isNotInterested) {
                                return;
                              }
                              UIHelper.showLoadingDialog(
                                  context, "Sending Response...");
                              AppResponse appResponse =
                                  await EventController.sendInvite(widget.event,
                                      isNotInterested: true);
                              Navigator.of(context).pop();
                              if (appResponse.isSuccess) {
                                UIHelper.showAlertDialog(
                                    context, "Success", "Response Sent");
                                eventInvite =
                                    EventInvite.named(isNotInterested: true);
                                setState(() {});
                              } else {
                                UIHelper.showErrorDialog(
                                    context, appResponse.error);
                              }
                            },
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          eventInvite.isNotInterested
                              ? Container()
                              : (eventInvite.friends?.length ?? 0) != 0
                                  ? AppGradientButton(
                                      label: "${eventInvite.friends?.length}",
                                      isOutline: false,
                                      onTap: null,
                                      gradient: AppParams.appBtnGradient,
                                      labelStyle: null,
                                    )
                                  : AppGradientButton(
                                      label: "+1",
                                      gradient: AppParams.appBtnGradient,
                                      labelStyle: null,
                                      padding: EdgeInsets.symmetric(
                                          vertical: 6, horizontal: 8),
                                      onTap: () async {
                                        List<UserMeta> friends =
                                            await Navigator.of(context).push(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        EventContactsScreen()));
                                        if (friends != null) {
                                          UIHelper.showLoadingDialog(
                                              context, "Sending Invite");
                                          AppResponse<bool> inviteResponse =
                                              await EventController.sendInvite(
                                                  event,
                                                  friends: friends,
                                                  isInterested: true);
                                          Navigator.of(context).pop();
                                          if (inviteResponse.isSuccess) {
                                            setState(() {
                                              eventInvite = eventInvite
                                                  .copyWith(EventInvite.named(
                                                      isInterested: true,
                                                      friends: friends));
                                            });
                                          } else {}
                                        }
                                      }),
                          Expanded(
                            child: Container(),
                            flex: 1,
                          )
                        ],
                      ),
                    ),
                  )
                : eventInvite.isGoing
                    ? SizedBox(
                        width: 150,
                        child: AppGradientButton(
                          label: "Going",
                          gradient: AppParams.appTxtGradient,
                          isOutline: false,
                          labelStyle:
                              TextStyle(fontSize: 16, color: Colors.black),
                          onTap: () async {
                            UIHelper.showAlertDialog(context, "Alert",
                                "Your invitation has been accepted by the host.");
                          },
                        ),
                      )
                    : Container();
  }

  // Reviews

  Event get event => widget.event;

  double get viewHeight => MediaQuery.of(context).size.height;

  double get viewWidth => MediaQuery.of(context).size.width;
}
