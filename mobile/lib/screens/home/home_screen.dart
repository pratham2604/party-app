import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:party/controllers/location_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/screens/event/event_explore_screen.dart';
import 'package:party/screens/feed/feed_screen.dart';
import 'package:party/screens/login/location_request_screen.dart';
import 'package:party/screens/profile/profile_screen.dart';
import 'package:party/screens/vendor/vendor_search_screen.dart';
import 'package:party/widgets/common/app_bottom_bar.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  BottomBarState bottomState;
  LatLng currentPosition;

  @override
  void initState() {
    super.initState();
    bottomState = BottomBarState.Event;
    refresh();
  }

  Future<void> refresh() async {
    bool hasPermission = await LocationController.hasPermission();
    if (!hasPermission) {
      bool gotPermission = await Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => LocationRequestScreen()));
      hasPermission = gotPermission ?? false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (bottomState != BottomBarState.Event) {
          setState(() {
            bottomState = BottomBarState.Event;
          });
        } else {
          return true;
        }
        return false;
      },
      child: Scaffold(
        body: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Column(
              children: <Widget>[
                Expanded(
                    child: Stack(
                  children: <Widget>[
                    Offstage(
                      child: TickerMode(
                          enabled: bottomState == BottomBarState.Event,
                          child: EventExploreScreen()),
                      offstage: bottomState != BottomBarState.Event,
                    ),
                    Offstage(
                      child: TickerMode(
                        child: VendorSearchScreen(),
                        enabled: bottomState == BottomBarState.Search,
                      ),
                      offstage: bottomState != BottomBarState.Search,
                    ),
                    Offstage(
                      child: TickerMode(
                        child: VendorSearchScreen(),
                        enabled: bottomState == BottomBarState.Search,
                      ),
                      offstage: bottomState != BottomBarState.Search,
                    ),
                    Offstage(
                      child: TickerMode(
                        child: FeedScreen(
                          onPopBack: () {
                            setState(() {
                              bottomState = BottomBarState.Event;
                            });
                          },
                        ),
                        enabled: bottomState == BottomBarState.Feed,
                      ),
                      offstage: bottomState != BottomBarState.Feed,
                    ),
                    Offstage(
                      child: TickerMode(
                        child: ProfileScreen(
                          onPopBack: () {
                            setState(() {
                              bottomState = BottomBarState.Event;
                            });
                          },
                        ),
                        enabled: bottomState == BottomBarState.Profile,
                      ),
                      offstage: bottomState != BottomBarState.Profile,
                    ),
                  ],
                )),
                Container(
                  color: Colors.black,
                  height: 64,
                ),
              ],
            ),
            AppBottomBar(
              current: bottomState,
              onSelected: (state) {
                if (mounted) {
                  setState(() {
                    bottomState = state;
                  });
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
