import 'dart:developer';
import 'dart:math' as Math;

import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:party/controllers/favourite_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/vendor.dart';
import 'package:party/screens/vendor/vendor_detail_screen.dart';
import 'package:party/screens/vendor/vendor_favourites_search_screen.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/ease_in_widget.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';

class VendorFavouritesScreen extends StatefulWidget {
  @override
  _VendorFavouritesScreenState createState() => _VendorFavouritesScreenState();
}

class _VendorFavouritesScreenState extends State<VendorFavouritesScreen> {
  List<Vendor> vendors;
  bool isLoading;

  @override
  void initState() {
    super.initState();
    vendors = [];
    isLoading = true;
    refresh();
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    AppResponse<List<Vendor>> response =
        await FavouriteController.getVendorFavourites();
    log("Response ${response.data.length}");
    setState(() {
      isLoading = false;
    });
    if (response.isSuccess) {
      setState(() {
        vendors = response.data;
      });
    } else {
      UIHelper.showErrorDialog(context, response.error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: GradientAppBar(
        title: "Favourites",
        showLeading: true,
      ),
      body: isLoading
          ? AppProgressIndicator()
          : GridView.builder(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 16),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, crossAxisSpacing: 1, childAspectRatio: 1),
              itemBuilder: (context, index) => buildVendor(vendors[index]),
              itemCount: vendors.length,
              shrinkWrap: true,
            ),
    );
  }

  Widget buildVendor(Vendor vendor) {
    return EaseInWidget(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => VendorDetailScreen(vendor: vendor)));
      },
      child: GridTile(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Center(
                child: LayoutBuilder(
                  builder: (context, constraints) {
                    double height = constraints.biggest.height;
                    double width = constraints.biggest.width;
                    double size = Math.min(height, width);
                    return Container(
                      decoration: BoxDecoration(
                          gradient: AppParams.appGradient,
                          shape: BoxShape.circle),
                      padding: EdgeInsets.all(2),
                      child: CircleAvatar(
                        radius: (size - 8) / 2,
                        backgroundImage: NetworkImage(
                            vendor.profilePic ?? vendor.defaultPic),
                      ),
                    );
                  },
                ),
              ),
            ),
            SizedBox(
              height: 12,
            ),
            GradientText(
              "${vendor.name}",
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
              textAlign: TextAlign.center,
              gradient: AppParams.appTxtGradient,
            ),
            GradientText(
              "${vendor.location?.fullAddress ?? ""}",
              style: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: 13,
              ),
              textAlign: TextAlign.center,
              gradient: AppParams.appTxtGradient,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildAddOption() {
    return EaseInWidget(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => VendorFavouritesSearchScreen()));
      },
      child: GridTile(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(2),
                child: Center(
                  child: LayoutBuilder(
                    builder: (context, constraints) {
                      double height = constraints.biggest.height;
                      double width = constraints.biggest.width;
                      double size = Math.min(height, width);
                      return CircleAvatar(
                        radius: size / 2,
                        backgroundColor: Colors.white,
                        child: Center(
                          child: Image.asset(
                            AppParams.appAddButton,
                            width: 24,
                            height: 24,
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 12,
            ),
            GradientText(
              "Add more to your\nFavourites list",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
              textAlign: TextAlign.center,
              gradient: AppParams.appTxtGradient,
            ),
          ],
        ),
      ),
    );
  }
}
