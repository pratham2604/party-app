import 'package:flutter/material.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';

class VendorFavouritesSearchScreen extends StatefulWidget {
  @override
  _VendorFavouritesSearchScreenState createState() =>
      _VendorFavouritesSearchScreenState();
}

class _VendorFavouritesSearchScreenState
    extends State<VendorFavouritesSearchScreen> {
  //
  bool isLoading;

  @override
  void initState() {
    super.initState();
    isLoading = true;
  }

  Future<void> refresh() async {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        title: "Search",
        showLeading: true,
      ),
      body: Container(
        color: Colors.black,
      ),
    );
  }
}
