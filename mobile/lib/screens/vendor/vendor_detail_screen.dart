import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/vendor.dart';
import 'package:party/widgets/common/app_gradient_button.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';
import 'package:party/widgets/common/gradient_divider.dart';
import 'package:party/widgets/vendor/vendor_favourite_widget.dart';
import 'package:party/widgets/vendor/vendor_reviews_list_widget.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

class VendorDetailScreen extends StatefulWidget {
  final Vendor vendor;

  const VendorDetailScreen({Key key, @required this.vendor}) : super(key: key);
  @override
  _VendorDetailScreenState createState() => _VendorDetailScreenState();
}

class _VendorDetailScreenState extends State<VendorDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: GradientAppBar(
        title: "${vendor.name}'s Profile",
        showLeading: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            buildProfile(),
            buildDescription(),
            GradientDivider(
              gradient: AppParams.appBtnBrightGradient,
              height: 4,
            ),
            buildAdditionalDetails(),
            VendorReviewsListWidget(
              vendor: widget.vendor,
            )
          ],
        ),
      ),
    );
  }

  Widget buildProfile() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Stack(
          alignment: Alignment.topRight,
          children: <Widget>[
            Image.network(
              "${vendor.coverImage}",
              width: viewWidth,
              height: 175,
              fit: BoxFit.cover,
            ),
            Padding(
              padding: EdgeInsets.all(12),
              child: VendorFavouriteWidget(
                vendorId: widget.vendor.id,
              ),
            )
          ],
        ),
        Padding(
          padding: EdgeInsets.all(8),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Transform.translate(
                offset: Offset(0, -24),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white, shape: BoxShape.circle),
                  padding: EdgeInsets.all(2),
                  child: CircleAvatar(
                    radius: 30,
                    backgroundImage: NetworkImage("${vendor.profilePic}"),
                  ),
                ),
              ),
              SizedBox(
                width: 8,
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      GradientText(
                        "${vendor.name}",
                        gradient: AppParams.appTxtGradient,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w300),
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      SmoothStarRating(
                        color: Colors.yellow.withOpacity(0.8),
                        allowHalfRating: true,
                        size: 14,
                        rating: vendor.rating,
                        borderColor: Colors.yellow.withOpacity(0.8),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      GradientText(
                        " |  ${NumberFormat.compactCurrency(name: vendor.currency, decimalDigits: 0).format(vendor.cost.toInt())}",
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w300),
                        gradient: AppParams.appTxtGradient,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  GradientText(
                    "${vendor.location?.fullAddress ?? ""}",
                    gradient: AppParams.appTxtGradient,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
                  )
                ],
              )
            ],
          ),
        ),
        Transform.translate(
          offset: Offset(0, -18),
          child: Row(
            children: <Widget>[
              SizedBox(
                width: 12,
              ),
              Expanded(
                  child: AppGradientButton(
                label: "Get More Info".toUpperCase(),
                gradient: AppParams.appBtnBrightGradient,
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 12),
                labelStyle: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: Colors.black),
                onTap: () {},
                isOutline: false,
              )),
              OutlineButton(
                onPressed: () {
                  try {
                    UrlLauncher.launch("tel://${vendor.mobileNo}");
                  } catch (e) {
                    UIHelper.showErrorDialog(
                        context, "Can not perform this operation right now.");
                  }
                },
                child: Icon(
                  Icons.call,
                  size: 20,
                  color: Colors.white60,
                ),
                shape: CircleBorder(),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget buildDescription() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          GradientText(
            "Description",
            gradient: AppParams.appTxtGradient,
            style: TextStyle(fontWeight: FontWeight.w300, fontSize: 15),
          ),
          SizedBox(
            height: 12,
          ),
          Text(
            "${vendor.description}",
            style:
                TextStyle(color: Colors.white.withOpacity(0.6), fontSize: 12),
          ),
          SizedBox(
            height: 24,
          ),
        ],
      ),
    );
  }

  Widget buildAdditionalDetails() {
    return vendor.questions.length == 0
        ? Container()
        : Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 24,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: GradientText(
                  "Additional Details",
                  gradient: AppParams.appTxtGradient,
                  style: TextStyle(fontWeight: FontWeight.w300, fontSize: 15),
                ),
              ),
              SizedBox(
                height: 18,
              ),
              ListView.separated(
                itemBuilder: (context, index) {
                  FAQ faq = vendor.questions[index];
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("${faq.question}",
                            style:
                                TextStyle(color: Colors.white70, fontSize: 12)),
                        SizedBox(
                          height: 4,
                        ),
                        Text("${faq.answer}",
                            style:
                                TextStyle(color: Colors.white38, fontSize: 11)),
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) => Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  child: GradientDivider(
                    gradient: AppParams.appBtnBrightGradient,
                    height: 0.5,
                  ),
                ),
                physics: ClampingScrollPhysics(),
                itemCount: vendor.questions.length,
                shrinkWrap: true,
              ),
              SizedBox(
                height: 24,
              ),
              Center(
                child: SizedBox(
                  width: viewWidth * 2 / 3,
                  child: AppGradientButton(
                    label: "Get More Info".toUpperCase(),
                    gradient: AppParams.appBtnBrightGradient,
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                    labelStyle: TextStyle(
                        fontSize: 10,
                        fontWeight: FontWeight.w600,
                        color: Colors.black),
                    onTap: () {},
                    isOutline: false,
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              GradientDivider(
                gradient: AppParams.appBtnBrightGradient,
                height: 4,
              ),
            ],
          );
  }

  double get viewWidth => MediaQuery.of(context).size.width;

  Vendor get vendor => widget.vendor;
}
