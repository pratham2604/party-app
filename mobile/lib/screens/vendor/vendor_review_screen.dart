import 'package:flutter/material.dart';
import 'package:party/controllers/review_controller.dart';
import 'package:party/helpers/input_helper.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/review.dart';
import 'package:party/models/vendor.dart';
import 'package:party/widgets/common/app_gradient_button.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class VendorReviewScreen extends StatefulWidget {
  final Vendor vendor;

  const VendorReviewScreen({Key key, @required this.vendor}) : super(key: key);
  @override
  _VendorReviewScreenState createState() => _VendorReviewScreenState();
}

class _VendorReviewScreenState extends State<VendorReviewScreen> {
  VendorReview vendorReview;
  bool isLoading;
  TextEditingController titleController, descriptionController;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    vendorReview = VendorReview.named();
    titleController = TextEditingController();
    descriptionController = TextEditingController();
    WidgetsBinding.instance.addPostFrameCallback((duration) {
      refresh();
    });
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    AppResponse<VendorReview> response =
        await ReviewController.getVendorReview(widget.vendor.id);
    if (response.data != null) {
      vendorReview = response.data;
      titleController.text = vendorReview.title;
      descriptionController.text = vendorReview.description;
      UIHelper.showErrorDialog(context, "You have already posted review.");
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: GradientAppBar(title: "Write Review", showLeading: true),
        backgroundColor: Colors.black,
        body: isLoading
            ? Center(
                child: AppProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Center(
                  child: Container(
                    width: MediaQuery.of(context).size.width - 32,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16, vertical: 12),
                    margin: EdgeInsets.symmetric(vertical: 18),
                    decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(8)),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Rating".toUpperCase(),
                          style: TextStyle(
                            color: Colors.white70,
                            fontSize: 11,
                          ),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        SmoothStarRating(
                          size: 32,
                          color: Colors.yellow,
                          borderColor: Colors.yellow,
                          onRatingChanged: (rating) {
                            setState(() {
                              if (vendorReview.id != null) {
                                return;
                              }
                              vendorReview = vendorReview
                                  .copyWith(VendorReview.named(rating: rating));
                            });
                          },
                          rating: vendorReview.rating ?? 0,
                        ),
                        TextField(
                          controller: titleController,
                          style: TextStyle(fontSize: 14),
                          maxLength: 100,
                          onChanged: (val) {
                            setState(() {
                              vendorReview = vendorReview
                                  .copyWith(VendorReview.named(title: val));
                            });
                          },
                          readOnly: vendorReview.id != null,
                          decoration: InputDecoration(
                              labelText: "Title".toUpperCase(),
                              hintText: "eg Superb",
                              hintStyle: TextStyle(fontSize: 12),
                              border: UnderlineInputBorder()),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Text(
                          "Description".toUpperCase(),
                          style: TextStyle(
                            color: Colors.white70,
                            fontSize: 11,
                          ),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        TextField(
                          style: TextStyle(fontSize: 14, color: Colors.black),
                          maxLength: 100,
                          controller: descriptionController,
                          maxLines: 4,
                          readOnly: vendorReview.id != null,
                          onChanged: (val) {
                            setState(() {
                              vendorReview = vendorReview.copyWith(
                                  VendorReview.named(description: val));
                            });
                          },
                          decoration: InputDecoration(
                              fillColor: Colors.white,
                              filled: true,
                              hintStyle: TextStyle(fontSize: 12),
                              border: UnderlineInputBorder()),
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        Opacity(
                          opacity: vendorReview.id != null ? 0.5 : 1,
                          child: AppGradientButton(
                              label: "Create",
                              isOutline: false,
                              gradient: AppParams.appBtnGradient,
                              labelStyle: TextStyle(fontSize: 15),
                              padding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              onTap: vendorReview.id != null
                                  ? null
                                  : () async {
                                      String error;
                                      if (vendorReview.rating == null) {
                                        error = "Please enter a valid rating";
                                      } else if (InputHelper.isBlank(
                                          vendorReview.title)) {
                                        error = InputHelper.getBlankValidation(
                                            "title");
                                      } else if (InputHelper.isBlank(
                                          vendorReview.description)) {
                                        error = InputHelper.getBlankValidation(
                                            "description");
                                      }
                                      if (error != null) {
                                        UIHelper.showErrorDialog(
                                            context, error);
                                        return;
                                      }
                                      UIHelper.showLoadingDialog(
                                          context, "Please wait...");
                                      vendorReview = vendorReview.copyWith(
                                          VendorReview.named(
                                              vendorId: widget.vendor.id));
                                      AppResponse<bool> response =
                                          await ReviewController
                                              .postVendorReview(vendorReview);
                                      Navigator.of(context).pop();
                                      if (response.isSuccess) {
                                        await UIHelper.showAlertDialog(context,
                                            "Success", "Review Posted");
                                        Navigator.of(context).pop();
                                      } else {
                                        UIHelper.showErrorDialog(
                                            context, response.error);
                                      }
                                    }),
                        )
                      ],
                    ),
                  ),
                ),
              ));
  }
}
