import 'package:flutter/material.dart';
import 'package:party/controllers/vendor_controller.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/vendor.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';
import 'package:party/widgets/vendor/vendor_widget.dart';

class VendorExploreScreen extends StatefulWidget {
  final String query;
  final VendorCategory category;

  const VendorExploreScreen(
      {Key key, @required this.query, @required this.category})
      : super(key: key);
  @override
  _VendorExploreScreenState createState() => _VendorExploreScreenState();
}

class _VendorExploreScreenState extends State<VendorExploreScreen> {
  // Variables
  List<Vendor> vendors;
  bool isLoading;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    vendors = [];
    WidgetsBinding.instance.addPostFrameCallback((duration) {
      refresh();
    });
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    AppResponse response;
    if (widget.category != null) {
      response = await VendorController.getVendorsByCategory(widget.category);
    } else if (widget.query != null) {
      response = await VendorController.getVendorsByQuery(widget.query);
    }
    if (response.isSuccess) {
      vendors = response.data;
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    String title = "Search";
    if (widget.query != null) {
      title = widget.query;
    }
    if (widget.category != null) {
      title = "${widget.category.name} available";
    }
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: GradientAppBar(
        title: title,
        showLeading: true,
      ),
      body: isLoading
          ? Center(
              child: AppProgressIndicator(),
            )
          : ListView.builder(
              itemBuilder: (context, index) =>
                  VendorWidget(vendor: vendors[index]),
              itemCount: vendors.length,
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
            ),
    );
  }
}
