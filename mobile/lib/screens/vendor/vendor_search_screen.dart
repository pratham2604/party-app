import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:party/controllers/vendor_controller.dart';
import 'package:party/helpers/input_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/vendor.dart';
import 'package:party/screens/vendor/vendor_explore_screen.dart';
import 'package:party/screens/vendor/vendor_favourites_screen.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/ease_in_widget.dart';
import 'package:party/widgets/common/gradient_icon_button.dart';
import 'package:party/widgets/vendor/vendor_category_widget.dart';

class VendorSearchScreen extends StatefulWidget {
  @override
  _VendorSearchScreenState createState() => _VendorSearchScreenState();
}

class _VendorSearchScreenState extends State<VendorSearchScreen> {
  bool isLoading;
  List<VendorCategory> vendorCategories;
  List<Vendor> vendors;
  String query;
  FocusNode focusNode;
  TextEditingController editingController;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    query = '';
    editingController = TextEditingController();
    focusNode = FocusNode();
    WidgetsBinding.instance.addPostFrameCallback((duration) {
      refresh();
    });
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });

    VendorController.getVendorCategories().listen((response) {
      if (response.isSuccess) {
        vendorCategories = response.data;
      } else {
        vendorCategories = [];
      }
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(gradient: AppParams.appDarkGradient),
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.black,
          body: isLoading
              ? Center(
                  child: AppProgressIndicator(),
                )
              : SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    children: <Widget>[
                      buildSearchBar(),
                      ListView.builder(
                        itemBuilder: (context, index) => VendorCategoryWidget(
                            category: vendorCategories[index]),
                        itemCount: vendorCategories.length,
                        shrinkWrap: true,
                        physics: ClampingScrollPhysics(),
                      )
                    ],
                  ),
                ),
        ),
      ),
    );
  }

  Widget buildSearchBar() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      child: Row(
        children: <Widget>[
          Expanded(
              child: Stack(
            alignment: Alignment.centerRight,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(24)),
                child: CupertinoTextField(
                  style: TextStyle(fontSize: 14, color: Colors.black),
                  placeholder: "Search here...",
                  onChanged: (val) {
                    setState(() {
                      query = val;
                    });
                  },
                  onSubmitted: (val) {
                    query = val;
                    setState(() {});
                    search(query);
                  },
                  focusNode: focusNode,
                  controller: editingController,
                  textInputAction: TextInputAction.search,
                  placeholderStyle: TextStyle(
                      fontWeight: FontWeight.normal, color: Colors.black45),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.transparent)),
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 12),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4),
                child: buildGradientBtn("assets/SearchUnslected.png", () {
                  search(query);
                }),
              )
            ],
          )),
          SizedBox(
            width: 14,
          ),
          GradientIconButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => VendorFavouritesScreen()));
            },
            asset: AppParams.heartSelectedIcon,
            color: Colors.white,
            size: 32,
          )
        ],
      ),
    );
  }

  void search(String query) {
    if (!InputHelper.isBlank(query)) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) =>
              VendorExploreScreen(query: query, category: null)));
      editingController.text = '';
      focusNode.unfocus();
    }
  }

  Widget buildGradientBtn(String asset, Function() onPressed,
      {double size = 20}) {
    return EaseInWidget(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: AppParams.appBtnGradient,
        ),
        padding: EdgeInsets.all(6),
        child: Image.asset(
          asset,
          width: size,
          height: size,
          fit: BoxFit.fill,
          color: Colors.white,
        ),
      ),
    );
  }
}
