import 'package:flutter/material.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/user.dart';
import 'package:party/screens/home/home_screen.dart';
import 'package:party/screens/login/email_login_screen.dart';
import 'package:party/screens/login/email_signup_screen.dart';
import 'package:party/screens/login/mobile_login_screen.dart';
import 'package:party/widgets/common/app_logo.dart';
import 'package:party/widgets/common/ease_in_widget.dart';
import 'package:party/widgets/login/login_social_button.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    double verticalMargin = 24;
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppParams.appPartyBackground),
                fit: BoxFit.fill),
          ),
          child: Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width * 2 / 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  AppLogo(),
                  Text(
                    "The PWM app is designed to make planning\nyour party easy.",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                  SizedBox(
                    height: 64,
                  ),
                  EaseInWidget(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => MobileLoginScreen()));
                    },
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 16, horizontal: 12),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        gradient: AppParams.appGradient,
                      ),
                      child: Text(
                        "Sign up with OTP",
                        style: TextStyle(fontSize: 12),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: verticalMargin,
                  ),
                  LoginSocialButton(
                    imageProvider: AssetImage(AppParams.loginGoogleIcon),
                    label: "Continue with Google",
                    onPressed: () async {
                      UIHelper.showLoadingDialog(context, "Please wait");
                      // Login with Google
                      AppResponse<User> response =
                          await LoginController.loginWithGoogle();
                      Navigator.of(context).pop();
                      if (response.isSuccess) {
                        LoginController.saveCurrentUserId(response.data.id);
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => HomeScreen()));
                      } else {
                        UIHelper.showErrorDialog(context, response.error);
                      }
                    },
                    color: Colors.white,
                    labelStyle: TextStyle(color: Colors.black),
                  ),
                  SizedBox(
                    height: verticalMargin,
                  ),
                  LoginSocialButton(
                    imageProvider: AssetImage(AppParams.loginFacebookIcon),
                    label: "Continue with Facebook",
                    onPressed: () async {
                      UIHelper.showLoadingDialog(context, "Please wait");
                      // Login with Facebook
                      AppResponse response =
                          await LoginController.loginWithFacebook();
                      Navigator.of(context).pop();
                      if (response.isSuccess) {
                        Navigator.of(context)
                            .popUntil(ModalRoute.withName('/'));
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => HomeScreen()));
                        UIHelper.showAlertDialog(context, "Success",
                            "You have logged in successfully");
                      } else {
                        UIHelper.showErrorDialog(context, response.error);
                      }
                    },
                    color: Color(0xff3b5998),
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                  SizedBox(
                    height: verticalMargin,
                  ),
                  LoginSocialButton(
                    imageProvider: AssetImage(AppParams.loginGmailIcon),
                    label: "Signup with your email",
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => EmailSignupScreen()));
                    },
                    color: Colors.white,
                    labelStyle: TextStyle(color: Colors.black),
                  ),
                  FlatButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => EmailLoginScreen()));
                      },
                      child: Text(
                        "Log In",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.normal),
                      )),
                  SizedBox(
                    height: 24,
                  ),
                  // FlatButton(
                  //     onPressed: () {},
                  //     child: Text(
                  //       "Sign up Later",
                  //       style: TextStyle(
                  //           color: Colors.white, fontWeight: FontWeight.normal),
                  //     )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
