// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/metadata/params.dart';
import 'package:party/screens/home/home_screen.dart';
import 'package:party/screens/login/login_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    login();
  }

  Future<void> login() async {
    String userId = await LoginController.getCurrentUserId();
    // String token = await FirebaseMessaging().getToken();
    // FirebaseController.saveToken(token);
    if (userId == null)
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => LoginScreen()));
    else {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => HomeScreen()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light
          .copyWith(statusBarColor: Colors.transparent),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage(AppParams.splashBackground),
          fit: BoxFit.fill,
        )),
      ),
    );
  }
}
