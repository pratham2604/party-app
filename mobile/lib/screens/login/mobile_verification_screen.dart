import 'package:flutter/material.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/user.dart';
import 'package:party/screens/home/home_screen.dart';
import 'package:party/widgets/common/app_button.dart';
import 'package:party/widgets/common/app_logo.dart';
import 'package:party/widgets/login/login_input_widget.dart';

class MobileVerificationScreen extends StatefulWidget {
  final String mobileNumber;

  const MobileVerificationScreen({Key key, @required this.mobileNumber})
      : super(key: key);
  @override
  _MobileVerificationScreenState createState() =>
      _MobileVerificationScreenState();
}

class _MobileVerificationScreenState extends State<MobileVerificationScreen> {
  bool isLoading;
  User user;
  String firstName;
  String lastName;

  @override
  void initState() {
    super.initState();
    refresh();
  }

  Future<void> refresh() async {
    UIHelper.showLoadingDialog(context, "Please wait...");
    setState(() {
      isLoading = true;
    });
    User temp = await LoginController.getExistingUser(
        mobileNumber: widget.mobileNumber);
    Navigator.of(context).pop();

    if (temp == null) {
      UIHelper.showErrorDialog(context, "User does not exists. Please sign up");
      setState(() {
        isLoading = false;
      });
    } else {
      if (temp.isDisabled) {
        await UIHelper.showErrorDialog(
          context,
          "Your account has been suspended. Please contact customer support to resolve the conflict.",
        );
        Navigator.of(context).pop();

        return;
      }
      LoginController.saveCurrentUserId(temp.id);
      Navigator.of(context).popUntil(ModalRoute.withName('/'));
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => HomeScreen()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      backgroundColor: Colors.black,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppParams.appPartyBackground),
              fit: BoxFit.fill),
        ),
        child: isLoading
            ? Container()
            : Center(
                child: SingleChildScrollView(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 2 / 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(height: 128, child: AppLogo()),
                        SizedBox(
                          height: 12,
                        ),
                        Text(
                          "Please fill following form to continue with ${widget.mobileNumber}",
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        LoginInputWidget(
                          label: "First Name",
                          onChanged: (val) {
                            setState(() {
                              firstName = val;
                            });
                          },
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        LoginInputWidget(
                          label: "Last Name",
                          onChanged: (val) {
                            setState(() {
                              lastName = val;
                            });
                          },
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        AppButton(
                            label: "Login",
                            onPressed: () async {
                              UIHelper.showLoadingDialog(
                                  context, "Please wait..");
                              AppResponse<User> response =
                                  await LoginController.signupUser(User.named(
                                      firstName: firstName,
                                      lastName: lastName,
                                      mobileNo: widget.mobileNumber));
                              Navigator.of(context).pop();
                              if (response.error != null) {
                                UIHelper.showErrorDialog(
                                    context, response.error);
                              } else {
                                await LoginController.saveCurrentUserId(
                                    response.data.id);
                                Navigator.of(context)
                                    .popUntil(ModalRoute.withName('/'));
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => HomeScreen()));
                              }
                            })
                      ],
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
