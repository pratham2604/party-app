import 'package:country_pickers/country_picker_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:party/controllers/otp_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/screens/login/mobile_verification_screen.dart';
import 'package:party/widgets/common/app_button.dart';
import 'package:party/widgets/common/app_logo.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

class MobileLoginScreen extends StatefulWidget {
  @override
  _MobileLoginScreenState createState() => _MobileLoginScreenState();
}

class _MobileLoginScreenState extends State<MobileLoginScreen> {
  bool isOTPSent;
  bool isMobileTyping;
  double fontSize;

  // Variables
  String mobileNumber;
  String countryCode;
  String otp;

  // Controllers
  TextEditingController editingController;
  FocusNode mobileNode;

  @override
  void initState() {
    super.initState();
    isOTPSent = false;
    isMobileTyping = true;
    fontSize = 16;
    //
    editingController = TextEditingController();
    mobileNode = FocusNode();
    countryCode = '91';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppParams.appPartyBackground),
              fit: BoxFit.fill),
        ),
        child: Center(
          child: Stack(
            children: <Widget>[
              Center(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 3 / 4,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(
                          height: 200,
                          child: AppLogo(),
                        ),
                        SizedBox(
                          height: 18,
                        ),
                        Text(
                          !isOTPSent
                              ? "Please enter your mobile number"
                              : "Verification code sent to your mobile number",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Center(
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              isMobileTyping
                                  ? Expanded(
                                      child: TextField(
                                        controller: editingController,
                                        focusNode: mobileNode,
                                        maxLength: 12,
                                        onChanged: (String val) {
                                          setState(() {
                                            mobileNumber = val;
                                          });
                                        },
                                        style: TextStyle(
                                            fontSize: fontSize,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500),
                                        inputFormatters: [
                                          WhitelistingTextInputFormatter
                                              .digitsOnly
                                        ],
                                        keyboardType:
                                            TextInputType.numberWithOptions(),
                                        decoration: InputDecoration(
                                            prefix: InkWell(
                                              onTap: () {
                                                showDialog(
                                                    context: context,
                                                    builder: (context) =>
                                                        CountryPickerDialog(
                                                          onValuePicked:
                                                              (country) {
                                                            setState(() {
                                                              countryCode =
                                                                  country
                                                                      .phoneCode;
                                                            });
                                                          },
                                                        ));
                                              },
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Row(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: <Widget>[
                                                    Text(
                                                      "+$countryCode",
                                                      style: TextStyle(
                                                          fontSize: fontSize,
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                    Icon(Icons.arrow_drop_down)
                                                  ],
                                                ),
                                              ),
                                            ),
                                            enabledBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.white)),
                                            focusedBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.white)),
                                            border: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.white,
                                                    width: 1))),
                                      ),
                                    )
                                  : FittedBox(
                                      child: Text(
                                        "$countryCode-$mobileNumber",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fontSize,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                              !isMobileTyping
                                  ? IconButton(
                                      icon: Icon(
                                        Icons.edit,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                      onPressed: () async {
                                        setState(() {
                                          isMobileTyping = true;
                                          editingController =
                                              TextEditingController(
                                                  text: editingController.text);
                                        });
                                        await Future.delayed(
                                            Duration(milliseconds: 200));
                                        mobileNode.requestFocus();
                                      },
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                        !isMobileTyping
                            ? Center(
                                child: SizedBox(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      SizedBox(
                                        height: 24,
                                      ),
                                      PinCodeTextField(
                                        wrapAlignment:
                                            WrapAlignment.spaceBetween,
                                        maxLength: 6,
                                        pinBoxWidth: 32,
                                        pinBoxHeight: 32,
                                        defaultBorderColor: Colors.white,
                                        onDone: (val) {
                                          setState(() {
                                            otp = val;
                                          });
                                        },
                                      ),
                                      SizedBox(
                                        height: 24,
                                      ),
                                      RichText(
                                          text: TextSpan(children: [
                                        TextSpan(
                                            text: "Didn't receive the code? "),
                                        TextSpan(
                                          text: "RESEND",
                                        )
                                      ]))
                                    ],
                                  ),
                                ),
                              )
                            : Container(),
                        SizedBox(
                          height: 18,
                        ),
                        AppButton(
                            label: isMobileTyping ? "Send OTP" : "Verify OTP",
                            onPressed: () {
                              if (isMobileTyping) {
                                sendOTP(mobileNumber);
                              } else {
                                verifyOTP(otp);
                              }
                            }),
                      ],
                    ),
                  ),
                ),
              ),
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> sendOTP(String mobileNumber) async {
    setState(() {
      isMobileTyping = false;
    });
    UIHelper.showLoadingDialog(context, "Sending OTP...");
    String error = await OTPController.sendOTP(mobileNumber);
    Navigator.of(context).pop();
    setState(() {
      isMobileTyping = false;
    });
    if (error == null) {
      setState(() {
        isOTPSent = true;
      });
    } else {
      UIHelper.showErrorDialog(context, error);
    }
  }

  Future<void> verifyOTP(String otp) async {
    UIHelper.showLoadingDialog(context, "Sending OTP...");
    AppResponse<bool> response =
        await OTPController.verifyOTP(mobileNumber, otp);
    Navigator.of(context).pop();
    if (response.isSuccess) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => MobileVerificationScreen(
              mobileNumber: "+$countryCode$mobileNumber")));
    } else {
      UIHelper.showErrorDialog(context, response.error);
    }
  }
}
