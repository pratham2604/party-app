import 'package:flutter/material.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/screens/home/home_screen.dart';
import 'package:party/widgets/common/app_button.dart';
import 'package:party/widgets/common/app_logo.dart';
import 'package:party/widgets/login/login_input_widget.dart';

class EmailLoginScreen extends StatefulWidget {
  @override
  _EmailLoginScreenState createState() => _EmailLoginScreenState();
}

class _EmailLoginScreenState extends State<EmailLoginScreen> {
  FocusNode emailNode;
  FocusNode passwordNode;

  // Variables
  String email;
  String password;

  @override
  void initState() {
    super.initState();
    emailNode = FocusNode();
    passwordNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppParams.appPartyBackground),
              fit: BoxFit.fill),
        ),
        child: Stack(
          alignment: Alignment.topLeft,
          children: <Widget>[
            Center(
              child: SingleChildScrollView(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 2 / 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(height: 0, child: AppLogo()),
                      SizedBox(
                        height: 16,
                      ),
                      LoginInputWidget(
                        label: "Email",
                        onChanged: (val) {
                          setState(() {
                            email = val;
                          });
                        },
                        onSubmitted: (val) {
                          emailNode.unfocus();
                          passwordNode.requestFocus();
                        },
                        focusNode: emailNode,
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      LoginInputWidget(
                        label: "Password",
                        onChanged: (val) {
                          password = val;
                        },
                        isObsecure: true,
                        focusNode: passwordNode,
                      ),
                      SizedBox(
                        height: 36,
                      ),
                      AppButton(
                          label: "Login",
                          onPressed: () async {
                            UIHelper.showLoadingDialog(
                                context, "Please wait..");
                            AppResponse response =
                                await LoginController.loginWithEmail(
                                    email, password);
                            Navigator.of(context).pop();
                            if (response.error != null) {
                              UIHelper.showErrorDialog(context, response.error);
                            } else {
                              Navigator.of(context)
                                  .popUntil(ModalRoute.withName('/'));
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => HomeScreen()));
                            }
                          })
                    ],
                  ),
                ),
              ),
            ),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
