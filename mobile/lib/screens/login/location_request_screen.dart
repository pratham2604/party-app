import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:party/controllers/location_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/widgets/common/app_button.dart';
import 'package:party/widgets/common/app_logo.dart';

class LocationRequestScreen extends StatefulWidget {
  @override
  _LocationRequestScreenState createState() => _LocationRequestScreenState();
}

class _LocationRequestScreenState extends State<LocationRequestScreen> {
  // Location Requesting

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: 12,
              ),
              Center(
                child: SizedBox(
                  child: Image.asset(
                    AppParams.locationImage,
                    height: viewHeight / 3,
                  ),
                ),
              ),
              SizedBox(
                height: 125,
                child: AppLogo(),
              ),
              SizedBox(
                width: viewWidth * 3 / 4,
                child: Text(
                  "Find all best vendors in your area",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Expanded(
                child: Container(),
                flex: 2,
              ),
              Text(
                "Now you can see all the party vendors in your area",
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: Colors.white70,
                    fontSize: 11),
              ),
              Expanded(
                child: Container(),
                flex: 1,
              ),
              SizedBox(
                width: viewWidth * 2 / 3,
                child: AppButton(
                  label: "Enable Location",
                  onPressed: () async {
                    UIHelper.showLoadingDialog(context, "Please Wait...");
                    AppResponse<LocationData> response =
                        await LocationController.getLocation();
                    Navigator.of(context).pop();
                    if (response.isSuccess) {
                      Navigator.of(context).pop(true);
                    } else {
                      UIHelper.showErrorDialog(context, response.error);
                    }
                  },
                ),
              ),
              Expanded(
                child: Container(),
                flex: 2,
              ),
              InkWell(
                onTap: () async {
                  Navigator.of(context).pop();
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "No Thanks",
                    style: TextStyle(
                        color: Colors.white.withOpacity(0.8), fontSize: 11),
                  ),
                ),
              ),
              Expanded(
                child: Container(),
                flex: 1,
              ),
            ],
          ),
        ),
      ),
    );
  }

  double get viewHeight => MediaQuery.of(context).size.height;

  double get viewWidth => MediaQuery.of(context).size.width;
}
