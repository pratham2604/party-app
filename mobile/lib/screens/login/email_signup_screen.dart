import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/user.dart';
import 'package:party/screens/home/home_screen.dart';
import 'package:party/widgets/common/app_button.dart';
import 'package:party/widgets/common/app_logo.dart';
import 'package:party/widgets/login/login_input_widget.dart';

class EmailSignupScreen extends StatefulWidget {
  @override
  _EmailSignupScreenState createState() => _EmailSignupScreenState();
}

class _EmailSignupScreenState extends State<EmailSignupScreen> {
  FocusNode firstNameNode;
  FocusNode lastNameNode;
  FocusNode emailNode;
  FocusNode passwordNode;
  FocusNode confirmPasswordNode;

  // Variables
  User user;
  String confirmPassword;

  @override
  void initState() {
    super.initState();
    // firstNameNode = FocusNode();
    // lastNameNode = FocusNode();
    // emailNode = FocusNode();
    // passwordNode = FocusNode();
    // confirmPasswordNode = FocusNode();
    //
    user = User.named();
    confirmPassword = '';
  }

  @override
  Widget build(BuildContext context) {
    double verticalMargin = 24;
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppParams.appPartyBackground),
              fit: BoxFit.fill),
        ),
        child: Stack(
          alignment: Alignment.topLeft,
          children: <Widget>[
            Center(
              child: SingleChildScrollView(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 2 / 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(height: 150, child: AppLogo()),
                      SizedBox(
                        height: 16,
                      ),
                      LoginInputWidget(
                        label: "First name",
                        onChanged: (val) {
                          setState(() {
                            user = user.copyWith(User.named(firstName: val));
                          });
                        },
                        onSubmitted: (val) {
                          // firstNameNode.unfocus();
                          // lastNameNode.requestFocus();
                        },
                        focusNode: firstNameNode,
                      ),
                      SizedBox(
                        height: verticalMargin,
                      ),
                      LoginInputWidget(
                        label: "Last name",
                        onChanged: (val) {
                          setState(() {
                            user = user.copyWith(User.named(lastName: val));
                          });
                        },
                        onSubmitted: (val) {
                          // lastNameNode.unfocus();
                          // emailNode.requestFocus();
                        },
                        focusNode: lastNameNode,
                      ),
                      SizedBox(
                        height: verticalMargin,
                      ),
                      LoginInputWidget(
                        label: "Email",
                        onChanged: (val) {
                          setState(() {
                            user = user.copyWith(User.named(email: val));
                          });
                        },
                        onSubmitted: (val) {
                          // emailNode.unfocus();
                          // passwordNode.requestFocus();
                        },
                        focusNode: emailNode,
                      ),
                      SizedBox(
                        height: verticalMargin,
                      ),
                      LoginInputWidget(
                        label: "Password",
                        onChanged: (val) {
                          setState(() {
                            user = user.copyWith(User.named(password: val));
                          });
                        },
                        onSubmitted: (val) {
                          // passwordNode.unfocus();
                          // confirmPasswordNode.requestFocus();
                        },
                        isObsecure: true,
                        focusNode: passwordNode,
                      ),
                      SizedBox(
                        height: verticalMargin,
                      ),
                      LoginInputWidget(
                        label: "Re-type Password",
                        onChanged: (val) {
                          setState(() {
                            confirmPassword = val;
                          });
                        },
                        isObsecure: true,
                        onSubmitted: (val) {},
                        focusNode: confirmPasswordNode,
                      ),
                      SizedBox(
                        height: 36,
                      ),
                      AppButton(
                          label: "Sign up",
                          onPressed: () async {
                            if (confirmPassword != user.password) {
                              UIHelper.showErrorDialog(
                                  context, "Password does not match");
                              return;
                            }
                            UIHelper.showLoadingDialog(
                                context, "Please wait..");
                            AppResponse<User> response =
                                await LoginController.signupWithEmail(user);
                            Navigator.of(context).pop();
                            if (response.error != null) {
                              UIHelper.showErrorDialog(context, response.error);
                            } else {
                              log("User Id ${response.data.id}");
                              Navigator.of(context)
                                  .popUntil(ModalRoute.withName('/'));
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => HomeScreen()));
                            }
                          })
                    ],
                  ),
                ),
              ),
            ),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
