import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:party/controllers/location_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/models/app_location.dart';
import 'package:party/models/app_response.dart';
import 'package:party/screens/location/location_picker_screen.dart';

class LocationSearchScreen extends StatefulWidget {
  @override
  _LocationSearchScreenState createState() => _LocationSearchScreenState();
}

class _LocationSearchScreenState extends State<LocationSearchScreen> {
  // Variables
  List<AppLocation> locations;
  bool isLoading;

  @override
  void initState() {
    super.initState();
    isLoading = false;
  }

  Future<void> searchLocation(String query) async {
    setState(() {
      isLoading = true;
    });
    locations = await LocationController.searchLocation(query);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Search Location"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
              child: TextField(
                decoration: InputDecoration(
                  labelText: "Search Location",
                ),
                onChanged: (val) {
                  searchLocation(val);
                },
              ),
            ),
            ListTile(
              onTap: () async {
                AppLocation result = await Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => LocationPickerScreen()));
                if (result != null) {
                  log(result.runtimeType.toString());
                  Navigator.of(context).pop(AppResponse<AppLocation>.named(
                      error: null, data: result));
                }
              },
              leading: Icon(Icons.location_on),
              title: Text("Pick Location"),
            ),
            Divider(),
            ListView.builder(
              itemBuilder: (context, index) {
                return ListTile(
                  onTap: () async {
                    UIHelper.showLoadingDialog(context, "Please wait...");
                    AppResponse<AppLocation> response =
                        await LocationController.getLocationFromPlaceId(
                            locations[index].id);

                    Navigator.of(context).pop();
                    log("Location ${response.data.fullAddress}");
                    if (response.isSuccess)
                      Navigator.of(context).pop(response);
                    else {
                      UIHelper.showErrorDialog(
                          context, "Unknown error occured");
                    }
                  },
                  leading: Icon(Icons.location_on),
                  title: Text("${locations[index].fullAddress}"),
                );
              },
              itemCount: locations?.length ?? 0,
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
            )
          ],
        ),
      ),
    );
  }
}
