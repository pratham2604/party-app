import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:party/controllers/location_controller.dart';
import 'package:party/models/app_location.dart';
import 'package:party/widgets/common/location_pick_map.dart';

class LocationPickerScreen extends StatefulWidget {
  const LocationPickerScreen({
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LocationPickerScreenState();
}

class _LocationPickerScreenState extends State<LocationPickerScreen> {
  AppLocation request;
  bool loading = true;
  LocationPickController controller;
  bool isFirstTime;

  @override
  void initState() {
    super.initState();
    isFirstTime = true;
  }

  void getCurrentLocation() {
    setState(() {
      this.loading = true;
    });
    LocationController.getLocation().then((response) {
      if (response.isSuccess) {
        LocationData locationData = response.data;
        refreshLocation(LatLng(locationData.latitude, locationData.longitude));
        this
            .controller
            .moveTo(LatLng(locationData.latitude, locationData.longitude));
      }
    });
  }

  void refreshLocation(LatLng position) {
    LocationController.getLocationRequest(position).then((response) {
      if (response.isSuccess) {
        this.request = response.data;
        isFirstTime = false;
        if (mounted)
          setState(() {
            this.loading = false;
          });
      }
    });
  }

  Widget buildLocationBar() {
    return SafeArea(
      child: Card(
        elevation: 5.0,
        margin: EdgeInsets.all(12.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                children: <Widget>[
                  Icon(Icons.location_on),
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.only(left: 4.0),
                    child: Text(
                      request?.fullAddress ?? "Searching Location",
                      style: TextStyle(
                        fontSize: 18.0,
                      ),
                      maxLines: 4,
                    ),
                  )),
                ],
              ),
            ),
            loading ? LinearProgressIndicator() : Container(height: 2.0)
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          LocationPickupMap(
            controllerCompleted: (controller) {
              this.controller = controller;
              getCurrentLocation();
            },
            onPositionChanged: (position) {
              setState(() {
                this.loading = true;
              });
              refreshLocation(position);
            },
          ),
          buildLocationBar(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.my_location,
          color: Theme.of(context).primaryColor,
        ),
        backgroundColor: Colors.white,
        onPressed: getCurrentLocation,
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(16.0),
        child: CupertinoButton(
          child: Text("Continue"),
          color: Colors.black,
          onPressed: () {
            Navigator.of(context).pop(this.request);
          },
        ),
      ),
    );
  }
}
