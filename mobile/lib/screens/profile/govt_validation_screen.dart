import 'dart:io';

import 'package:flutter/material.dart';
import 'package:party/controllers/image_controller.dart';
import 'package:party/controllers/user_controller.dart';
import 'package:party/helpers/input_helper.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/validation_doc.dart';
import 'package:party/widgets/common/app_gradient_button.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';
import 'package:party/widgets/common/image_input_widget.dart';

class GovtValidationScreen extends StatefulWidget {
  final String name;
  final String id;

  const GovtValidationScreen({Key key, @required this.name, @required this.id})
      : super(key: key);
  @override
  _GovtValidationScreenState createState() => _GovtValidationScreenState();
}

class _GovtValidationScreenState extends State<GovtValidationScreen> {
  File file;
  ValidationDoc validationDoc;

  @override
  void initState() {
    super.initState();
    validationDoc = ValidationDoc.named();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: GradientAppBar(
        title: "Govt Identification",
        showLeading: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 18),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextField(
                style: TextStyle(fontSize: 14),
                maxLength: 100,
                controller: TextEditingController(text: widget.name),
                readOnly: true,
                decoration: InputDecoration(
                    labelText: "Name".toUpperCase(),
                    hintStyle: TextStyle(fontSize: 12),
                    border: UnderlineInputBorder()),
              ),
              SizedBox(
                height: 12,
              ),
              TextField(
                style: TextStyle(fontSize: 14),
                maxLength: 100,
                onChanged: (val) {
                  setState(() {
                    validationDoc =
                        validationDoc.copyWith(ValidationDoc.named(data: val));
                  });
                },
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                    labelText: "Identification Number".toUpperCase(),
                    hintStyle: TextStyle(fontSize: 12),
                    border: UnderlineInputBorder()),
              ),
              SizedBox(
                height: 12,
              ),
              ImageInputWidget(
                onImagesAdded: (val) {
                  if (val.length == 1)
                    setState(() {
                      file = val[0];
                    });
                  else {
                    setState(() {
                      file = null;
                    });
                  }
                },
                maxCount: 1,
              ),
              SizedBox(
                height: 12,
              ),
              AppGradientButton(
                  label: "Add ${widget.name}",
                  gradient: AppParams.appBtnGradient,
                  labelStyle: null,
                  isOutline: false,
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 12),
                  onTap: () async {
                    if (InputHelper.isBlank(validationDoc.data)) {
                      UIHelper.showErrorDialog(
                          context, "Please enter valid identification");
                      return;
                    } else if (file == null) {
                      UIHelper.showErrorDialog(
                          context, "Please select photo for verfication");
                      return;
                    }
                    UIHelper.showLoadingDialog(context, "Please Wait...");
                    String imageLink = await ImageController.storeImage(file);
                    if (imageLink == null) {
                      Navigator.of(context).pop();
                      UIHelper.showErrorDialog(context, "Please try again");
                      return;
                    } else {
                      validationDoc =
                          validationDoc.copyWith(ValidationDoc.named(
                        photo: imageLink,
                        id: widget.id,
                      ));
                      AppResponse postResponse =
                          await UserController.addValidation(validationDoc);
                      Navigator.of(context).pop();
                      if (postResponse.isSuccess) {
                        Navigator.of(context).pop(true);
                      } else {
                        UIHelper.showErrorDialog(context, postResponse.error);
                      }
                    }
                  })
            ],
          ),
        ),
      ),
    );
  }
}
