import 'package:flutter/material.dart';
import 'package:party/controllers/event_controller.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/event.dart';
import 'package:party/models/event_invite.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';
import 'package:party/widgets/event/event_invite_widget.dart';

class InvitesScreen extends StatefulWidget {
  @override
  _InvitesScreenState createState() => _InvitesScreenState();
}

class _InvitesScreenState extends State<InvitesScreen> {
  List<Event> events;
  List<EventInvite> eventInvites;
  bool isLoading;
  String selectedEvent;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    events = [];
    eventInvites = [];
    refresh();
  }

  Future<void> loadInvites() async {
    AppResponse<List<EventInvite>> response =
        await EventController.getMyInvites();
    if (response.isSuccess) {
      eventInvites = response.data;
    } else {
      UIHelper.showErrorDialog(context, response.error);
    }
  }

  Future<void> loadEvents() async {
    String currentUserId = await LoginController.getCurrentUserId();
    AppResponse<List<Event>> response =
        await EventController.getUserEvents(currentUserId);
    if (response.isSuccess) {
      events = response.data;
    } else {
      UIHelper.showErrorDialog(context, response.error);
    }
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    await Future.wait([loadInvites(), loadEvents()]);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<EventInvite> filtered = eventInvites ?? [];
    if (selectedEvent != null) {
      filtered = filtered.where((val) => val.eventId == selectedEvent).toList();
    }
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: GradientAppBar(
        title: "Invites",
        showLeading: true,
      ),
      body: isLoading
          ? Center(
              child: AppProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: 8,
                  ),
                  buildEvents(),
                  ListView.builder(
                    itemBuilder: (context, index) => EventInviteWidget(
                      eventInvite: filtered[index],
                      onTap: (id, isSelected) async {
                        UIHelper.showLoadingDialog(context, "Please wait...");
                        AppResponse<bool> response =
                            await EventController.acceptInvite(filtered[index],
                                check: isSelected);
                        Navigator.of(context).pop();
                        if (response.isSuccess) {
                          setState(() {
                            eventInvites.removeWhere((val) => val.id == id);
                          });
                        } else {
                          UIHelper.showErrorDialog(context, response.error);
                        }
                      },
                    ),
                    itemCount: filtered.length,
                    shrinkWrap: true,
                  )
                ],
              ),
            ),
    );
  }

  Widget buildEvents() {
    return Container(
      height: 42,
      margin: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
      child: ListView.builder(
        itemBuilder: (context, index) => selectedEvent == events[index].id
            ? Padding(
                padding: const EdgeInsets.only(right: 12),
                child: RaisedButton(
                  onPressed: null,
                  disabledColor: Colors.white,
                  child: Text(
                    "${events[index].name}",
                    style: TextStyle(color: Colors.black),
                  ),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24)),
                ),
              )
            : Padding(
                padding: const EdgeInsets.only(right: 12),
                child: OutlineButton(
                  onPressed: () {
                    setState(() {
                      selectedEvent = events[index].id;
                    });
                  },
                  child: Text(
                    "${events[index].name}",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.white,
                  borderSide: BorderSide(color: Colors.white, width: 1),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24)),
                ),
              ),
        itemCount: events.length,
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}
