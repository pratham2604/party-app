import 'package:flutter/material.dart';
import 'package:party/controllers/event_controller.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/event.dart';
import 'package:party/screens/event/event_detail_screen.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';
import 'package:party/widgets/event/event_widget.dart';

class MyEventsScreen extends StatefulWidget {
  @override
  _MyEventsScreenState createState() => _MyEventsScreenState();
}

class _MyEventsScreenState extends State<MyEventsScreen> {
  List<Event> events;
  bool isLoading;
  String selectedEvent;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    events = [];
    refresh();
  }

  Future<void> loadEvents() async {
    String currentUserId = await LoginController.getCurrentUserId();
    AppResponse<List<Event>> response =
        await EventController.getUserEvents(currentUserId);
    if (response.isSuccess) {
      events = response.data;
    } else {
      UIHelper.showErrorDialog(context, response.error);
    }
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    await Future.wait([loadEvents()]);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: GradientAppBar(
        title: "My Events",
        showLeading: true,
      ),
      body: isLoading
          ? Center(
              child: AppProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: 8,
                  ),
                  ListView.builder(
                    itemBuilder: (context, index) => EventWidget(
                      event: events[index],
                      onTap: (event) async {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                EventDetailScreen(event: events[index])));
                      },
                    ),
                    itemCount: events.length,
                    shrinkWrap: true,
                  )
                ],
              ),
            ),
    );
  }

  Widget buildEvents() {
    return Container(
      height: 42,
      margin: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
      child: ListView.builder(
        itemBuilder: (context, index) => selectedEvent == events[index].id
            ? Padding(
                padding: const EdgeInsets.only(right: 12),
                child: RaisedButton(
                  onPressed: null,
                  disabledColor: Colors.white,
                  child: Text(
                    "${events[index].name}",
                    style: TextStyle(color: Colors.black),
                  ),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24)),
                ),
              )
            : Padding(
                padding: const EdgeInsets.only(right: 12),
                child: OutlineButton(
                  onPressed: () {
                    setState(() {
                      selectedEvent = events[index].id;
                    });
                  },
                  child: Text(
                    "${events[index].name}",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.white,
                  borderSide: BorderSide(color: Colors.white, width: 1),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24)),
                ),
              ),
        itemCount: events.length,
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}
