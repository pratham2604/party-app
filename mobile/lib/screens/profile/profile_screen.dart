import 'package:flutter/material.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/controllers/user_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/user.dart';
import 'package:party/screens/login/login_screen.dart';
import 'package:party/screens/profile/account_screen.dart';
import 'package:party/screens/profile/invites_screen.dart';
import 'package:party/screens/profile/my_events_screen.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';

class ProfileScreen extends StatefulWidget {
  final Function() onPopBack;

  const ProfileScreen({Key key, this.onPopBack}) : super(key: key);
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  User currentUser;
  bool isLoading;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    refresh();
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    AppResponse<User> response = await UserController.getCurrentUser();

    if (response.isSuccess) {
      currentUser = response.data;
    } else {
      UIHelper.showErrorDialog(context, "You are not logged in");
    }
    if (mounted)
      setState(() {
        isLoading = false;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: GradientAppBar(
        title: "Profile",
        showLeading: true,
        onPopBack: widget.onPopBack,
      ),
      body: isLoading
          ? Center(
              child: AppProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  buildDetail(),
                  buildSeperator("Settings"),
                  buildOptions(
                      label: "Account",
                      iconData: Icons.person,
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => AccountScreen()));
                      }),
                  buildOptions(
                      label: "My Events",
                      iconData: Icons.event,
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => MyEventsScreen()));
                      }),
                  buildOptions(
                      label: "Invites",
                      iconData: Icons.notifications,
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => InvitesScreen()));
                      }),
                  // buildOptions(
                  //     label: "Help & Feedback",
                  //     iconData: Icons.help,
                  //     onPressed: () {}),
                  buildOptions(
                      label: "Logout",
                      iconData: Icons.settings_power,
                      onPressed: () async {
                        bool result = await UIHelper.showConfirmDialog(
                            context, "Do you want to logout?");
                        if (result) {
                          UIHelper.showLoadingDialog(context, "Please wait...");
                          await LoginController.logout();
                          Navigator.of(context).pop();
                          Navigator.of(context)
                              .popUntil(ModalRoute.withName('/'));
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => LoginScreen()));
                        }
                      }),
                ],
              ),
            ),
    );
  }

  Widget buildDetail() {
    return currentUser == null
        ? Container()
        : ListTile(
            contentPadding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
            leading: CircleAvatar(
              radius: 24,
              backgroundColor: Colors.white,
              backgroundImage: NetworkImage(
                  "${currentUser.profilePic ?? UserController.getDefaultPic(currentUser.name)}"),
            ),
            title: Text("${currentUser.name}"),
            subtitle: Text("${currentUser.uniqueKey}"),
            trailing: InkWell(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => AccountScreen()));
              },
              child: Container(
                child: Icon(
                  Icons.chevron_right,
                  color: Colors.black,
                ),
                padding: EdgeInsets.all(2),
                decoration:
                    BoxDecoration(color: Colors.white, shape: BoxShape.circle),
              ),
            ),
          );
  }

  Widget buildOptions({String label, IconData iconData, Function() onPressed}) {
    return ListTile(
      title: Text("$label"),
      leading: Icon(iconData),
      onTap: onPressed,
    );
  }

  Widget buildSeperator(String label) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
      color: Colors.white38,
      child: Text(
        "$label",
        style: TextStyle(fontSize: 14),
      ),
    );
  }
}
