import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/controllers/user_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/user.dart';
import 'package:party/models/validation_doc.dart';
import 'package:party/screens/profile/govt_validation_screen.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/approval_wating_widget.dart';
import 'package:party/widgets/common/gradient_app_bar.dart';
import 'package:party/widgets/common/gradient_divider.dart';
import 'package:party/widgets/common/gradient_text.dart';
import 'package:party/widgets/profile/account_email_widget.dart';
import 'package:party/widgets/profile/account_mobile_widget.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  // Variables
  bool isLoading;
  User currentUser;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      refresh();
    });
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    AppResponse<User> response = await UserController.getCurrentUser();
    setState(() {
      isLoading = false;
    });
    if (response.isSuccess) {
      setState(() {
        currentUser = response.data;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: GradientAppBar(
        title: "Account",
        showLeading: true,
      ),
      body: isLoading
          ? Center(
              child: AppProgressIndicator(),
            )
          : currentUser == null
              ? Container()
              : SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      buildUserDetails(currentUser),
                      SizedBox(
                        height: 26,
                      ),
                      buildSocialAccounts(),
                      SizedBox(
                        height: 24,
                      ),
                      buildVerifications()
                    ],
                  ),
                ),
    );
  }

  Widget buildUserDetails(User user) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          CircleAvatar(
            radius: (viewWidth - 24) / 6,
            backgroundImage: NetworkImage(user.profilePic ?? user.defaultPic),
          ),
          SizedBox(
            height: 16,
          ),
          Text(
            "${user.name}",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
          ),
          SizedBox(
            height: 6,
          ),
          Text("${user.uniqueKey}"),
        ],
      ),
    );
  }

  Widget buildSocialAccounts() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        GradientDivider(
          gradient: AppParams.appBtnGradient,
          height: 5,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 18),
          child: GradientText(
              text: Text(
                "Social Accounts",
                style: TextStyle(fontSize: 16),
              ),
              gradient: AppParams.appTxtGradient),
        ),
        GradientDivider(
          gradient: AppParams.appBtnGradient,
          height: 1,
        ),
        ListTile(
          leading: buildIcon(AppParams.loginGmailIcon),
          title: Text("Email"),
          trailing: currentUser.password != null
              ? buildCheck()
              : buildAction("Link", () async {
                  bool isUpdated = await Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) => AccountEmailWidget()));
                  isUpdated = isUpdated ?? false;
                  if (isUpdated) {
                    refresh();
                  }
                }),
        ),
        ListTile(
          leading: buildIcon(AppParams.loginGoogleIcon),
          title: Text("Google"),
          trailing: currentUser.googleId != null
              ? buildCheck()
              : buildAction("Link", () async {
                  UIHelper.showLoadingDialog(context, "Please Wait...");
                  AppResponse<User> response =
                      await LoginController.getGoogleUser();
                  User user = response.data;
                  if (response.isSuccess) {
                    User oldUser = await LoginController.getExistingUser(
                        googleId: response.data.googleId);
                    log("Google Id ${response.data.googleId}");
                    if (oldUser != null) {
                      Navigator.of(context).pop();
                      await GoogleSignIn().signOut();
                      await UIHelper.showErrorDialog(
                          context, "User already linked with this id");
                    } else {
                      AppResponse<User> newResponse =
                          await LoginController.updateUser(currentUser
                              .copyWith(User.named(googleId: user.googleId)));
                      Navigator.of(context).pop();
                      if (newResponse.isSuccess) {
                        setState(() {
                          currentUser = currentUser
                              .copyWith(User.named(googleId: user.googleId));
                        });
                      } else {
                        UIHelper.showErrorDialog(context, newResponse.error);
                      }
                    }
                  } else {
                    await GoogleSignIn().signOut();
                    Navigator.of(context).pop();
                    UIHelper.showErrorDialog(context, response.error);
                  }
                }),
        ),
        ListTile(
          leading: buildIcon(AppParams.loginFacebookIcon),
          title: Text("Facebook"),
          trailing: currentUser.facebookId != null
              ? buildCheck()
              : buildAction("Link", () async {
                  UIHelper.showLoadingDialog(context, "Please Wait...");
                  AppResponse<User> response =
                      await LoginController.getFacebookUser();
                  User user = response.data;

                  if (response.isSuccess) {
                    User oldUser = await LoginController.getExistingUser(
                        facebookId: response.data.facebookId);
                    log("Facebook Id ${response.data.facebookId}");
                    if (oldUser != null) {
                      Navigator.of(context).pop();
                      await FacebookLogin().logOut();
                      await UIHelper.showErrorDialog(
                          context, "User already linked with this id");
                    } else {
                      AppResponse<User> newResponse =
                          await LoginController.updateUser(currentUser.copyWith(
                              User.named(facebookId: user.facebookId)));
                      Navigator.of(context).pop();
                      if (newResponse.isSuccess) {
                        setState(() {
                          currentUser = currentUser
                              .copyWith(User.named(googleId: user.facebookId));
                        });
                      } else {
                        UIHelper.showErrorDialog(context, newResponse.error);
                      }
                    }
                  } else {
                    Navigator.of(context).pop();
                    UIHelper.showErrorDialog(context, response.error);
                  }
                }),
        ),
        ListTile(
          leading: Icon(Icons.mobile_screen_share),
          title: Text("Mobile"),
          trailing: currentUser.mobileNo != null
              ? buildCheck()
              : buildAction("Link", () async {
                  bool isUpdated = await Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) => AccountMobileWidget()));
                  isUpdated = isUpdated ?? false;
                  if (isUpdated) {
                    refresh();
                  }
                }),
        )
      ],
    );
  }

  Widget buildIcon(String asset, {Color color}) {
    return Image.asset(
      asset,
      width: 24,
      height: 24,
      color: color,
    );
  }

  Widget buildCheck() {
    return FlatButton(
      onPressed: null,
      child: Icon(
        Icons.check_circle,
        color: Colors.white,
      ),
    );
  }

  Widget buildAction(String label, Function() onPressed) {
    return FlatButton(
        onPressed: onPressed,
        child: Text(
          "$label",
          style: TextStyle(
              color: Colors.blue, fontWeight: FontWeight.w500, fontSize: 14),
        ));
  }

  // Docs

  Widget buildVerifications() {
    Map<String, ValidationDoc> map = currentUser?.govtIds;
    if (map == null) {
      map = {};
    }
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        GradientDivider(
          gradient: AppParams.appBtnGradient,
          height: 5,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 18),
          child: GradientText(
              text: Text(
                "Verification IDs",
                style: TextStyle(fontSize: 16),
              ),
              gradient: AppParams.appTxtGradient),
        ),
        GradientDivider(
          gradient: AppParams.appBtnGradient,
          height: 1,
        ),
        ListTile(
          leading: Icon(Icons.credit_card),
          title: Text("Aadhaar"),
          trailing: map[User.AADHAR_CARD] != null
              ? map[User.AADHAR_CARD].isApproved
                  ? buildCheck()
                  : ApprovalWaitingWidget()
              : buildAction("Link", () async {
                  validateDoc(name: "Aadhaar", id: User.AADHAR_CARD);
                }),
        ),
        ListTile(
          leading: Icon(Icons.credit_card),
          title: Text("Pan Card"),
          trailing: map[User.PAN_CARD] != null
              ? map[User.PAN_CARD].isApproved
                  ? buildCheck()
                  : ApprovalWaitingWidget()
              : buildAction("Link", () async {
                  validateDoc(name: "Pan Card", id: User.PAN_CARD);
                }),
        ),
        ListTile(
          leading: Icon(Icons.credit_card),
          title: Text("Passport"),
          trailing: map[User.PASSPORT] != null
              ? map[User.PASSPORT].isApproved
                  ? buildCheck()
                  : ApprovalWaitingWidget()
              : buildAction("Link", () async {
                  validateDoc(name: "Passport", id: User.PASSPORT);
                }),
        ),
      ],
    );
  }

  Future<void> validateDoc({String name, String id}) async {
    bool result = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => GovtValidationScreen(
              name: name,
              id: id,
            )));
    if (result != null) {
      refresh();
    }
  }

  double get viewWidth => MediaQuery.of(context).size.width;
}
