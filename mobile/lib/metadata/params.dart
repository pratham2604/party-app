import 'package:flutter/material.dart';

class AppParams {
  ///
  /// App
  ///

  static LinearGradient appGradient = LinearGradient(
      colors: [Color(0xffff31c9), Color(0xffff5999), Color(0xffffb530)]);

  static LinearGradient appBtnGradient = LinearGradient(
      colors: [Color(0xffff31c9), Color(0xffff5999), Color(0xffffb530)]);

  static LinearGradient appBtnBrightGradient = LinearGradient(colors: [
    Color(0xffeac92b),
    Color(0xffff00ff),
  ]);

  static LinearGradient appDarkGradient = LinearGradient(colors: [
    Color(0xff540d47),
    Color(0xff543518),
  ]);
  static LinearGradient appTxtGradient =
      LinearGradient(colors: appBtnGradient.colors.reversed.toList());
  static const String appLogo = "assets/Logo.png";
  static const String appMapIcon = "assets/MapIcon.png";
  static const String appBackButton = "assets/Backgradient.png";
  static const String appAddButton = "assets/AddSign(Gradient).png";
  static const String appPartyBackground = "assets/PartyScreen.png";
  static const String appDefaultProfile = "assets/ProfilePic.jpg";
  static const String appLocation = 'assets/Picker.png';

  ///
  /// Splash Screen
  ///

  static const String splashBackground = "assets/Splash.png";
  static const String locationImage = "assets/Location.png";

  ///
  /// Login
  ///
  static const String loginGoogleIcon = "assets/Google.png";
  static const String loginFacebookIcon = "assets/Facebook.png";
  static const String loginGmailIcon = "assets/Gmail.png";

  ///
  /// Event
  ///

  static const String eventBackground = "assets/EventBG.png";

  ///
  /// Favourite
  ///
  static const String heartSelectedIcon = "assets/HeartSelected.png";
  static const String heartUnselectedIcon = "assets/HeartUnselected.png";
}
