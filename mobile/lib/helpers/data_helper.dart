import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';

class DataHelper {
  static List<Color> shuffle(List<Color> input) {
    List<Color> items = List<Color>.from(input);
    var random = new Random();

    // Go through all elements.
    for (var i = items.length - 1; i > 0; i--) {
      // Pick a pseudorandom number according to the list length
      var n = random.nextInt(i + 1);

      var temp = items[i];
      items[i] = items[n];
      items[n] = temp;
    }

    return items;
  }

  static String convertNumberToTwo(int number) {
    if (number < 10) {
      return "0$number";
    }
    return "$number";
  }

  static String getShortCount(int count) {
    if (count < 1000) {
      return "$count";
    }
    return "${count ~/ 1000}K";
  }

  static Map<String, dynamic> convertToJson(json) {
    return jsonDecode(jsonEncode(json));
  }
}
