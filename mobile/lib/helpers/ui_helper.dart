import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UIHelper {
  static Future<void> showAlertDialog(
      BuildContext context, String title, String content) async {
    if (Platform.isAndroid) {
      await showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: Text("$title"),
                content: Text(content),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Ok"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ));
    } else if (Platform.isIOS) {
      await showCupertinoDialog(
          context: context,
          builder: (context) => CupertinoAlertDialog(
                  title: Text("$title"),
                  content: Text(content),
                  actions: <Widget>[
                    CupertinoActionSheetAction(
                      isDefaultAction: true,
                      child: Text("Ok"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    )
                  ]));
    }
  }

  static Future<bool> showConfirmDialog(
    BuildContext context,
    String title,
  ) async {
    bool result = false;
    if (Platform.isAndroid) {
      result = await showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: Text("$title"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Cancel"),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                  ),
                  FlatButton(
                    child: Text("Ok"),
                    onPressed: () {
                      Navigator.of(context).pop(true);
                    },
                  )
                ],
              ));
    } else if (Platform.isIOS) {
      result = await showCupertinoDialog(
          context: context,
          builder: (context) =>
              CupertinoAlertDialog(title: Text("$title"), actions: <Widget>[
                CupertinoActionSheetAction(
                  isDestructiveAction: true,
                  child: Text("Cancel"),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
                CupertinoActionSheetAction(
                  isDefaultAction: true,
                  child: Text("Ok"),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                ),
              ]));
    }
    return result ?? false;
  }

  static Future<void> showErrorDialog(
      BuildContext context, String content) async {
    if (Platform.isAndroid) {
      await showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: Text("Error"),
                content: Text(content),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Ok"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ));
    } else if (Platform.isIOS) {
      await showCupertinoDialog(
          context: context,
          builder: (context) => CupertinoAlertDialog(
                  title: Text("Error"),
                  content: Text(content),
                  actions: <Widget>[
                    CupertinoActionSheetAction(
                      isDefaultAction: true,
                      child: Text("Ok"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    )
                  ]));
    }
  }

  // Loading Dialog with a message
  static Future<void> showLoadingDialog(
      BuildContext context, String message) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0)),
              content: WillPopScope(
                onWillPop: () async {
                  return true;
                },
                child: Container(
                  height: 32,
                  child: Row(
                    children: <Widget>[
                      CircularProgressIndicator(),
                      SizedBox(
                        width: 12.0,
                      ),
                      Expanded(
                        child: Text("$message"),
                      )
                    ],
                  ),
                ),
              ),
            ));
  }
}
