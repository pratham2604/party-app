// Created for checking empty values,
// wrong inputs etc
class InputHelper {
  // Input Constants
  static const String USERNAME = "username";
  static const String PASSWORD = "password";
  static final RegExp _emailRegex = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

  static bool isBlank(var value) {
    return value == null || value.isEmpty;
  }

  static String getBlankValidation(String label) {
    return "Please enter valid $label";
  }

  static bool validateEmail(String email) {
    return _emailRegex.hasMatch(email);
  }
}
