import 'data_helper.dart';
import 'package:intl/intl.dart';

class DateHelper {
  static const List<String> _monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  static const List<String> _shortMonthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];

  static String getMonth(int month) {
    return _monthNames[month % 12];
  }

  static String getShortMonth(int month) {
    return _shortMonthNames[month % 12];
  }

  static String getDateString(DateTime time) {
    Duration diff = DateTime.now().difference(time);
    if (diff.inHours < 24) {
      return "Today";
    }
    //  else if (diff.inHours < 24) {
    //    return "${diff.inHours} hours ago";
    //  }
    else if (diff.inDays == 1) {
      return "Yesterday";
    }
    return DateFormat.yMMMMd('en_US').add_jm().format(time);
  }

  static String getTimeString(DateTime time) {
    int hour = time.hour;
    if (hour != 12) {
      hour = hour % 12;
    }
    return "${DataHelper.convertNumberToTwo(hour)}:${DataHelper.convertNumberToTwo(time.minute)} ${time.hour < 12 ? "am" : "pm"}";
  }

  static String getMonthTimeString(DateTime time) {
    if (time == null) {
      return "";
    }
    return "${getShortMonth(time.month)}.${time.day}.${time.year}";
  }

  static bool isTimeZoneSame(Duration otherZoneOffset) {
    return DateTime.now().timeZoneOffset == otherZoneOffset;
  }
}
