import 'dart:typed_data';
import 'package:flutter/services.dart';

class AssetHelper {
  static Future<Uint8List> getAssetData(String asset) async {
    ByteData fileData = await rootBundle.load(asset);
    return fileData.buffer.asUint8List();
  }
}
