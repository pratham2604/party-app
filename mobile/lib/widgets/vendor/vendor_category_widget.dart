import 'package:flutter/material.dart';
import 'package:party/models/vendor.dart';
import 'package:party/screens/vendor/vendor_explore_screen.dart';
import 'package:party/widgets/common/ease_in_widget.dart';

class VendorCategoryWidget extends StatelessWidget {
  final VendorCategory category;

  const VendorCategoryWidget({Key key, @required this.category})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return EaseInWidget(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) =>
                VendorExploreScreen(query: null, category: category)));
      },
      child: Container(
        height: 150,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Image.network(
              category.image,
              fit: BoxFit.cover,
            ),
            Container(
                decoration: BoxDecoration(color: Colors.black54),
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        "${category.name}",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 28),
                      ),
                    ),
                    Text(
                      "${category.vendorsCount ?? 0} available",
                      style: TextStyle(
                          color: Colors.white.withOpacity(0.8), fontSize: 16),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
