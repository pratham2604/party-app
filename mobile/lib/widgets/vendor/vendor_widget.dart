import 'package:flutter/material.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/vendor.dart';
import 'package:party/screens/vendor/vendor_detail_screen.dart';
import 'package:party/widgets/vendor/vendor_favourite_widget.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:intl/intl.dart';

class VendorWidget extends StatelessWidget {
  final Vendor vendor;

  const VendorWidget({Key key, @required this.vendor}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => VendorDetailScreen(vendor: vendor)));
      },
      child: Stack(
        alignment: Alignment.topRight,
        children: <Widget>[
          Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Image.network(
                      "${vendor.coverImage}",
                      height: 175,
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.width,
                    ),
                  ],
                ),
                Container(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                    decoration:
                        BoxDecoration(gradient: AppParams.appBtnBrightGradient),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Transform.translate(
                          offset: Offset(0, -32),
                          child: Container(
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: Colors.white),
                            padding: EdgeInsets.all(1),
                            child: CircleAvatar(
                              radius: 32,
                              backgroundImage:
                                  NetworkImage("${vendor.profilePic}"),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 12,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Text(
                                    "${vendor.name}",
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.black,
                                        height: 1,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  SizedBox(
                                    width: 6,
                                  ),
                                  SmoothStarRating(
                                    color: Colors.yellow,
                                    allowHalfRating: true,
                                    size: 14,
                                    rating: vendor.rating,
                                    borderColor: Colors.yellow.withOpacity(0.8),
                                  ),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Text(
                                    " |  ${NumberFormat.compactCurrency(name: vendor.currency, decimalDigits: 0).format(vendor.cost.toInt())}",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 6,
                              ),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "${vendor.description}".length < 100
                                          ? "${vendor.description}"
                                          : "${vendor.description?.substring(0, 100)}...",
                                    ),
                                    TextSpan(text: "  "),
                                    TextSpan(
                                        text: "Read More",
                                        style: TextStyle(
                                            color: Colors.orangeAccent,
                                            fontSize: 10,
                                            fontWeight: FontWeight.w500,
                                            decoration:
                                                TextDecoration.underline))
                                  ],
                                  style: TextStyle(
                                      fontSize: 11, color: Colors.black),
                                ),
                                softWrap: true,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                              )
                            ],
                          ),
                        )
                      ],
                    ))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: VendorFavouriteWidget(
              vendorId: vendor.id,
            ),
          )
        ],
      ),
    );
  }
}
