import 'package:flutter/material.dart';
import 'package:party/controllers/favourite_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/widgets/common/gradient_icon_button.dart';

class VendorFavouriteWidget extends StatefulWidget {
  final String vendorId;
  const VendorFavouriteWidget({Key key, this.vendorId}) : super(key: key);
  @override
  _VendorFavouriteWidgetState createState() => _VendorFavouriteWidgetState();
}

class _VendorFavouriteWidgetState extends State<VendorFavouriteWidget> {
  bool isFavourite;

  bool isEnabled;

  @override
  void initState() {
    super.initState();
    isEnabled = false;
    isFavourite = false;
    refresh();
  }

  Future<void> refresh() async {
    AppResponse<bool> response =
        await FavouriteController.isVendorFavourite(widget.vendorId);
    if (!mounted) {
      return;
    }
    if (response.isSuccess) {
      setState(() {
        isFavourite = response.data;
      });
      isEnabled = true;
    } else {
      setState(() {
        isFavourite = false;
        isEnabled = false;
      });
    }
  }

  Future<void> toggleFavourite() async {
    AppResponse<bool> response;
    setState(() {
      isEnabled = false;
    });
    if (isFavourite) {
      response =
          await FavouriteController.removeVendorFavourite(widget.vendorId);
    } else {
      response = await FavouriteController.setVendorFavourite(widget.vendorId);
    }
    if (mounted) {
      if (response.isSuccess)
        setState(() {
          isFavourite = !isFavourite;
          isEnabled = true;
        });
      else {
        UIHelper.showErrorDialog(context, response.error);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: isEnabled ? 1.0 : 0.6,
      child: GradientIconButton(
        onPressed: isEnabled ? toggleFavourite : null,
        asset: isFavourite
            ? AppParams.heartSelectedIcon
            : AppParams.heartUnselectedIcon,
      ),
    );
  }
}
