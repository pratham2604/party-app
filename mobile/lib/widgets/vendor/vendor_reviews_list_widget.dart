import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:party/controllers/review_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/review.dart';
import 'package:party/models/vendor.dart';
import 'package:party/screens/vendor/vendor_review_screen.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/ease_in_widget.dart';
import 'package:party/widgets/common/gradient_divider.dart';
import 'package:party/widgets/vendor/vendor_review_widget.dart';

class VendorReviewsListWidget extends StatefulWidget {
  final Vendor vendor;

  const VendorReviewsListWidget({Key key, @required this.vendor})
      : super(key: key);
  @override
  _VendorReviewsListWidgetState createState() =>
      _VendorReviewsListWidgetState();
}

class _VendorReviewsListWidgetState extends State<VendorReviewsListWidget> {
  // Variables
  bool isLoading;
  List<VendorReview> reviews;

  @override
  void initState() {
    super.initState();
    reviews = [];
    refresh();
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    AppResponse<List<VendorReview>> result =
        await ReviewController.getVendorReviews(vendor.id);
    setState(() {
      isLoading = false;
    });
    if (result.isSuccess) {
      setState(() {
        reviews = result.data;
      });
    } else {
      UIHelper.showErrorDialog(context, result.error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: 56,
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: GradientText("Reviews",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
                    gradient: AppParams.appTxtGradient),
              ),
              Center(
                child: EaseInWidget(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => VendorReviewScreen(
                              vendor: widget.vendor,
                            )));
                  },
                  child: OutlineGradientButton(
                    child: Text(
                      "Write A Review".toUpperCase(),
                      style: TextStyle(
                          fontSize: 13, color: Colors.white.withOpacity(0.7)),
                    ),
                    gradient: AppParams.appBtnBrightGradient,
                    strokeWidth: 1.5,
                    radius: Radius.circular(2),
                    padding: EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                  ),
                ),
              ),
            ],
          ),
        ),
        GradientDivider(gradient: AppParams.appBtnBrightGradient),
        isLoading
            ? Center(
                child: AppProgressIndicator(),
              )
            : ListView.builder(
                itemBuilder: (context, index) {
                  VendorReview review = reviews[index];
                  return VendorReviewWidget(review: review);
                },
                shrinkWrap: true,
                itemCount: reviews.length,
                physics: ClampingScrollPhysics(),
              )
      ],
    );
  }

  Vendor get vendor => widget.vendor;
}
