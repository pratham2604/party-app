import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LocationPickupMap extends StatefulWidget {
  final Function(LatLng) onPositionChanged;
  final Function(LocationPickController) controllerCompleted;
  const LocationPickupMap({
    Key key,
    this.onPositionChanged,
    this.controllerCompleted,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LocationPickupMapState();
}

class _LocationPickupMapState extends State<LocationPickupMap> {
  final LatLng storeLocation = LatLng(12.910824, 77.651693);
  final Set<Marker> _markers = Set<Marker>();
  int currentIndex = 0;
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _mapController;
  CameraPosition _position = CameraPosition(
      target: LatLng(
    0,
    0,
  ));
  Marker marker;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("Markers ${_markers.length}");
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: GoogleMap(
        mapType: MapType.normal,
        myLocationEnabled: true,
        onCameraIdle: () {
          if (searching) {
            widget.onPositionChanged(_position.target);
            searching = false;
          }
        },
        myLocationButtonEnabled: false,
        initialCameraPosition: _position,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
          _mapController = controller;
          widget.controllerCompleted(LocationPickController._(this));
          addMarkerIcon();
        },
        markers: _markers,
        onCameraMove: _onCameraMove,
      ),
    );
  }

  bool searching = false;
  void _onCameraMove(CameraPosition position) {
    _position = position;
    _markers.clear();
    _markers.add(marker.copyWith(positionParam: _position.target));
    setState(() {
      searching = true;
    });
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    Codec codec = await instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(
      format: ImageByteFormat.png,
    ))
        .buffer
        .asUint8List();
  }

  void addMarkerIcon() async {
    String asset = 'assets/picker.png';
    final Uint8List markerIcon = await getBytesFromAsset(asset, 100);
    _markers.clear();

    marker = Marker(
        zIndex: 1.0,
        markerId: MarkerId("23"),
        position: _position.target,
        icon: BitmapDescriptor.fromBytes(markerIcon),
        draggable: true,
        infoWindow: InfoWindow(title: "Your Location"));
    _markers.add(marker);
    Marker store = await getStoreMarker();
    _markers.add(store);
    setState(() {});
  }

  Future<Marker> getStoreMarker() async {
    String asset = "assets/icon.png";
    final Uint8List markerIcon = await getBytesFromAsset(asset, 100);
    final Marker marker = Marker(
        zIndex: 1.0,
        markerId: MarkerId("24"),
        position: storeLocation,
        icon: BitmapDescriptor.fromBytes(markerIcon),
        infoWindow: InfoWindow(title: "AtomADay"));
    return marker;
  }
}

class LocationPickController {
  final _LocationPickupMapState state;
  LocationPickController._(this.state);
  void moveTo(LatLng position) {
    if (this.state._mapController != null) {
      this.state._position = CameraPosition(target: position, zoom: 10.0);
      this
          .state
          ._mapController
          .moveCamera(CameraUpdate.newCameraPosition(this.state._position));
    }
    this.state.addMarkerIcon();
  }
}
