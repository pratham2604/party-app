import 'package:flutter/material.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/controllers/user_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/user.dart';
import 'package:party/widgets/common/app_button.dart';
import 'package:party/widgets/common/app_logo.dart';
import 'package:party/widgets/login/login_input_widget.dart';

class AccountEmailWidget extends StatefulWidget {
  @override
  _AccountEmailWidgetState createState() => _AccountEmailWidgetState();
}

class _AccountEmailWidgetState extends State<AccountEmailWidget> {
  FocusNode emailNode;
  FocusNode passwordNode;

  // Variables
  String email;
  String password;

  @override
  void initState() {
    super.initState();
    emailNode = FocusNode();
    passwordNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppParams.appPartyBackground),
              fit: BoxFit.fill),
        ),
        child: Stack(
          alignment: Alignment.topLeft,
          children: <Widget>[
            Center(
              child: SingleChildScrollView(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 2 / 3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(height: 0, child: AppLogo()),
                      SizedBox(
                        height: 16,
                      ),
                      LoginInputWidget(
                        label: "Email",
                        onChanged: (val) {
                          setState(() {
                            email = val;
                          });
                        },
                        onSubmitted: (val) {
                          emailNode.unfocus();
                          passwordNode.requestFocus();
                        },
                        focusNode: emailNode,
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      LoginInputWidget(
                        label: "Password",
                        onChanged: (val) {
                          password = val;
                        },
                        isObsecure: true,
                        focusNode: passwordNode,
                      ),
                      SizedBox(
                        height: 36,
                      ),
                      AppButton(
                          label: "Update",
                          onPressed: () async {
                            UIHelper.showLoadingDialog(context, "Updating...");
                            User currentUser;
                            User oldUser;
                            await Future.wait([
                              Future(() async {
                                AppResponse<User> response =
                                    await UserController.getCurrentUser();
                                if (response.isSuccess) {
                                  currentUser = response.data;
                                }
                              }),
                              Future(() async {
                                User temp =
                                    await LoginController.getExistingUser(
                                        email: email);
                                if (temp != null) {
                                  oldUser = temp;
                                }
                              })
                            ]);
                            if (currentUser == null) {
                              Navigator.of(context).pop();
                              await UIHelper.showErrorDialog(
                                  context, "You are not logged in");
                              Navigator.of(context).pop(false);
                              return;
                            }
                            if (oldUser != null) {
                              Navigator.of(context).pop();
                              await UIHelper.showErrorDialog(
                                  context, "User with email already exists");
                              Navigator.of(context).pop(false);
                              return;
                            }
                            User newUser = currentUser.copyWith(User.named(
                              email: email,
                              password: password,
                            ));
                            AppResponse<User> newResponse =
                                await LoginController.updateUser(newUser);
                            Navigator.of(context).pop();
                            if (newResponse.isSuccess) {
                              Navigator.of(context).pop(true);
                            } else {
                              Navigator.of(context).pop(false);
                            }
                          })
                    ],
                  ),
                ),
              ),
            ),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
