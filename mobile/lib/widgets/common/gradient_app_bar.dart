import 'package:flutter/material.dart';
import 'package:party/metadata/params.dart';

class GradientAppBar extends StatefulWidget with PreferredSizeWidget {
  final String title;
  final bool showLeading;
  final Function() onPopBack;

  GradientAppBar(
      {Key key, @required this.title, this.showLeading = false, this.onPopBack})
      : super(key: key);
  @override
  _GradientAppBarState createState() => _GradientAppBarState();

  @override
  Size get preferredSize => Size.fromHeight(64);
}

class _GradientAppBarState extends State<GradientAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.black),
      child: SafeArea(
        top: widget.showLeading,
        child: Container(
          height: 64,
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              Color(0xff540d47),
              Color(0xff543518),
            ]),
          ),
          child: Row(
            children: <Widget>[
              widget.showLeading
                  ? InkWell(
                      onTap: () {
                        if (widget.onPopBack == null) {
                          Navigator.of(context).pop();
                        } else {
                          widget.onPopBack();
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          AppParams.appBackButton,
                          width: 24,
                          height: 24,
                        ),
                      ))
                  : Container(),
              Expanded(
                child: Text(
                  "${widget.title}",
                  style: TextStyle(color: Colors.white, fontSize: 18),
                  textAlign: TextAlign.center,
                ),
              ),
              widget.showLeading
                  ? Container(
                      width: 24,
                      height: 24,
                      padding: const EdgeInsets.all(8.0),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
