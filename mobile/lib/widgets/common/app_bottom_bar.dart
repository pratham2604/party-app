import 'package:flutter/material.dart';

class AppBottomBar extends StatefulWidget {
  final Function(BottomBarState) onSelected;
  final BottomBarState current;

  const AppBottomBar(
      {Key key, @required this.onSelected, this.current = BottomBarState.Event})
      : super(key: key);
  @override
  _AppBottomBarState createState() => _AppBottomBarState();
}

class _AppBottomBarState extends State<AppBottomBar> {
  List<BottomBarOption> options;

  @override
  void initState() {
    super.initState();
    options = [
      BottomBarOption.named(
          icon: AssetImage("assets/EventUnselected.png"),
          label: "Events",
          state: BottomBarState.Event),
      BottomBarOption.named(
          icon: AssetImage("assets/SearchUnslected.png"),
          label: "Search",
          state: BottomBarState.Search),
      BottomBarOption.named(
          icon: AssetImage("assets/GalleryUnselected.png"),
          label: "Feed",
          state: BottomBarState.Feed),
      BottomBarOption.named(
          icon: AssetImage("assets/ProfileUnselected.png"),
          label: "Profile",
          state: BottomBarState.Profile),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 6),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24), topRight: Radius.circular(24)),
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(begin: Alignment.bottomLeft, colors: [
            Color(0xff2f0827),
            Color(0xff13070b),
            Color(0xff3b2d0a)
          ])),
          height: 70,
          child: Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: List<BottomBarOption>.from(options)
                  .map((val) => SizedBox(
                        width: MediaQuery.of(context).size.width /
                            (val.state == widget.current ? 3 : 5),
                        child: Center(
                          child: _AppBottomBarOptionWidget(
                            bottomBarOption: val,
                            isSelected: widget.current == val.state,
                            onTap: () {
                              widget.onSelected(val.state);
                            },
                          ),
                        ),
                      ))
                  .toList(),
            ),
          ),
        ),
      ),
    );
  }
}

class _AppBottomBarOptionWidget extends StatefulWidget {
  final BottomBarOption bottomBarOption;
  final bool isSelected;
  final Function() onTap;

  const _AppBottomBarOptionWidget(
      {Key key, this.bottomBarOption, this.isSelected = false, this.onTap})
      : super(key: key);
  @override
  __AppBottomBarOptionWidgetState createState() =>
      __AppBottomBarOptionWidgetState();
}

class __AppBottomBarOptionWidgetState extends State<_AppBottomBarOptionWidget> {
  bool isExpanded;

  @override
  void initState() {
    super.initState();
    isExpanded = false;
    WidgetsBinding.instance.addPersistentFrameCallback((duration) async {
      await Future.delayed(Duration(milliseconds: 100));
      if (mounted)
        setState(() {
          isExpanded = true;
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        double width = constraints.biggest.width;
        double size = 24;
        double containerHeight = size * 2;
        return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          physics: NeverScrollableScrollPhysics(),
          child: GestureDetector(
            onTap: widget.onTap,
            child: AnimatedContainer(
              padding:
                  EdgeInsets.symmetric(horizontal: widget.isSelected ? 12 : 0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(
                      widget.isSelected ? containerHeight : 0),
                  color: widget.isSelected
                      ? Colors.white.withOpacity(0.2)
                      : Colors.transparent),
              duration: Duration(milliseconds: 250),
              height: containerHeight,
              width: width - 24,
              child: SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      width: size,
                      height: size,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: widget.bottomBarOption.icon,
                              fit: BoxFit.fill)),
                    ),
                    AnimatedContainer(
                      width: widget.isSelected ? (width - size - 24) : 0,
                      duration: Duration(milliseconds: 0),
                      child: Text(
                        " ${widget.bottomBarOption.label}",
                        maxLines: 1,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class BottomBarOption {
  final String label;
  final ImageProvider icon;
  final Color color;
  final BottomBarState state;

  BottomBarOption(this.label, this.icon, this.color, this.state);

  BottomBarOption.named({
    this.label,
    this.icon,
    this.color,
    this.state,
  });
}

enum BottomBarState { Event, Search, Feed, Profile }
