import 'package:flutter/material.dart';

class ApprovalWaitingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.yellow,
        borderRadius: BorderRadius.circular(4),
      ),
      padding: EdgeInsets.all(4),
      child: Text(
        "Approval Waiting",
        style: TextStyle(color: Colors.black, fontSize: 12),
      ),
    );
  }
}
