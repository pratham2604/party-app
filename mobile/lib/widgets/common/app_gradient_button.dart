import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:party/widgets/common/ease_in_widget.dart';

class AppGradientButton extends StatelessWidget {
  final String label;
  final Gradient gradient;
  final TextStyle labelStyle;
  final bool isOutline;
  final Function onTap;
  final EdgeInsets padding;

  const AppGradientButton(
      {Key key,
      @required this.label,
      @required this.gradient,
      @required this.labelStyle,
      this.isOutline = true,
      @required this.onTap,
      this.padding})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    double radius = 4;
    EdgeInsets newPadding = EdgeInsets.symmetric(vertical: 4, horizontal: 12);
    if (padding != null) {
      newPadding = padding;
    }
    return EaseInWidget(
      onTap: onTap,
      child: isOutline
          ? OutlineGradientButton(
              padding: newPadding,
              radius: Radius.circular(radius),
              child: Center(
                  child: GradientText("$label",
                      gradient: gradient, style: labelStyle)),
              strokeWidth: 1,
              gradient: gradient,
            )
          : Container(
              padding: newPadding,
              decoration: BoxDecoration(
                  gradient: gradient,
                  borderRadius: BorderRadius.circular(radius)),
              child: Center(child: Text("$label", style: labelStyle)),
            ),
    );
  }
}
