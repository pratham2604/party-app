import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:ui' as ui;

class CustomMap extends StatefulWidget {
  final Function(CustomMapData data) onTap;
  final Function(LatLng) onPositionChanged;
  final Function(CustomMapController) onControllerCompleted;
  final LatLng initialPosition;
  const CustomMap(
      {Key key,
      @required this.onControllerCompleted,
      @required this.onTap,
      this.initialPosition,
      this.onPositionChanged})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _CustomMapState();
}

class _CustomMapState extends State<CustomMap> {
  final List<CustomMapData> _data = [];
  final Set<Marker> _markers = Set<Marker>();
  int currentIndex = 0;
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController _mapController;
  LatLng _position = LatLng(20.5937, 78.9629);

  @override
  void initState() {
    super.initState();
    _position = widget.initialPosition ?? _position;
  }

  @override
  Widget build(BuildContext context) {
    try {
      return Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
            Expanded(
              child: GoogleMap(
                mapToolbarEnabled: false,
                mapType: MapType.normal,
                myLocationEnabled: true,
                myLocationButtonEnabled: false,
                initialCameraPosition:
                    CameraPosition(target: _position, zoom: 3),
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);

                  _mapController = controller;
                  _mapController.setMapStyle(jsonEncode([
                    {
                      "elementType": "geometry",
                      "stylers": [
                        {"color": "#f5f5f5"}
                      ]
                    },
                    {
                      "featureType": "road",
                      "elementType": "geometry",
                      "stylers": [
                        {"color": "#ffffff"}
                      ]
                    },
                    {
                      "featureType": "water",
                      "elementType": "geometry",
                      "stylers": [
                        {"color": "#c9c9c9"}
                      ]
                    },
                    {
                      "featureType": "water",
                      "elementType": "labels.text.fill",
                      "stylers": [
                        {"color": "#9e9e9e"}
                      ]
                    }
                  ]));
                  widget.onControllerCompleted(CustomMapController._(this));
                  setState(() {});
                },
                onCameraIdle: () {
                  widget.onPositionChanged(_position);
                },
                markers: _markers,
                buildingsEnabled: true,
                onCameraMove: _onCameraMove,
              ),
            ),
            _mapController == null
                ? LinearProgressIndicator()
                : Container(
                    height: 2,
                  ),
          ],
        ),
      );
    } catch (e) {
      return Scaffold(
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Please try again"),
          ),
        ),
      );
    }
  }

  void moveToPosition(LatLng position) {
    _mapController.moveCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: position, zoom: 8.0)));
  }

  void _onCameraMove(CameraPosition position) {
    _position = position.target;
  }

  _showMarkers() async {
    _markers.clear();
    for (int i = 0; i < _data.length; i++) {
      CustomMapData value = _data[i];
      String asset = value.asset;
      final Uint8List markerIcon = await getBytesFromAsset(asset, 100);
      BitmapDescriptor descriptor = BitmapDescriptor.fromBytes(markerIcon);
      _markers.add(Marker(
        zIndex: currentIndex == i ? 10.0 : 1.0,
        markerId: MarkerId("${value.id}"),
        position: LatLng(value.position.latitude, value.position.longitude),
        infoWindow: InfoWindow(
          title: value.title,
        ),
        icon: descriptor,
        onTap: () => widget.onTap(value),
      ));
    }
    if (mounted) setState(() {});
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  void _clearData() {
    if (mounted) {
      setState(() {
        this._data.clear();
      });
    }
  }

  void _addData(List<CustomMapData> data) {
    _clearData();
    if (mounted) {
      setState(() {
        this._data.addAll(data);
      });
    }
    _showMarkers();
  }
}

class CustomMapController {
  final _CustomMapState _customMapState;

  CustomMapController._(this._customMapState);

  void addData(List<CustomMapData> services) {
    _customMapState._addData(services);
  }

  void moveToMarker(int index) {
    _customMapState.currentIndex = index;
    Marker marker = _customMapState._markers.elementAt(index);
    _customMapState._mapController.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: marker.position, zoom: 11.0)));
  }

  void moveToLocation(LatLng position) {
    _customMapState._position = position;
    log(
      "Position ${position.latitude}",
    );
    _customMapState._mapController.moveCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: position, zoom: 14.0)));
  }

  Future<LatLngBounds> getBounds() async {
    LatLngBounds bounds =
        await _customMapState._mapController.getVisibleRegion();
    return bounds;
  }
}

class CustomMapData {
  final String title;
  final String id;
  final LatLng position;
  final String asset;

  CustomMapData(this.id, this.title, this.position, this.asset);

  @override
  bool operator ==(other) => position == other.position;

  @override
  int get hashCode => position.hashCode;
}
