import 'package:flutter/cupertino.dart';
import 'package:party/metadata/params.dart';

class AppLogo extends StatefulWidget {
  @override
  _AppLogoState createState() => _AppLogoState();
}

class _AppLogoState extends State<AppLogo> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        double height = constraints.biggest.height;
        return Image.asset(
          AppParams.appLogo,
          height: height != double.infinity ? height : 100,
        );
      },
    );
  }
}
