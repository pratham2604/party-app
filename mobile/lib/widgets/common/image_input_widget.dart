import 'dart:io';
import 'package:flutter/material.dart';
import 'package:party/widgets/common/image_picker_widget.dart';

class ImageInputWidget extends StatefulWidget {
  final Function(List<File>) onImagesAdded;
  final int maxCount;

  const ImageInputWidget(
      {Key key, @required this.onImagesAdded, this.maxCount = 10})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _ImageInputWidgetState();
}

class _ImageInputWidgetState extends State<ImageInputWidget> {
  List<Widget> images;
  List<File> files;
  @override
  void initState() {
    super.initState();
    images = [];
    files = [];
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0),
          child: Text(
            "Add Photo".toUpperCase(),
            style:
                TextStyle(fontSize: 14.0, color: Colors.white.withOpacity(0.7)),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0),
          child: Text(
            "Thumbnail".toUpperCase(),
            style:
                TextStyle(fontSize: 12.0, color: Colors.white.withOpacity(0.7)),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 8.0),
          child: Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: getImages(),
          ),
        ),
      ],
    );
  }

  List<Widget> getImages() {
    List<Widget> widgets = [];
    if (widget.maxCount > files.length)
      widgets.add(ImagePickerWidget(
        onImagePicked: (file) {
          setState(() {
            files.add(file);
          });
          widget.onImagesAdded(files);
        },
      ));
    for (int i = 0; i < files.length; i++) {
      widgets.add(createImage(files[i], i));
    }

    return widgets;
  }

  Widget createImage(var file, int index) {
    return Padding(
      padding: const EdgeInsets.only(right: 12.0),
      child: Stack(
        alignment: Alignment.topRight,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
              child: Image.file(
                file,
                width: 72.0,
                height: 72.0,
                fit: BoxFit.fill,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                this.files.removeAt(index);
                widget.onImagesAdded(files);
              });
            },
            child: Container(
              child: Icon(
                Icons.clear,
                size: 18.0,
                color: Colors.black,
              ),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                      color: Colors.black26,
                      style: BorderStyle.solid,
                      width: 1.0),
                  borderRadius: BorderRadius.all(Radius.circular(36.0))),
              padding: EdgeInsets.all(8.0),
            ),
          )
        ],
      ),
    );
  }
}
