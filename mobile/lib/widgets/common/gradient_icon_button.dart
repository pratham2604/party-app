import 'package:flutter/material.dart';
import 'package:party/metadata/params.dart';
import 'package:party/widgets/common/ease_in_widget.dart';

class GradientIconButton extends StatelessWidget {
  final Function() onPressed;
  final String asset;
  final double size;
  final Color color;
  final Padding padding;

  const GradientIconButton(
      {Key key,
      @required this.onPressed,
      @required this.asset,
      this.size = 32,
      this.color,
      this.padding})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return EaseInWidget(
      onTap: onPressed,
      child: Container(
        height: size,
        width: size,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: AppParams.appBtnGradient,
        ),
        padding: padding ?? EdgeInsets.all(9),
        child: Image.asset(
          asset,
          fit: BoxFit.fill,
          color: Colors.white,
        ),
      ),
    );
  }
}
