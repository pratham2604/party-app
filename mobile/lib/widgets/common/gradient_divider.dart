import 'package:flutter/material.dart';

class GradientDivider extends StatelessWidget {
  final Gradient gradient;
  final double height;

  const GradientDivider({Key key, @required this.gradient, this.height = 1})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Container(
          height: height,
          width: constraints.biggest.width,
          decoration: BoxDecoration(gradient: gradient),
        );
      },
    );
  }
}
