import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerWidget extends StatelessWidget {
  final Function onImagePicked;

  const ImagePickerWidget({Key key, this.onImagePicked}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => getImage(context),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(4.0),
            ),
            color: Colors.white.withOpacity(0.2)),
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Icon(
            Icons.add,
          ),
        ),
      ),
    );
  }

  void getImage(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.camera),
                title: Text("Camera"),
                onTap: () {
                  Navigator.of(context).pop();
                  pickImage(ImageSource.camera);
                },
              ),
              ListTile(
                leading: Icon(Icons.photo),
                title: Text("Gallery"),
                onTap: () {
                  Navigator.of(context).pop();
                  pickImage(ImageSource.gallery);
                },
              )
            ],
          );
        });
  }

  void pickImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(source: source);
    if (image != null) {
      onImagePicked(image);
    }
  }
}
