import 'package:flutter/material.dart';
import 'package:party/metadata/params.dart';
import 'package:party/widgets/common/ease_in_widget.dart';

class AppButton extends StatelessWidget {
  final String label;
  final Function() onPressed;
  final TextStyle textStyle;

  const AppButton(
      {Key key, @required this.label, @required this.onPressed, this.textStyle})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: EaseInWidget(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
            decoration: BoxDecoration(
                gradient: AppParams.appBtnGradient,
                borderRadius: BorderRadius.circular(6)),
            child: Text(
              "$label",
              style: textStyle ??
                  TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.normal),
              textAlign: TextAlign.center,
            ),
          ),
          onTap: onPressed),
    );
  }
}
