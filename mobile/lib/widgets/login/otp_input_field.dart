import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OTPInputField extends StatefulWidget {
  final int length;

  const OTPInputField({Key key, this.length = 6}) : super(key: key);
  @override
  _OTPInputFieldState createState() => _OTPInputFieldState();
}

class _OTPInputFieldState extends State<OTPInputField> {
  List<FocusNode> focusNodes;

  @override
  void initState() {
    super.initState();
    focusNodes = List<FocusNode>.generate(widget.length, (index) => FocusNode())
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Row(
        children: List<Widget>.generate(
            widget.length,
            (index) => Expanded(
                    child: Container(
                  margin: EdgeInsets.only(right: 8),
                  child: TextField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    focusNode: focusNodes[index],
                    maxLength: 1,
                    textAlign: TextAlign.center,
                    onChanged: (val) {
                      if (val.length == 1) {
                        focusNodes[index].nextFocus();
                      } else {
                        focusNodes[index].previousFocus();
                      }
                    },
                    obscureText: true,
                    decoration: InputDecoration(counter: Container()),
                  ),
                ))).toList(),
      );
    });
  }
}
