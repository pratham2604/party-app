import 'package:flutter/material.dart';
import 'package:party/widgets/common/ease_in_widget.dart';

class LoginSocialButton extends StatelessWidget {
  final ImageProvider imageProvider;
  final String label;
  final Function() onPressed;
  final Color color;
  final TextStyle labelStyle;

  const LoginSocialButton(
      {Key key,
      @required this.imageProvider,
      @required this.label,
      @required this.onPressed,
      @required this.color,
      this.labelStyle})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return EaseInWidget(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 12, horizontal: 12),
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(4),
          ),
          child: Row(
            children: <Widget>[
              SizedBox(
                width: 24,
              ),
              Container(
                height: 24,
                width: 24,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    image: DecorationImage(
                      image: imageProvider,
                    )),
              ),
              SizedBox(
                width: 12,
              ),
              Expanded(
                child: Text(
                  "$label",
                  style: labelStyle.copyWith(fontSize: 12),
                ),
              )
            ],
          ),
        ),
        onTap: onPressed);
  }
}
