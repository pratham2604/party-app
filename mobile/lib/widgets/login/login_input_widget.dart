import 'package:flutter/material.dart';

class LoginInputWidget extends StatefulWidget {
  final String label;
  final Function(dynamic) onSubmitted;
  final Function(dynamic) onChanged;
  final FocusNode focusNode;
  final bool isObsecure;

  const LoginInputWidget(
      {Key key,
      @required this.label,
      this.onSubmitted,
      this.focusNode,
      this.onChanged,
      this.isObsecure = false})
      : super(key: key);
  @override
  _LoginInputWidgetState createState() => _LoginInputWidgetState();
}

class _LoginInputWidgetState extends State<LoginInputWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4), color: Colors.white),
      child: TextField(
          textAlign: TextAlign.center,
          onChanged: widget.onChanged,
          obscureText: widget.isObsecure,
          onSubmitted: widget.onSubmitted,
          focusNode: widget.focusNode,
          style: TextStyle(
            fontSize: 14,
            color: Colors.black,
          ),
          decoration: InputDecoration(
              border: InputBorder.none,
              hintText: widget.label,
              hintStyle: TextStyle(
                color: Colors.black,
              ))),
    );
  }
}
