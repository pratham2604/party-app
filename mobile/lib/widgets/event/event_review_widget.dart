import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:party/controllers/user_controller.dart';
import 'package:party/helpers/date_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/review.dart';
import 'package:party/models/user.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class EventReviewWidget extends StatelessWidget {
  final EventReview review;

  const EventReviewWidget({Key key, @required this.review}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          buildUser(),
          SizedBox(
            height: 10,
          ),
          Text(
            "${review.title}",
            style: TextStyle(
                color: Colors.white.withOpacity(0.6),
                fontSize: 13,
                height: 1.2),
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            "${review.description}",
            style: TextStyle(
                color: Colors.white.withOpacity(0.5),
                fontSize: 11,
                height: 1.2),
          ),
        ],
      ),
    );
  }

  Widget buildUser() {
    UserMeta user = review.user;
    return Builder(
      builder: (context) {
        return Row(
          children: <Widget>[
            CircleAvatar(
              radius: 18,
              backgroundImage: NetworkImage(
                  "${review.user.profilePic ?? UserController.getDefaultPic(review.user.name)}"),
            ),
            SizedBox(
              width: 12,
            ),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GradientText(
                    user.name,
                    gradient: AppParams.appTxtGradient,
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  GradientText(
                    "PWM member since ${DateHelper.getMonthTimeString(user.createdAt)}",
                    gradient: AppParams.appTxtGradient,
                    style: TextStyle(fontSize: 8, fontWeight: FontWeight.w300),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SmoothStarRating(
                        rating: review.rating,
                        color: Colors.yellow,
                        size: 10,
                      ),
                      SizedBox(
                        width: 6,
                      ),
                      (review.isSelf && !review.approved)
                          ? Opacity(
                              opacity: 0.4,
                              child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.yellow,
                                      borderRadius: BorderRadius.circular(12)),
                                  padding: EdgeInsets.all(6),
                                  child: Text(
                                    "Approval Pending",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 10),
                                  )),
                            )
                          : Container()
                    ],
                  )
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
