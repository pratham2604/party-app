import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:party/controllers/event_controller.dart';
import 'package:party/helpers/date_helper.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/event.dart';
import 'package:party/models/review.dart';
import 'package:party/screens/event/event_review_screen.dart';
import 'package:party/widgets/common/app_progress_indicator.dart';
import 'package:party/widgets/common/ease_in_widget.dart';
import 'package:party/widgets/common/gradient_divider.dart';
import 'package:party/widgets/event/event_review_widget.dart';

class EventReviewsListWidget extends StatefulWidget {
  final Event event;
  final bool canReview;

  const EventReviewsListWidget(
      {Key key, @required this.event, this.canReview = false})
      : super(key: key);
  @override
  _EventReviewsListWidgetState createState() => _EventReviewsListWidgetState();
}

class _EventReviewsListWidgetState extends State<EventReviewsListWidget> {
  // Variables
  bool isLoading;
  List<EventReview> reviews;

  @override
  void initState() {
    super.initState();
    refresh();
  }

  Future<void> refresh() async {
    setState(() {
      isLoading = true;
    });
    AppResponse<List<EventReview>> result =
        await EventController.getReviews(event.id);
    setState(() {
      isLoading = false;
    });
    if (result.isSuccess) {
      setState(() {
        reviews = result.data;
      });
    } else {
      UIHelper.showErrorDialog(context, result.error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          height: 12,
        ),
        Container(
          decoration: BoxDecoration(
            gradient: AppParams.appBtnBrightGradient,
          ),
          height: 6,
        ),
        Container(
          height: 56,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Container(
                  alignment: Alignment.center,
                  child: GradientText("Reviews",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
                      gradient: AppParams.appTxtGradient),
                ),
              ),
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "${DateHelper.getMonthTimeString(event.performanceAt)}",
                      style: TextStyle(
                          fontSize: 14, color: Colors.white.withOpacity(0.7)),
                    ),
                    SizedBox(
                      width: 24,
                    ),
                    !widget.canReview
                        ? Container()
                        : EaseInWidget(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => EventReviewScreen(
                                        event: widget.event,
                                      )));
                            },
                            child: OutlineGradientButton(
                              child: Text(
                                "Write A Review".toUpperCase(),
                                style: TextStyle(
                                    fontSize: 13,
                                    color: Colors.white.withOpacity(0.7)),
                              ),
                              gradient: AppParams.appBtnBrightGradient,
                              strokeWidth: 1.5,
                              radius: Radius.circular(2),
                              padding: EdgeInsets.symmetric(
                                  vertical: 6, horizontal: 10),
                            ),
                          ),
                    SizedBox(
                      width: 24,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        GradientDivider(gradient: AppParams.appBtnBrightGradient),
        isLoading
            ? Center(
                child: AppProgressIndicator(),
              )
            : ListView.builder(
                itemBuilder: (context, index) {
                  EventReview review = reviews[index];
                  return EventReviewWidget(review: review);
                },
                shrinkWrap: true,
                itemCount: reviews?.length ?? 0,
                physics: ClampingScrollPhysics(),
              )
      ],
    );
  }

  Event get event => widget.event;
}
