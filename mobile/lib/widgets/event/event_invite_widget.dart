import 'package:flutter/material.dart';
import 'package:party/helpers/date_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/event_invite.dart';
import 'package:party/models/user.dart';
import 'package:party/widgets/common/app_gradient_button.dart';

class EventInviteWidget extends StatelessWidget {
  final EventInvite eventInvite;
  final Function(String id, bool accepted) onTap;

  const EventInviteWidget(
      {Key key, @required this.eventInvite, @required this.onTap})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 16),
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.white30, width: 1),
          borderRadius: BorderRadius.circular(12)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          buildRow("Event Name", eventInvite.eventName),
          SizedBox(
            height: 8,
          ),
          buildRow("User Id", eventInvite.leader.id),
          SizedBox(
            height: 8,
          ),
          buildRow("User Name", eventInvite.leader.name),
          SizedBox(
            height: 8,
          ),
          eventInvite.friends == null
              ? Container()
              : buildRow("Guest Count", "${eventInvite.friends.length}",
                  action: InkWell(
                    onTap: () {
                      showModalBottomSheet(
                          context: context,
                          builder: (context) => Scaffold(
                                appBar: AppBar(
                                  leading: CloseButton(),
                                  centerTitle: true,
                                  title: Text("Guests"),
                                ),
                                body: ListView.separated(
                                  itemBuilder: (context, index) {
                                    UserMeta meta = eventInvite.friends[index];
                                    return ListTile(
                                      title: Text("${meta.name}"),
                                      subtitle: Text("${meta.id}"),
                                      leading: CircleAvatar(
                                        radius: 18,
                                        backgroundImage:
                                            NetworkImage("${meta.profilePic}"),
                                      ),
                                    );
                                  },
                                  separatorBuilder: (context, index) =>
                                      Divider(),
                                  itemCount: eventInvite.friends.length,
                                  shrinkWrap: true,
                                ),
                              ));
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text(
                        "View List",
                        style: TextStyle(
                            color: Colors.blue,
                            decoration: TextDecoration.underline,
                            fontSize: 14),
                      ),
                    ),
                  )),
          eventInvite.friends == null
              ? Container()
              : SizedBox(
                  height: 8,
                ),
          buildRow("Sent At",
              "${DateHelper.getMonthTimeString(eventInvite.recordedAt)} ${DateHelper.getTimeString(eventInvite.recordedAt)}"),
          SizedBox(
            height: 16,
          ),
          Row(
            children: <Widget>[
              Expanded(
                  child: AppGradientButton(
                label: "Decline",
                gradient: AppParams.appBtnGradient,
                labelStyle: TextStyle(fontSize: 16),
                padding: EdgeInsets.symmetric(vertical: 6),
                onTap: () {
                  onTap(eventInvite.id, false);
                },
                isOutline: true,
              )),
              SizedBox(
                width: 16,
              ),
              Expanded(
                  child: AppGradientButton(
                label: "Accept",
                gradient: AppParams.appBtnGradient,
                labelStyle: TextStyle(color: Colors.white, fontSize: 16),
                padding: EdgeInsets.symmetric(vertical: 7),
                onTap: () {
                  onTap(eventInvite.id, true);
                },
                isOutline: false,
              )),
            ],
          )
        ],
      ),
    );
  }

  Widget buildRow(String label, String value, {Widget action}) {
    return Row(
      children: <Widget>[
        Expanded(
            child: Text(
          label,
          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
        )),
        SizedBox(
          width: 8,
        ),
        Expanded(
          flex: 3,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                child: Text(
                  "$value",
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
                ),
              ),
              action ?? Container()
            ],
          ),
        )
      ],
    );
  }
}
