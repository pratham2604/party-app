import 'package:flutter/material.dart';
import 'package:party/metadata/params.dart';

class EventBackgroundWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppParams.eventBackground))),
        ),
        Container(
          color: Colors.black.withOpacity(0.8),
        )
      ],
    );
  }
}
