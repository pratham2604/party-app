import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/helpers/ui_helper.dart';
import 'package:party/models/user.dart';

class EventContactWidget extends StatelessWidget {
  final User contact;
  final Function(User user) onInvited;
  final bool isAdded;

  const EventContactWidget(
      {Key key, @required this.onInvited, this.contact, this.isAdded = false})
      : super(key: key);

  Future<void> refresh(BuildContext context) async {
    UIHelper.showLoadingDialog(context, "Checking Details...");
    User temp =
        await LoginController.getExistingUser(mobileNumber: contact.mobileNo);
    if (temp == null) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content:
            Text("User with mobile number ${contact.mobileNo} does not exist"),
        action: SnackBarAction(label: "Ok", onPressed: () {}),
      ));
    } else {
      onInvited(temp);
    }
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    log("Invited $isAdded");
    return ListTile(
      key: Key("${contact.mobileNo}"),
      leading: CircleAvatar(
        backgroundColor: Colors.white,
        radius: 20,
        backgroundImage: NetworkImage("${contact.defaultPic}"),
      ),
      title: Text(contact.name),
      subtitle: Text(contact.mobileNo),
      trailing: isAdded
          ? Icon(Icons.check)
          : RaisedButton(
              onPressed: () => refresh(context),
              child: Text("Invite"),
            ),
    );
  }
}
