import 'package:flutter/material.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';
import 'package:party/helpers/date_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/event.dart';
import 'package:party/widgets/common/approval_wating_widget.dart';

class EventWidget extends StatelessWidget {
  final Event event;
  final Function(Event event) onTap;
  final Function(Event event) onLongTap;

  const EventWidget(
      {Key key, @required this.event, @required this.onTap, this.onLongTap})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap(event);
      },
      onLongPress: () {
        if (onLongTap != null) {
          onLongTap(event);
        }
      },
      child: LayoutBuilder(
        builder: (context, constraints) {
          double width = constraints.biggest.width;
          double imageSize = width / 3.2;
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 12),
            child: Row(
              children: <Widget>[
                SizedBox(
                  width: 24,
                ),
                event.images.length == 0
                    ? Container()
                    : OutlineGradientButton(
                        padding: EdgeInsets.all(2),
                        radius: Radius.circular(imageSize),
                        child: Container(
                          width: imageSize,
                          height: imageSize,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(imageSize),
                              image: DecorationImage(
                                  image: NetworkImage(event.images[0]),
                                  fit: BoxFit.cover)),
                        ),
                        gradient: AppParams.appGradient,
                        strokeWidth: 1.5,
                      ),
                SizedBox(
                  width: 24,
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      GradientText(
                        event.name,
                        gradient: LinearGradient(
                            colors: AppParams.appBtnGradient.colors.reversed
                                .toList()),
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.normal),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "${DateHelper.getShortMonth(event.performanceAt.month)}. ${event.performanceAt.day}, ${event.performanceAt.year}"
                            .toUpperCase(),
                        style: TextStyle(
                            color: Colors.white.withOpacity(0.9), fontSize: 11),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Opacity(
                        opacity: event.approved ? 0.0 : 0.5,
                        child: !event.approved
                            ? ApprovalWaitingWidget()
                            : Container(),
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
