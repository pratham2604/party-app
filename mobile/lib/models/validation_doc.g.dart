// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'validation_doc.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ValidationDoc _$ValidationDocFromJson(Map<String, dynamic> json) {
  return ValidationDoc(
    json['id'] as String,
    json['photo'] as String,
    json['data'] as String,
    json['isApproved'] as bool ?? false,
  );
}

Map<String, dynamic> _$ValidationDocToJson(ValidationDoc instance) =>
    <String, dynamic>{
      'id': instance.id,
      'photo': instance.photo,
      'data': instance.data,
      'isApproved': instance.isApproved,
    };
