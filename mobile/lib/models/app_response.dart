import 'package:flutter/material.dart';

class AppResponse<T> {
  final String error;
  final T data;

  AppResponse(
    this.error,
    this.data,
  );

  AppResponse.named({@required this.error, @required this.data});

  bool get isSuccess => error == null;
}
