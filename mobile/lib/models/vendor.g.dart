// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vendor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Vendor _$VendorFromJson(Map<String, dynamic> json) {
  return Vendor(
    json['id'] as String,
    json['firstName'] as String,
    json['lastName'] as String,
    json['email'] as String,
    json['password'] as String,
    json['googleId'] as String,
    json['facebookId'] as String,
    json['mobileNo'] as String,
    json['profilePic'] as String,
    json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    (json['govtIds'] as Map<String, dynamic>)?.map(
          (k, e) => MapEntry(k, e == null ? null : ValidationDoc.fromJson(e)),
        ) ??
        {},
    json['isDisabled'] as bool ?? false,
    json['description'] as String,
    (json['rating'] as num)?.toDouble() ?? 0.0,
    json['ratingCount'] as int ?? 0,
    (json['cost'] as num)?.toDouble() ?? 0,
    json['currency'] as String,
    json['category'] as String,
    (json['questions'] as List)
            ?.map((e) => e == null ? null : FAQ.fromJson(e))
            ?.toList() ??
        [],
    json['coverImage'] as String,
    json['location'] == null ? null : AppLocation.fromJson(json['location']),
  );
}

Map<String, dynamic> _$VendorToJson(Vendor instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'email': instance.email,
      'password': instance.password,
      'googleId': instance.googleId,
      'facebookId': instance.facebookId,
      'mobileNo': instance.mobileNo,
      'profilePic': instance.profilePic,
      'createdAt': instance.createdAt?.toIso8601String(),
      'govtIds': instance.govtIds?.map((k, e) => MapEntry(k, e?.toJson())),
      'isDisabled': instance.isDisabled,
      'description': instance.description,
      'rating': instance.rating,
      'coverImage': instance.coverImage,
      'ratingCount': instance.ratingCount,
      'cost': instance.cost,
      'currency': instance.currency,
      'category': instance.category,
      'questions': instance.questions?.map((e) => e?.toJson())?.toList(),
      'location': instance.location?.toJson(),
    };

FAQ _$FAQFromJson(Map<String, dynamic> json) {
  return FAQ(
    json['question'] as String,
    json['answer'] as String,
  );
}

Map<String, dynamic> _$FAQToJson(FAQ instance) => <String, dynamic>{
      'question': instance.question,
      'answer': instance.answer,
    };

VendorCategory _$VendorCategoryFromJson(Map<String, dynamic> json) {
  return VendorCategory(
    json['id'] as String,
    json['name'] as String,
    json['image'] as String,
    json['vendorsCount'] as int,
  );
}

Map<String, dynamic> _$VendorCategoryToJson(VendorCategory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'image': instance.image,
      'vendorsCount': instance.vendorsCount,
    };
