// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_location.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppLocation _$AppLocationFromJson(Map<String, dynamic> json) {
  return AppLocation(
    json['id'] as String,
    json['fullAddress'] as String,
    json['city'] as String,
    json['country'] as String,
    (json['latitude'] as num)?.toDouble(),
    (json['longitude'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$AppLocationToJson(AppLocation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'fullAddress': instance.fullAddress,
      'city': instance.city,
      'country': instance.country,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };
