// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'review.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Review _$ReviewFromJson(Map<String, dynamic> json) {
  return Review(
    json['id'] as String,
    json['user'] == null ? null : UserMeta.fromJson(json['user']),
    json['title'] as String,
    json['description'] as String,
    json['recordedAt'] == null
        ? null
        : DateTime.parse(json['recordedAt'] as String),
    (json['rating'] as num)?.toDouble(),
    json['customerId'] as String,
    json['approved'] as bool ?? false,
    json['isSelf'] as bool ?? false,
  );
}

Map<String, dynamic> _$ReviewToJson(Review instance) => <String, dynamic>{
      'id': instance.id,
      'approved': instance.approved,
      'user': instance.user?.toJson(),
      'title': instance.title,
      'description': instance.description,
      'recordedAt': instance.recordedAt?.toIso8601String(),
      'rating': instance.rating,
      'customerId': instance.customerId,
      'isSelf': instance.isSelf,
    };

EventReview _$EventReviewFromJson(Map<String, dynamic> json) {
  return EventReview(
    json['id'] as String,
    json['user'] == null ? null : UserMeta.fromJson(json['user']),
    json['title'] as String,
    json['description'] as String,
    json['recordedAt'] == null
        ? null
        : DateTime.parse(json['recordedAt'] as String),
    (json['rating'] as num)?.toDouble(),
    json['customerId'] as String,
    json['eventId'] as String,
    json['approved'] as bool ?? false,
    json['isSelf'] as bool ?? false,
  );
}

Map<String, dynamic> _$EventReviewToJson(EventReview instance) =>
    <String, dynamic>{
      'id': instance.id,
      'approved': instance.approved,
      'user': instance.user?.toJson(),
      'title': instance.title,
      'description': instance.description,
      'recordedAt': instance.recordedAt?.toIso8601String(),
      'rating': instance.rating,
      'customerId': instance.customerId,
      'isSelf': instance.isSelf,
      'eventId': instance.eventId,
    };

VendorReview _$VendorReviewFromJson(Map<String, dynamic> json) {
  return VendorReview(
    json['id'] as String,
    json['user'] == null ? null : UserMeta.fromJson(json['user']),
    json['title'] as String,
    json['description'] as String,
    json['customerId'] as String,
    json['recordedAt'] == null
        ? null
        : DateTime.parse(json['recordedAt'] as String),
    (json['rating'] as num)?.toDouble(),
    json['approved'] as bool ?? false,
    json['isSelf'] as bool ?? false,
    json['vendorId'] as String,
    json['eventId'] as String,
  );
}

Map<String, dynamic> _$VendorReviewToJson(VendorReview instance) =>
    <String, dynamic>{
      'id': instance.id,
      'approved': instance.approved,
      'user': instance.user?.toJson(),
      'title': instance.title,
      'description': instance.description,
      'recordedAt': instance.recordedAt?.toIso8601String(),
      'rating': instance.rating,
      'customerId': instance.customerId,
      'isSelf': instance.isSelf,
      'vendorId': instance.vendorId,
      'eventId': instance.eventId,
    };

ReviewNotification _$ReviewNotificationFromJson(Map<String, dynamic> json) {
  return ReviewNotification(
    json['id'] as String,
    json['eventId'] as String,
    json['vendorId'] as String,
    json['userId'] as String,
  );
}

Map<String, dynamic> _$ReviewNotificationToJson(ReviewNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'eventId': instance.eventId,
      'vendorId': instance.vendorId,
      'userId': instance.userId,
    };
