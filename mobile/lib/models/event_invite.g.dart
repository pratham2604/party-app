// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_invite.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventInvite _$EventInviteFromJson(Map<String, dynamic> json) {
  return EventInvite(
    json['id'] as String,
    json['eventId'] as String,
    json['eventName'] as String,
    json['eventHostId'] as String,
    json['recordedAt'] == null
        ? null
        : DateTime.parse(json['recordedAt'] as String),
    json['leader'] == null ? null : UserMeta.fromJson(json['leader']),
    (json['friends'] as List)
            ?.map((e) => e == null ? null : UserMeta.fromJson(e))
            ?.toList() ??
        [],
    json['isInterested'] as bool ?? false,
    json['isNotInterested'] as bool ?? false,
    json['isGoing'] as bool ?? false,
  );
}

Map<String, dynamic> _$EventInviteToJson(EventInvite instance) =>
    <String, dynamic>{
      'id': instance.id,
      'eventId': instance.eventId,
      'eventName': instance.eventName,
      'eventHostId': instance.eventHostId,
      'recordedAt': instance.recordedAt?.toIso8601String(),
      'leader': instance.leader?.toJson(),
      'friends': instance.friends?.map((e) => e?.toJson())?.toList(),
      'isInterested': instance.isInterested,
      'isNotInterested': instance.isNotInterested,
      'isGoing': instance.isGoing,
    };
