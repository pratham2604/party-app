import 'package:json_annotation/json_annotation.dart';
import 'package:party/helpers/data_helper.dart';
import 'package:party/models/user.dart';

part 'event_invite.g.dart';

@JsonSerializable(explicitToJson: true)
class EventInvite {
  final String id;
  final String eventId;
  final String eventName;
  final String eventHostId;
  final DateTime recordedAt;
  final UserMeta leader;
  @JsonKey(defaultValue: [])
  final List<UserMeta> friends;
  @JsonKey(defaultValue: false)
  final bool isInterested;
  @JsonKey(defaultValue: false)
  final bool isNotInterested;
  @JsonKey(defaultValue: false)
  final bool isGoing;

  EventInvite(
    this.id,
    this.eventId,
    this.eventName,
    this.eventHostId,
    this.recordedAt,
    this.leader,
    this.friends,
    this.isInterested,
    this.isNotInterested,
    this.isGoing,
  );

  EventInvite.named(
      {this.id,
      this.eventId,
      this.eventName,
      this.eventHostId,
      this.recordedAt,
      this.leader,
      this.friends,
      this.isInterested = false,
      this.isNotInterested = false,
      this.isGoing = false});

  Map<String, dynamic> toJson() => _$EventInviteToJson(this);

  factory EventInvite.fromJson(json) =>
      _$EventInviteFromJson(DataHelper.convertToJson(json));

  EventInvite copyWith(EventInvite other) {
    return EventInvite.named(
        id: other.id ?? id,
        eventHostId: other.eventHostId ?? eventHostId,
        eventId: other.eventId ?? eventId,
        eventName: other.eventName ?? eventName,
        isGoing: other.isGoing ?? isGoing,
        isInterested: other.isInterested ?? isInterested,
        isNotInterested: other.isNotInterested ?? isNotInterested,
        recordedAt: other.recordedAt ?? recordedAt,
        leader: other.leader ?? leader,
        friends: other.friends ?? friends);
  }
}
