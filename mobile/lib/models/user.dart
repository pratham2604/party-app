import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:party/controllers/user_controller.dart';
import 'package:party/helpers/data_helper.dart';
import 'package:party/helpers/date_helper.dart';
import 'package:party/models/validation_doc.dart';

part 'user.g.dart';

@JsonSerializable(explicitToJson: true)
class User extends Equatable {
  static const String AADHAR_CARD = "aadharCard",
      PAN_CARD = "panCard",
      PASSPORT = "passport";
  final String id;
  final String firstName;
  final String lastName;
  final String email;
  final String password;
  final String googleId;
  final String facebookId;
  final String mobileNo;
  final String profilePic;
  final DateTime createdAt;
  @JsonKey(defaultValue: {})
  final Map<String, ValidationDoc> govtIds;
  @JsonKey(defaultValue: false)
  final bool isDisabled;

  User(
      this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.password,
      this.googleId,
      this.facebookId,
      this.mobileNo,
      this.profilePic,
      this.createdAt,
      this.govtIds,
      this.isDisabled);

  User.named({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.password,
    this.googleId,
    this.facebookId,
    this.mobileNo,
    this.profilePic,
    this.createdAt,
    this.govtIds,
    this.isDisabled,
  });

  User copyWith(User other) {
    return User.named(
        id: other.id ?? id,
        firstName: other.firstName ?? firstName,
        lastName: other.lastName ?? lastName,
        email: other.email ?? email,
        password: other.password ?? password,
        facebookId: other.facebookId ?? facebookId,
        googleId: other.googleId ?? googleId,
        mobileNo: other.mobileNo ?? mobileNo,
        profilePic: other.profilePic ?? profilePic,
        createdAt: other.createdAt ?? createdAt,
        govtIds: other.govtIds ?? govtIds,
        isDisabled: other.isDisabled ?? isDisabled);
  }

  Map<String, dynamic> toJson() {
    return _$UserToJson(this);
  }

  String get name => "${firstName ?? ""} ${lastName ?? ""}";

  String get defaultPic => UserController.getDefaultPic(name);

  factory User.fromJson(json) {
    return _$UserFromJson(DataHelper.convertToJson(json));
  }

  String get uniqueKey => createdAt != null
      ? DateHelper.getMonthTimeString(createdAt)
      : id ?? mobileNo ?? googleId ?? facebookId ?? email;

  @override
  List<Object> get props => [id];

  UserMeta get userMeta => UserMeta.named(
      id: id, createdAt: createdAt, name: name, profilePic: profilePic);
}

@JsonSerializable(explicitToJson: true)
class UserMeta extends Equatable {
  final String id;
  final String name;
  final String profilePic;
  final DateTime createdAt;

  UserMeta(this.id, this.name, this.profilePic, this.createdAt);

  UserMeta.named({this.id, this.name, this.profilePic, this.createdAt});

  Map<String, dynamic> toJson() => _$UserMetaToJson(this);

  factory UserMeta.fromJson(json) => _$UserMetaFromJson(json);

  @override
  List<Object> get props => [id];
}
