import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:party/helpers/data_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_location.dart';
import 'package:party/models/user.dart';
import 'package:party/widgets/common/custom_map.dart';

part 'event.g.dart';

@JsonSerializable(explicitToJson: true)
class Event extends Equatable implements CustomMapData {
  final String id;
  final String name;
  final List<String> images;
  final String description;
  final DateTime performanceAt;
  final List<String> hashtags;
  final AppLocation location;
  @JsonKey(defaultValue: 0)
  final int attendeeCount;
  final List<String> attendees;
  @JsonKey(defaultValue: 0)
  final int capacity;
  @JsonKey(defaultValue: 0)
  final double rating;
  @JsonKey(defaultValue: 0)
  final int ratingCount;
  final UserMeta host;
  @JsonKey(defaultValue: false)
  final bool approved;

  Event(
      this.id,
      this.name,
      this.description,
      this.images,
      this.performanceAt,
      this.hashtags,
      this.location,
      this.attendeeCount,
      this.attendees,
      this.capacity,
      this.rating,
      this.ratingCount,
      this.host,
      this.approved);

  Event.named({
    this.id,
    this.name,
    this.description,
    this.images,
    this.performanceAt,
    this.hashtags,
    this.location,
    this.attendeeCount,
    this.attendees,
    this.capacity,
    this.rating,
    this.ratingCount,
    this.host,
    this.approved,
  });

  @override
  List<Object> get props => [id];

  String get hashtagText => hashtags.map((val) => "#$val").join(", ");

  Map<String, dynamic> toJson() => _$EventToJson(this);

  factory Event.fromJson(json) =>
      _$EventFromJson(DataHelper.convertToJson(json));

  Event copyWith(Event other) {
    return Event.named(
        id: other.id ?? id,
        name: other.name ?? name,
        description: other.description ?? description,
        rating: other.rating ?? rating,
        ratingCount: other.ratingCount ?? ratingCount,
        attendeeCount: other.attendeeCount ?? attendeeCount,
        attendees: other.attendees ?? List<String>.from(attendees ?? []),
        approved: other.approved ?? approved,
        capacity: other.capacity ?? capacity,
        images: other.images ?? images,
        hashtags: other.hashtags ?? hashtags,
        host: other.host ?? host,
        location: other.location ?? location,
        performanceAt: other.performanceAt ?? performanceAt);
  }

  @override
  String get asset => AppParams.appMapIcon;

  @override
  LatLng get position => LatLng(location.latitude, location.longitude);

  @override
  String get title => name;
}
