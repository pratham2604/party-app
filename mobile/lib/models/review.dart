import 'package:json_annotation/json_annotation.dart';
import 'package:party/helpers/data_helper.dart';
import 'package:party/models/user.dart';

part 'review.g.dart';

@JsonSerializable(explicitToJson: true)
class Review {
  final String id;
  @JsonKey(defaultValue: false)
  final bool approved;
  final UserMeta user;
  final String title;
  final String description;
  final DateTime recordedAt;
  final double rating;
  final String customerId;
  @JsonKey(defaultValue: false)
  final bool isSelf;

  Review(
    this.id,
    this.user,
    this.title,
    this.description,
    this.recordedAt,
    this.rating,
    this.customerId,
    this.approved,
    this.isSelf,
  );

  Review.named({
    this.id,
    this.user,
    this.title,
    this.description,
    this.recordedAt,
    this.rating,
    this.customerId,
    this.approved,
    this.isSelf,
  });

  Map<String, dynamic> toJson() => _$ReviewToJson(this);

  factory Review.fromJson(json) =>
      _$ReviewFromJson(DataHelper.convertToJson(json));
}

@JsonSerializable(explicitToJson: true)
class EventReview extends Review {
  final String eventId;

  EventReview(
    String id,
    UserMeta user,
    String title,
    String description,
    DateTime recordedAt,
    double rating,
    String customerId,
    this.eventId,
    bool approved,
    bool isSelf,
  ) : super(id, user, title, description, recordedAt, rating, customerId,
            approved, isSelf);

  EventReview.named({
    String id,
    UserMeta user,
    String title,
    String description,
    DateTime recordedAt,
    double rating,
    customerId,
    bool approved,
    this.eventId,
    bool isSelf,
  }) : super(id, user, title, description, recordedAt, rating, customerId,
            approved, isSelf);

  EventReview copyWith(EventReview other) {
    return EventReview.named(
        id: other.id ?? id,
        title: other.title ?? title,
        description: other.description ?? description,
        eventId: other.eventId ?? eventId,
        rating: other.rating ?? rating,
        recordedAt: other.recordedAt ?? recordedAt,
        user: other.user ?? user,
        customerId: other.customerId ?? customerId,
        approved: other.approved ?? approved,
        isSelf: other.isSelf ?? isSelf);
  }

  Map<String, dynamic> toJson() => _$EventReviewToJson(this);

  factory EventReview.fromJson(json) =>
      _$EventReviewFromJson(DataHelper.convertToJson(json));
}

@JsonSerializable(explicitToJson: true)
class VendorReview extends Review {
  final String vendorId;
  final String eventId;

  VendorReview(
      String id,
      UserMeta user,
      String title,
      String description,
      String customerId,
      DateTime recordedAt,
      double rating,
      bool approved,
      bool isSelf,
      this.vendorId,
      this.eventId)
      : super(id, user, title, description, recordedAt, rating, customerId,
            approved, isSelf);

  VendorReview.named({
    this.vendorId,
    this.eventId,
    String id,
    UserMeta user,
    String title,
    String description,
    String customerId,
    DateTime recordedAt,
    double rating,
    bool approved,
    bool isSelf,
  }) : super(id, user, title, description, recordedAt, rating, customerId,
            approved, isSelf);

  VendorReview copyWith(VendorReview other) {
    return VendorReview.named(
        id: other.id ?? id,
        vendorId: other.vendorId ?? vendorId,
        title: other.title ?? title,
        description: other.description ?? description,
        eventId: other.eventId ?? eventId,
        rating: other.rating ?? rating,
        recordedAt: other.recordedAt ?? recordedAt,
        user: other.user ?? user,
        customerId: other.customerId ?? customerId,
        approved: other.approved ?? approved,
        isSelf: other.isSelf ?? isSelf);
  }

  Map<String, dynamic> toJson() => _$VendorReviewToJson(this);

  factory VendorReview.fromJson(json) =>
      _$VendorReviewFromJson(DataHelper.convertToJson(json));
}

@JsonSerializable(explicitToJson: true)
class ReviewNotification {
  final String id;
  final String eventId;
  final String vendorId;
  final String userId;

  ReviewNotification(this.id, this.eventId, this.vendorId, this.userId);

  ReviewNotification.named({this.id, this.eventId, this.vendorId, this.userId});

  Map<String, dynamic> toJson() => _$ReviewNotificationToJson(this);

  factory ReviewNotification.fromJson(json) =>
      _$ReviewNotificationFromJson(json);
}
