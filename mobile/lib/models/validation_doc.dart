import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:party/helpers/data_helper.dart';

part 'validation_doc.g.dart';

@JsonSerializable(explicitToJson: true)
class ValidationDoc extends Equatable {
  final String id;
  final String photo;
  final String data;
  @JsonKey(defaultValue: false)
  final bool isApproved;

  ValidationDoc(this.id, this.photo, this.data, this.isApproved);

  ValidationDoc.named({this.id, this.photo, this.data, this.isApproved});

  @override
  List<Object> get props => [id];

  Map<String, dynamic> toJson() => _$ValidationDocToJson(this);

  factory ValidationDoc.fromJson(json) =>
      _$ValidationDocFromJson(DataHelper.convertToJson(json));
  ValidationDoc copyWith(ValidationDoc other) {
    return ValidationDoc.named(
        id: other.id ?? id,
        data: other.data ?? data,
        isApproved: other.isApproved ?? isApproved,
        photo: other.photo ?? photo);
  }
}
