import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:party/helpers/data_helper.dart';

part 'app_location.g.dart';

@JsonSerializable(explicitToJson: true)
class AppLocation extends Equatable {
  final String id;
  final String fullAddress;
  final String city;
  final String country;
  final double latitude;
  final double longitude;

  AppLocation(this.id, this.fullAddress, this.city, this.country, this.latitude,
      this.longitude);

  const AppLocation.named(
      {this.id,
      this.fullAddress,
      this.city,
      this.country,
      this.latitude,
      this.longitude});

  Map<String, dynamic> toJson() => _$AppLocationToJson(this);

  factory AppLocation.fromJson(json) =>
      _$AppLocationFromJson(DataHelper.convertToJson(json));

  @override
  List<Object> get props => [id];

  LatLng get latLng => LatLng(latitude, longitude);
}
