// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['id'] as String,
    json['firstName'] as String,
    json['lastName'] as String,
    json['email'] as String,
    json['password'] as String,
    json['googleId'] as String,
    json['facebookId'] as String,
    json['mobileNo'] as String,
    json['profilePic'] as String,
    json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    (json['govtIds'] as Map<String, dynamic>)?.map(
          (k, e) => MapEntry(k, e == null ? null : ValidationDoc.fromJson(e)),
        ) ??
        {},
    json['isDisabled'] as bool ?? false,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'email': instance.email,
      'password': instance.password,
      'googleId': instance.googleId,
      'facebookId': instance.facebookId,
      'mobileNo': instance.mobileNo,
      'profilePic': instance.profilePic,
      'createdAt': instance.createdAt?.toIso8601String(),
      'govtIds': instance.govtIds?.map((k, e) => MapEntry(k, e?.toJson())),
      'isDisabled': instance.isDisabled,
    };

UserMeta _$UserMetaFromJson(Map<String, dynamic> json) {
  return UserMeta(
    json['id'] as String,
    json['name'] as String,
    json['profilePic'] as String,
    json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
  );
}

Map<String, dynamic> _$UserMetaToJson(UserMeta instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'profilePic': instance.profilePic,
      'createdAt': instance.createdAt?.toIso8601String(),
    };
