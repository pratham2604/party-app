// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Event _$EventFromJson(Map<String, dynamic> json) {
  return Event(
    json['id'] as String,
    json['name'] as String,
    json['description'] as String,
    (json['images'] as List)?.map((e) => e as String)?.toList(),
    json['performanceAt'] == null
        ? null
        : DateTime.parse(json['performanceAt'] as String),
    (json['hashtags'] as List)?.map((e) => e as String)?.toList(),
    json['location'] == null ? null : AppLocation.fromJson(json['location']),
    json['attendeeCount'] as int ?? 0,
    (json['attendees'] as List)?.map((e) => e as String)?.toList(),
    json['capacity'] as int ?? 0,
    (json['rating'] as num)?.toDouble() ?? 0,
    json['ratingCount'] as int ?? 0,
    json['host'] == null ? null : UserMeta.fromJson(json['host']),
    json['approved'] as bool ?? false,
  );
}

Map<String, dynamic> _$EventToJson(Event instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'images': instance.images,
      'description': instance.description,
      'performanceAt': instance.performanceAt?.toIso8601String(),
      'hashtags': instance.hashtags,
      'location': instance.location?.toJson(),
      'attendeeCount': instance.attendeeCount,
      'attendees': instance.attendees,
      'capacity': instance.capacity,
      'rating': instance.rating,
      'ratingCount': instance.ratingCount,
      'host': instance.host?.toJson(),
      'approved': instance.approved,
    };
