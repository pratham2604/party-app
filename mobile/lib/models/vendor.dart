import 'package:json_annotation/json_annotation.dart';
import 'package:party/helpers/data_helper.dart';
import 'package:party/models/app_location.dart';
import 'package:party/models/user.dart';
import 'package:party/models/validation_doc.dart';

part 'vendor.g.dart';

@JsonSerializable(explicitToJson: true)
class Vendor extends User {
  final String description;
  @JsonKey(defaultValue: 0.0)
  final double rating;
  final String coverImage;
  @JsonKey(defaultValue: 0)
  final int ratingCount;
  @JsonKey(defaultValue: 0)
  final double cost;
  final String currency;
  final String category;
  @JsonKey(defaultValue: [])
  final List<FAQ> questions;
  final AppLocation location;

  Vendor(
      String id,
      String firstName,
      String lastName,
      String email,
      String password,
      String googleId,
      String facebookId,
      String mobileNo,
      String profilePic,
      DateTime createdAt,
      Map<String, ValidationDoc> govtIds,
      bool isDisabled,
      this.description,
      this.rating,
      this.ratingCount,
      this.cost,
      this.currency,
      this.category,
      this.questions,
      this.coverImage,
      this.location)
      : super(id, firstName, lastName, email, password, googleId, facebookId,
            mobileNo, profilePic, createdAt, govtIds, isDisabled);

  Vendor.named(
      {String id,
      String firstName,
      String lastName,
      String email,
      String password,
      String googleId,
      String facebookId,
      String mobileNo,
      String profilePic,
      DateTime createdAt,
      Map<String, ValidationDoc> govtIds,
      bool isDisabled,
      this.description,
      this.rating,
      this.ratingCount,
      this.cost,
      this.currency,
      this.category,
      this.questions,
      this.coverImage,
      this.location})
      : super.named(
            id: id,
            firstName: firstName,
            lastName: lastName,
            email: email,
            password: password,
            googleId: googleId,
            facebookId: facebookId,
            mobileNo: mobileNo,
            profilePic: profilePic,
            createdAt: createdAt,
            govtIds: govtIds,
            isDisabled: isDisabled);

  Map<String, dynamic> toJson() => _$VendorToJson(this);

  factory Vendor.fromJson(json) =>
      _$VendorFromJson(DataHelper.convertToJson(json));
}

@JsonSerializable(explicitToJson: true)
class FAQ {
  final String question;
  final String answer;

  FAQ(this.question, this.answer);

  FAQ.named({this.question, this.answer});

  Map<String, dynamic> toJson() => _$FAQToJson(this);

  factory FAQ.fromJson(json) => _$FAQFromJson(DataHelper.convertToJson(json));
}

@JsonSerializable(explicitToJson: true)
class VendorCategory {
  final String id;
  final String name;
  final String image;
  final int vendorsCount;

  VendorCategory(this.id, this.name, this.image, this.vendorsCount);

  VendorCategory.named({this.id, this.name, this.image, this.vendorsCount});

  Map<String, dynamic> toJson() => _$VendorCategoryToJson(this);

  factory VendorCategory.fromJson(json) =>
      _$VendorCategoryFromJson(DataHelper.convertToJson(json));
}
