import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:party/controllers/firebase_controller.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/user.dart';
import 'package:party/models/validation_doc.dart';

class UserController {
  static Future<AppResponse<User>> getUserDetails(String id) async {
    try {
      DocumentSnapshot snapshot = await FirebaseController.firestore
          .collection(LoginController.USERS)
          .document(id)
          .get();
      if (snapshot.exists) {
        return AppResponse.named(
            error: null, data: User.fromJson(snapshot.data));
      }
      return AppResponse.named(error: "User does not exists", data: null);
    } catch (e) {
      return AppResponse.named(error: e.toString(), data: null);
    }
  }

  static Future<AppResponse<User>> getCurrentUser() async {
    try {
      String id = await LoginController.getCurrentUserId();
      AppResponse response = await getUserDetails(id);
      return response;
    } catch (e) {
      log("Error", error: e);
      return AppResponse.named(error: e.toString(), data: null);
    }
  }

  static String getDefaultPic(String name) {
    return "https://ui-avatars.com/api/?name=$name&background=0D8ABC&color=fff";
  }

  static Future<AppResponse<bool>> addValidation(
      ValidationDoc validationDoc) async {
    try {
      AppResponse<User> response = await getCurrentUser();
      if (!response.isSuccess) {
        return AppResponse.named(error: "User does not exists", data: null);
      }
      User currentUser = response.data;
      var map = currentUser.govtIds ?? {};
      map[validationDoc.id] = validationDoc;
      currentUser = currentUser.copyWith(User.named(govtIds: map));
      await FirebaseController.firestore
          .collection(LoginController.USERS)
          .document(currentUser.id)
          .setData(currentUser.toJson());
      return AppResponse.named(error: null, data: true);
    } catch (e) {
      log("Error", error: e);
      return AppResponse.named(error: "Some error occurred", data: null);
    }
  }
}
