import 'dart:convert';
import 'dart:developer';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:party/models/app_location.dart';
import 'package:party/models/app_response.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class LocationController {
  static final String apiKey = "AIzaSyDiMMBCVBM0MbYI6aJhs8cHN-BZXlhr-r0";

  static const String LOCATION_KEY = "location_key";

  static SharedPreferences sharedPreferences;

  static Future<AppResponse<LocationData>> getLocation() async {
    try {
      Location location = Location();
      PermissionStatus permissionStatus = await location.hasPermission();
      if (permissionStatus == PermissionStatus.DENIED) {
        PermissionStatus isPermit = await location.requestPermission();
        if (isPermit == PermissionStatus.GRANTED) {
          LocationData locationData = await location.getLocation();
          return AppResponse.named(error: null, data: locationData);
        } else {
          return AppResponse.named(
              error: "You have denied permission", data: null);
        }
      }
      LocationData locationData = await location.getLocation();
      return AppResponse.named(error: null, data: locationData);
    } catch (e) {
      return AppResponse.named(error: e, data: null);
    }
  }

  static Future<AppResponse<AppLocation>> getLocationRequest(
      LatLng position) async {
    try {
      List<String> mapTypes = [
        "locality",
        "country",
        "postal_code",
        "administrative",
        "administrative_area_level_1"
      ];
      String apiUrl =
          "https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.latitude}, ${position.longitude}&key=$apiKey";
      http.Response response = await http.get(apiUrl);
      var json = jsonDecode(response.body);
      var results = json['results'];
      Map<String, dynamic> addressMap = {};
      if (results != null && results is List && results.length > 0) {
        var result = results[0];
        var address = result["formatted_address"];
        var addressComponents = result["address_components"];
        addressComponents.forEach((val) {
          if (val["types"] != null) {
            List<String> types = List<String>.from(val["types"]);
            for (String type in types) {
              if (types != null && mapTypes.contains(type)) {
                addressMap[type] = val["short_name"];
                break;
              }
            }
          }
        });
        var locationMap = result["geometry"]["location"];
        LatLng position = LatLng(locationMap["lat"], locationMap["lng"]);
        AppLocation locationRequest = AppLocation.named(
            fullAddress: address,
            city: addressMap["locality"],
            latitude: position.latitude,
            longitude: position.longitude);

        return AppResponse.named(error: null, data: locationRequest);
      }
      return AppResponse.named(error: "Something wrong happened", data: null);
    } catch (e) {
      return AppResponse.named(error: "Something wrong happened", data: null);
    }
  }

  //Get Location
  static Future<List<AppLocation>> searchLocation(String query) async {
    String url =
        "https://maps.googleapis.com/maps/api/place/autocomplete/json?" +
            "key=$apiKey" +
            "&" +
            "input=$query" +
            "&sensor=false";
    http.Response response = await http.get(url);
    var json = jsonDecode(response.body);
    if (json["predictions"] != null) {
      var list =
          List<AppLocation>.from(List.from(json['predictions']).map((item) {
        return AppLocation.named(
          id: item["place_id"],
          fullAddress: item["description"],
        );
      }).toList());
      return list;
    }
    return [];
  }

  static AppLocation getLocationResponseFromJson(result) {
    List<String> mapTypes = [
      "locality",
      "country",
      "postalCode",
      "administrative",
      "administrative_area_level_1"
    ];
    Map<String, dynamic> addressMap = {};
    var address = result["formatted_address"];
    var addressComponents = result["address_components"];
    addressComponents.forEach((val) {
      if (val["types"] != null) {
        List<String> types = List<String>.from(val["types"]);
        for (String type in types) {
          if (types != null && mapTypes.contains(type)) {
            addressMap[type] = val["short_name"];
            break;
          }
        }
      }
    });
    var locationMap = result["geometry"]["location"];

    LatLng position = LatLng(locationMap["lat"], locationMap["lng"]);
    AppLocation locationRequest = AppLocation.named(
        fullAddress: address,
        city: addressMap["locality"],
        country: addressMap["country"],
        id: result["place_id"],
        latitude: position.latitude,
        longitude: position.longitude);

    return locationRequest;
  }

  static Future<AppResponse<AppLocation>> getLocationFromPlaceId(
      String placeId) async {
    try {
      log("Place Id $placeId");
      String url =
          "https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&key=$apiKey";
      http.Response response = await http.get(url);
      var json = jsonDecode(response.body);
      if (json["result"] != null)
        return AppResponse<AppLocation>.named(
            error: null, data: getLocationResponseFromJson(json["result"]));
      return AppResponse<AppLocation>.named(
          error: "Location does not exists", data: null);
    } catch (e) {
      return AppResponse<AppLocation>.named(
          error: "Location does not exists", data: null);
    }
  }

  static Future<bool> saveOldLocation(LatLng latLng) async {
    try {
      if (sharedPreferences == null) {
        sharedPreferences = await SharedPreferences.getInstance();
      }
      return sharedPreferences.setStringList(
          LOCATION_KEY, ["${latLng.latitude}", "${latLng.longitude}"]);
    } catch (e) {
      log("Error", error: e);

      return false;
    }
  }

  static Future<LatLng> getOldLocation() async {
    try {
      if (sharedPreferences == null) {
        sharedPreferences = await SharedPreferences.getInstance();
      }
      List<String> values = sharedPreferences.getStringList(
        LOCATION_KEY,
      );
      return LatLng(double.parse(values[0]), double.parse(values[1]));
    } catch (e) {
      log("Error", error: e);
      return null;
    }
  }

  static Future<bool> hasPermission() async {
    try {
      Location location = Location();
      PermissionStatus permissionStatus = await location.hasPermission();
      return permissionStatus == PermissionStatus.GRANTED;
    } catch (e) {
      log("Error", error: e);
      return false;
    }
  }
}
