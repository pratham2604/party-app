import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:party/controllers/firebase_controller.dart';
import 'package:party/controllers/user_controller.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/review.dart';
import 'package:party/models/user.dart';

import 'login_controller.dart';

class ReviewController {
  static const VENDOR_REVIEWS = "vendor_reviews";
  static const REVIEWS = "reviews";

  static Future<AppResponse<bool>> postVendorReview(
      VendorReview vendorReview) async {
    AppResponse<User> currentUser = await UserController.getCurrentUser();
    try {
      if (!currentUser.isSuccess) {
        return AppResponse<bool>.named(
            error: "You are not logged in", data: null);
      }
      // Getting old rating of evernt by event id
      // String vendorId = vendorReview.vendorId;
      // DocumentSnapshot eventSnapshot = await FirebaseController.firestore
      //     .collection(VENDORS_LIST)
      //     .document(vendorId)
      //     .get();
      // if (!eventSnapshot.exists) {
      //   return AppResponse<bool>.named(
      //       error: "Sorry event does not exits", data: null);
      // }
      // Vendor oldVendor = Vendor.fromJson(eventSnapshot.data);
      // double oldRating = oldVendor.rating ?? 0;
      // int oldRatingCount = oldVendor.ratingCount ?? 0;
      // int newRatingCount = oldRatingCount + 1;
      // double newRating =
      //     ((oldRating * oldRatingCount) + vendorReview.rating) / newRatingCount;
      // FirebaseController.firestore
      //     .collection(VENDORS)
      //     .document(vendorId)
      //     .updateData({"ratingCount": newRatingCount, "rating": newRating});
      //
      VendorReview newEventReview = vendorReview.copyWith(VendorReview.named(
          id: currentUser.data.id,
          user: currentUser.data.userMeta,
          recordedAt: DateTime.now(),
          approved: false,
          customerId: currentUser.data.id));
      FirebaseController.firestore
          .collection(VENDOR_REVIEWS)
          .document(vendorReview.vendorId)
          .collection(REVIEWS)
          .document(currentUser.data.id)
          .setData(newEventReview.toJson());
      return AppResponse<bool>.named(error: null, data: true);
    } catch (e) {
      log("Error ", error: e);
      return AppResponse<bool>.named(error: "Error occurred", data: null);
    }
  }

  static Future<AppResponse<List<VendorReview>>> getVendorReviews(
      String vendorId) async {
    try {
      String currentUserId = await LoginController.getCurrentUserId();
      QuerySnapshot snapshot = await FirebaseController.firestore
          .collection(VENDOR_REVIEWS)
          .document(vendorId)
          .collection(REVIEWS)
          .getDocuments();
      List<VendorReview> vendorReviews = snapshot.documents
          .map((val) => VendorReview.fromJson(val.data))
          .toList();
      vendorReviews = vendorReviews
          .map((e) => (e).copyWith(
              VendorReview.named(isSelf: currentUserId == e.customerId)))
          .toList();
      vendorReviews = vendorReviews
          .where((element) =>
              (element.approved) || (!element.approved && element.isSelf))
          .toList();
      return AppResponse<List<VendorReview>>.named(
          error: null, data: vendorReviews);
    } catch (e) {
      log("Error ", error: e);
      return AppResponse.named(error: "Error occurred", data: null);
    }
  }

  static Future<AppResponse<VendorReview>> getVendorReview(
      String vendorId) async {
    AppResponse<User> currentUser = await UserController.getCurrentUser();
    try {
      if (!currentUser.isSuccess) {
        return AppResponse<VendorReview>.named(
            error: "You are not logged in", data: null);
      }
      DocumentSnapshot snapshot = await FirebaseController.firestore
          .collection(VENDOR_REVIEWS)
          .document(vendorId)
          .collection(REVIEWS)
          .document(currentUser.data.id)
          .get();
      VendorReview vendorReview = VendorReview.fromJson(snapshot.data);
      return AppResponse<VendorReview>.named(error: null, data: vendorReview);
    } catch (e) {
      log("Error ", error: e);
      return AppResponse<VendorReview>.named(
          error: "Error occurred", data: null);
    }
  }
}
