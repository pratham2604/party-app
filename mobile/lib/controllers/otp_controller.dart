import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:party/models/app_response.dart';

class OTPController {
  static const String baseUrl = "https://control.msg91.com/api/";

  static const String API_KEY = "authkey=319452AvZtmgQhB5e4f830aP1";

  static String sessionId;

  static Future<String> sendOTP(String mobileNumber) async {
    String url =
        baseUrl + "sendotp.php?mobile=$mobileNumber&otp_length=6&$API_KEY";
    print("BASE URL $baseUrl");
    http.Response response = await http.post(url);
    print("MSG Mobile Body ${response.body}");
    var json = jsonDecode(response.body);
    return response.statusCode == 200 ? null : "Sending OTP Failed";
  }

  static Future<AppResponse<bool>> verifyOTP(
      String mobileNumber, String otp) async {
    String url =
        baseUrl + "verifyRequestOTP.php?mobile=$mobileNumber&otp=$otp&$API_KEY";
    print("URL $url");
    http.Response response = await http.post(url);
    print("Response ${response.body}");
    var json = jsonDecode(response.body);
    String error = json["type"] == "error"
        ? (json["message"] as String).replaceAll("_", " ")
        : null;
    return AppResponse.named(error: error, data: error != null ? true : false);
  }
}
