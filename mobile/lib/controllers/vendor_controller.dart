import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:party/controllers/firebase_controller.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/vendor.dart';

class VendorController {
  static const VENDORS = "vendors";
  static const VENDOR_CAREGORIES = "vendor_categories";

  static Stream<AppResponse<List<VendorCategory>>>
      getVendorCategories() async* {
    await for (var snap in FirebaseController.firestore
        .collection(VENDOR_CAREGORIES)
        .snapshots()) {
      try {
        yield AppResponse.named(
            error: null,
            data: snap.documents
                .map((val) => VendorCategory.fromJson(val.data))
                .toList());
      } catch (e) {
        yield AppResponse.named(error: e, data: null);
      }
    }
  }

  static Future<AppResponse<List<Vendor>>> getVendorsByCategory(
      VendorCategory category) async {
    try {
      QuerySnapshot snapshot = await FirebaseController.firestore
          .collection(VENDORS)
          .where("category", isEqualTo: category.id)
          .getDocuments();
      return AppResponse.named(
          error: null,
          data: snapshot.documents
              .map((val) => Vendor.fromJson(val.data))
              .toList());
    } catch (e) {
      log("Error", error: e);
      return AppResponse.named(error: "Vendors not found", data: null);
    }
  }

  static Future<AppResponse<List<Vendor>>> getVendorsByQuery(
      String query) async {
    try {
      QuerySnapshot snapshot = await FirebaseController.firestore
          .collection(VENDORS)
          .where("searchKey", isGreaterThan: query)
          .getDocuments();
      return AppResponse.named(
          error: null,
          data: snapshot.documents
              .map((val) => Vendor.fromJson(val.data))
              .toList());
    } catch (e) {
      return AppResponse.named(error: "Vendors not found", data: null);
    }
  }

  static Future<AppResponse<Vendor>> getVendorDetails(String vendorId) async {
    try {
      DocumentSnapshot snapshot = await FirebaseController.firestore
          .collection(VENDORS)
          .document(vendorId)
          .get();
      if (!snapshot.exists) {
        return AppResponse.named(error: "Vendor not exists", data: null);
      }
      return AppResponse.named(
          error: null, data: Vendor.fromJson(snapshot.data));
    } catch (e) {
      log("Erorr", error: e);
      return AppResponse.named(error: "Vendor not exists", data: null);
    }
  }
}
