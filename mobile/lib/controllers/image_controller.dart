import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:party/helpers/asset_helper.dart';
import 'package:uuid/uuid.dart';

class ImageController {
  static Future<Uint8List> compressImage(File file) async {
    try {
      List<int> result = await FlutterImageCompress.compressWithFile(
        file.absolute.path,
        quality: 75,
      );
      return Uint8List.fromList(result);
    } catch (e) {
      print("Excpetion $e");
      return file.readAsBytesSync();
    }
  }

  static Future<Uint8List> compressVideo(File file) async {
    return file.readAsBytesSync();
  }

  static Future<List<String>> addFiles(imageFiles) async {
    List<String> images = [];
    for (var image in imageFiles) {
      String path = await storeImage(image);
      images.add(path);
    }
    return images;
  }

  static Future<String> storeImage(File file,
      {bool compress = true, String id}) async {
    try {
      Uint8List data;
      if (compress) {
        data = await compressImage(file);
      } else {
        data = file.readAsBytesSync();
      }
      String imageId = id ?? Uuid().v4().toString();

      StorageReference reference =
          FirebaseStorage.instance.ref().child(imageId);
      StorageUploadTask uploadTask = reference.putData(data);
      StorageTaskSnapshot snapshot = await uploadTask.onComplete;
      String imagePath = (await snapshot.ref.getDownloadURL()).toString();
      return imagePath;
    } catch (e) {
      log("Error $e");
      return null;
    }
  }

  static Future<String> storeAssetImage(String asset,
      {bool compress = true, String id}) async {
    try {
      Uint8List data = await AssetHelper.getAssetData(asset);
      String imageId = id ?? Uuid().v4().toString();
      StorageReference reference =
          FirebaseStorage.instance.ref().child(imageId);
      StorageUploadTask uploadTask = reference.putData(data);
      StorageTaskSnapshot snapshot = await uploadTask.onComplete;
      String imagePath = (await snapshot.ref.getDownloadURL()).toString();
      return imagePath;
    } catch (e) {
      log("Error $e");
      return null;
    }
  }

  static storeVideo(File file, {bool compress}) async {
    try {
      Uint8List data;
      if (compress) {
        data = await compressVideo(file);
      } else {
        data = file.readAsBytesSync();
      }
      StorageReference reference =
          FirebaseStorage.instance.ref().child(Uuid().v4().toString());
      StorageUploadTask uploadTask = reference.putData(data);
      StorageTaskSnapshot snapshot = await uploadTask.onComplete;
      String imagePath = (await snapshot.ref.getDownloadURL()).toString();
      return imagePath;
    } catch (e) {
      return null;
    }
  }
}
