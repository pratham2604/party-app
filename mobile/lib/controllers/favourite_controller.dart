import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:party/controllers/firebase_controller.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/controllers/vendor_controller.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/vendor.dart';

class FavouriteController {
  static const String USER_FAVOURITES = "user_favourites";

  static Future<AppResponse<bool>> setVendorFavourite(String id) async {
    try {
      String currentUserId = await LoginController.getCurrentUserId();
      if (currentUserId == null) {
        return AppResponse.named(error: "User does not exist ", data: null);
      }
      await FirebaseController.firestore
          .collection(USER_FAVOURITES)
          .document(currentUserId)
          .collection(VendorController.VENDORS)
          .document(id)
          .setData({"vendorId": id});
      return AppResponse.named(error: null, data: true);
    } catch (e) {
      log("Error ", error: e);
      return AppResponse.named(error: e, data: null);
    }
  }

  static Future<AppResponse<bool>> removeVendorFavourite(String id) async {
    try {
      String currentUserId = await LoginController.getCurrentUserId();
      if (currentUserId == null) {
        return AppResponse.named(error: "User does not exist ", data: null);
      }
      await FirebaseController.firestore
          .collection(USER_FAVOURITES)
          .document(currentUserId)
          .collection(VendorController.VENDORS)
          .document(id)
          .delete();
      return AppResponse.named(error: null, data: true);
    } catch (e) {
      log("Error ", error: e);
      return AppResponse.named(error: e, data: null);
    }
  }

  static Future<AppResponse<List<Vendor>>> getVendorFavourites() async {
    try {
      String currentUserId = await LoginController.getCurrentUserId();
      if (currentUserId == null) {
        return AppResponse.named(error: "User does not exist ", data: null);
      }
      QuerySnapshot snapshot = await FirebaseController.firestore
          .collection(USER_FAVOURITES)
          .document(currentUserId)
          .collection(VendorController.VENDORS)
          .getDocuments();
      List<String> vendorIds = [];
      snapshot.documents.forEach((val) {
        vendorIds.add(val.data["vendorId"]);
      });
      List<Vendor> vendors = [];
      await Future.wait(vendorIds.map((vendorId) => Future(() async {
            AppResponse<Vendor> response =
                await VendorController.getVendorDetails(vendorId);
            if (response.isSuccess) vendors.add(response.data);
          })));
      return AppResponse.named(error: null, data: vendors);
    } catch (e) {
      return AppResponse.named(error: e, data: null);
    }
  }

  static Future<AppResponse<bool>> isVendorFavourite(String vendorId) async {
    try {
      String currentUserId = await LoginController.getCurrentUserId();
      if (currentUserId == null) {
        return AppResponse.named(error: "User does not exist ", data: null);
      }
      QuerySnapshot snapshot = await FirebaseController.firestore
          .collection(USER_FAVOURITES)
          .document(currentUserId)
          .collection(VendorController.VENDORS)
          .getDocuments();
      List<String> vendorIds = [];
      snapshot.documents.forEach((val) {
        vendorIds.add(val.data["vendorId"]);
      });
      return AppResponse.named(
          error: null, data: vendorIds.indexOf(vendorId) != -1);
    } catch (e) {
      return AppResponse.named(error: e, data: null);
    }
  }
}
