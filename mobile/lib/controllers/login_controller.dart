import 'dart:convert';
import 'dart:developer';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:party/controllers/firebase_controller.dart';
import 'package:party/controllers/image_controller.dart';
import 'package:party/helpers/data_helper.dart';
import 'package:party/helpers/input_helper.dart';
import 'package:party/metadata/params.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

class LoginController {
  // Collections
  static const String USERS = "users";
  static const String CURRENT_USER_ID = "current_user_id";

  // Local Storage
  static SharedPreferences sharedPreferences;

  static Future<AppResponse<User>> signupWithEmail(User user) async {
    try {
      String error;
      if (InputHelper.isBlank(user.firstName)) {
        error = InputHelper.getBlankValidation('First name');
      }
      if (InputHelper.isBlank(user.lastName)) {
        error = InputHelper.getBlankValidation('Last name');
      }
      if (InputHelper.isBlank(user.email) ||
          !InputHelper.validateEmail(user.email)) {
        error = InputHelper.getBlankValidation('Email');
      }
      if (InputHelper.isBlank(user.password)) {
        error = InputHelper.getBlankValidation('Password');
      }
      if (error != null) {
        return AppResponse.named(
          error: error,
          data: null,
        );
      }
      AppResponse<User> appResponse;
      User existingUser = await getExistingUser(
        email: user.email,
      );
      if (existingUser != null) {
        appResponse = AppResponse<User>.named(
          error: "User Already exists",
          data: null,
        );
        return appResponse;
      }
      appResponse = await signupUser(user);
      if (appResponse.isSuccess) {
        await saveCurrentUserId(appResponse.data.id);
      }
      return appResponse;
    } catch (e) {
      return AppResponse.named(
        error: e.toString(),
        data: null,
      );
    }
  }

  // Signup
  static Future<AppResponse<User>> signupUser(User user) async {
    try {
      String id = Uuid().v4();
      // Store default profile pic
      String defaultPic = await ImageController.storeAssetImage(
          AppParams.appDefaultProfile,
          id: id);

      User newUser = user.copyWith(User.named(
          id: id, createdAt: DateTime.now(), profilePic: defaultPic));

      await FirebaseController.firestore
          .collection(USERS)
          .document(id)
          .setData(newUser.toJson());
      return AppResponse<User>.named(
        error: null,
        data: newUser,
      );
    } catch (e) {
      return AppResponse.named(
        error: e,
        data: null,
      );
    }
  }

  // Update
  static Future<AppResponse<User>> updateUser(User user) async {
    try {
      if (user.id == null) {
        return AppResponse.named(error: "Id not exists", data: null);
      }
      await FirebaseController.firestore
          .collection(USERS)
          .document(user.id)
          .updateData(DataHelper.convertToJson(user.toJson()));
      return AppResponse<User>.named(
        error: null,
        data: user,
      );
    } catch (e) {
      log("Error", error: e);
      return AppResponse.named(
        error: "Some error occurred",
        data: null,
      );
    }
  }

  // Google Login App
  static Future<AppResponse<User>> getGoogleUser() async {
    try {
      User user;
      GoogleSignIn _googleSignIn = GoogleSignIn(
        scopes: [
          'email',
        ],
      );
      GoogleSignInAccount currentUser = await _googleSignIn.signIn();
      await currentUser.authentication;
      if (currentUser == null) {
        return AppResponse.named(error: "Login with google failed", data: null);
      }
      user = User.named(
          email: currentUser.email,
          firstName: currentUser.displayName,
          googleId: currentUser.id);

      return AppResponse.named(error: null, data: user);
    } catch (e) {
      return AppResponse.named(error: e, data: null);
    }
  }

// Google Login
  static Future<AppResponse<User>> loginWithGoogle() async {
    try {
      AppResponse<User> response = await getGoogleUser();
      if (!response.isSuccess) {
        return response;
      }
      User user = response.data;

      ///
      /// Check user exists or not if yes then login
      /// else create new user
      ///
      User oldUser = await getExistingUser(
        googleId: user.googleId,
      );
      if (oldUser == null) {
        AppResponse<User> appResponse = await signupUser(user);
        if (appResponse.isSuccess) {
          await saveCurrentUserId(appResponse.data.id);
        }
        return appResponse;
      } else {
        if (oldUser.googleId == null && oldUser.email != null) {
          AppResponse<User> updateResponse = await updateUser(
              oldUser.copyWith(User.named(googleId: user.googleId)));
          return updateResponse;
        }

        if (oldUser.isDisabled) {
          await logout();
          return AppResponse.named(
              error:
                  "Your account has been suspended. Please contact customer support to resolve the conflict.",
              data: null);
        }
        await saveCurrentUserId(oldUser.id);
        return AppResponse<User>.named(
          error: null,
          data: oldUser,
        );
      }
    } catch (e) {
      log("Error", error: e);
      return AppResponse<User>.named(
        error: "Error occurred",
        data: null,
      );
    }
  }

  // Google Login App
  static Future<AppResponse<User>> getFacebookUser() async {
    try {
      User user;
      final facebookLogin = FacebookLogin();
      final result = await facebookLogin.logIn(['email']);
      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          final String token = result.accessToken.token;
          final graphResponse = await http.get(
              'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=$token');
          Map<String, dynamic> profile = jsonDecode(graphResponse.body);
          user = User.named(
            email: profile["email"],
            firstName: profile["first_name"],
            lastName: profile["last_name"],
            facebookId: profile["id"],
            password: null,
          );
          return AppResponse.named(error: null, data: user);
        case FacebookLoginStatus.cancelledByUser:
          return AppResponse.named(
            error: "You cancelled login with facebook",
            data: null,
          );
          break;
        case FacebookLoginStatus.error:
          return AppResponse.named(
            error: "Some error occurred",
            data: null,
          );
          break;
      }
      return AppResponse.named(
        error: "Some error occurred",
        data: null,
      );
    } catch (e) {
      return AppResponse.named(error: e, data: null);
    }
  }

  // Faceboook Login
  static Future<AppResponse<User>> loginWithFacebook() async {
    try {
      User user;
      AppResponse<User> response = await getFacebookUser();
      if (!response.isSuccess) {
        return response;
      }
      user = response.data;
      User oldUser = await getExistingUser(
        facebookId: user.facebookId,
      );
      if (oldUser == null) {
        AppResponse<User> appResponse = await signupUser(user);
        if (appResponse.isSuccess) {
          await saveCurrentUserId(appResponse.data.id);
        }
        return appResponse;
      } else {
        if (oldUser.email != null && oldUser.facebookId == null) {
          await updateUser(
              oldUser.copyWith(User.named(facebookId: user.facebookId)));
        }
        if (oldUser.isDisabled) {
          await logout();
          return AppResponse.named(
              error:
                  "Your account has been suspended. Please contact customer support to resolve the conflict.",
              data: null);
        }
        await saveCurrentUserId(oldUser.id);
        return AppResponse<User>.named(
          error: null,
          data: oldUser,
        );
      }
    } catch (e) {
      log("Error", error: e.toString());
      return AppResponse.named(
        error: "Some error occurred",
        data: null,
      );
    }
  }

  // Login with email
  static Future<AppResponse<User>> loginWithEmail(
      String email, String password) async {
    try {
      if (InputHelper.isBlank(email) || !InputHelper.validateEmail(email)) {
        return AppResponse.named(
          error: "Please enter valid email",
          data: null,
        );
      }
      if (InputHelper.isBlank(password)) {
        return AppResponse.named(
          error: "Please enter valid password",
          data: null,
        );
      }
      User user = await getExistingUser(email: email);
      if (user == null) {
        return AppResponse.named(
          error: "User does not exists",
          data: null,
        );
      }
      if (user.password != password) {
        return AppResponse.named(
          error: "Password does not match",
          data: null,
        );
      }

      if (user.isDisabled) {
        return AppResponse.named(
            error:
                "Your account has been suspended. Please contact customer support to resolve the conflict.",
            data: null);
      }
      await saveCurrentUserId(user.id);

      return AppResponse.named(
        error: null,
        data: user,
      );
    } catch (e) {
      return AppResponse.named(
        error: e.toString(),
        data: null,
      );
    }
  }

  // Check User Exists
  static Future<User> getExistingUser(
      {String email,
      String googleId,
      String facebookId,
      String mobileNumber}) async {
    try {
      List<User> result = await Future.wait([
        Future(() async {
          if (email == null) {
            return null;
          }
          var result = await FirebaseController.firestore
              .collection(USERS)
              .where("email", isEqualTo: email)
              .getDocuments();
          if (result.documents.length == 0) {
            return null;
          }
          User user = User.fromJson(result.documents[0].data);
          return user;
        }),
        Future(() async {
          if (googleId == null) {
            return null;
          }
          var result = await FirebaseController.firestore
              .collection(USERS)
              .where("googleId", isEqualTo: googleId)
              .getDocuments();
          if (result.documents.length == 0) {
            return null;
          }
          User user = User.fromJson(result.documents[0].data);
          return user;
        }),
        Future(() async {
          if (facebookId == null) {
            return null;
          }
          var result = await FirebaseController.firestore
              .collection(USERS)
              .where("facebookId", isEqualTo: facebookId)
              .getDocuments();
          if (result.documents.length == 0) {
            return null;
          }
          User user = User.fromJson(result.documents[0].data);
          return user;
        }),
        Future(() async {
          if (mobileNumber == null) {
            return null;
          }
          var result = await FirebaseController.firestore
              .collection(USERS)
              .where("mobileNo", isEqualTo: mobileNumber)
              .getDocuments();
          if (result.documents.length == 0) {
            return null;
          }
          User user = User.fromJson(result.documents[0].data);
          return user;
        }),
      ]);
      User temp;
      result.forEach((val) {
        if (val != null) {
          temp = val;
        }
      });
      return temp;
    } catch (e) {
      return null;
    }
  }

  // Saving user id
  static Future<void> saveCurrentUserId(String userId) async {
    if (sharedPreferences == null) {
      sharedPreferences = await SharedPreferences.getInstance();
    }
    await sharedPreferences.setString(CURRENT_USER_ID, userId);
  }

  // Getting user id
  static Future<String> getCurrentUserId() async {
    if (sharedPreferences == null) {
      sharedPreferences = await SharedPreferences.getInstance();
    }
    return sharedPreferences.getString(
      CURRENT_USER_ID,
    );
  }

  static Future<void> logout() async {
    await sharedPreferences.remove(CURRENT_USER_ID);
    await GoogleSignIn().signOut();
    await FacebookLogin().logOut();
  }
}
