import 'package:contacts_service/contacts_service.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/user.dart';
import 'package:permission_handler/permission_handler.dart';

class ContactController {
  static Future<AppResponse<List<User>>> getPhoneContacts() async {
    try {
      PermissionHandler permissionHandler = PermissionHandler();
      Map<PermissionGroup, PermissionStatus> statusResult =
          await permissionHandler
              .requestPermissions([PermissionGroup.contacts]);
      PermissionStatus status = statusResult[PermissionGroup.camera];
      if (status == PermissionStatus.denied) {
        return AppResponse.named(error: "Permisison not granted", data: null);
      }
      Iterable<Contact> contacts = await ContactsService.getContacts(
          withThumbnails: false, orderByGivenName: true);

      List<User> result =
          contacts.where((element) => element.phones.length > 0).map((e) {
        String mobileNumber = e.phones.first.value;
        return User.named(
            id: mobileNumber, firstName: e.displayName, mobileNo: mobileNumber);
      }).toList();
      return AppResponse.named(error: null, data: result);
    } catch (e) {
      return AppResponse.named(error: e, data: null);
    }
  }
}
