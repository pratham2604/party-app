import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/models/app_response.dart';
import 'package:http/http.dart' as http;

class FirebaseController {
  static const String FCM_TOKENS = "fcm_tokens";
  static Firestore _firestore = Firestore.instance;

  static Firestore get firestore => _firestore;

  static Future<AppResponse<bool>> sendNotification(
      {String title, String description, String image, String userId}) async {
    try {
      String token = await getFCM(userId);
      String url = "https://fcm.googleapis.com/fcm/send";
      var headers = {
        "Authorization": "key=AIzaSyA40ScrMoqV0EDexOpCYopTAjZLE_ATPk0",
        "Content-Type": "application/json"
      };
      var body = {
        "to": "$token",
        "collapse_key": "type_a",
        "android_channel_id": "FLUTTER_CLICK",
        "notification": {
          "body": "$description",
          "title": "$title",
        },
        "data": {
          "click_action": "FLUTTER_NOTIFICATION_CLICK",
          "body": "$description",
          "title": "$title",
        }
      };
      await http.post(url, headers: headers, body: jsonEncode(body));
      return AppResponse.named(error: null, data: true);
    } catch (e) {
      return AppResponse.named(error: e, data: null);
    }
  }

  static Future saveToken(String token) async {
    try {
      String id = await LoginController.getCurrentUserId();
      if (id == null) {
        return;
      }
      await _firestore
          .collection(FCM_TOKENS)
          .document(id)
          .setData({"fcm": token});
    } catch (e) {}
  }

  static Future<String> getFCM(String userId) async {
    try {
      DocumentSnapshot snapshot =
          await _firestore.collection(FCM_TOKENS).document(userId).get();
      return snapshot.data["fcm"];
    } catch (e) {
      return null;
    }
  }
}
