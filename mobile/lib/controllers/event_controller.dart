import 'dart:developer';
import 'dart:io';
import 'dart:math' as Math;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:party/controllers/firebase_controller.dart';
import 'package:party/controllers/image_controller.dart';
import 'package:party/controllers/login_controller.dart';
import 'package:party/controllers/user_controller.dart';
import 'package:party/models/app_response.dart';
import 'package:party/models/event.dart';
import 'package:party/models/event_invite.dart';
import 'package:party/models/review.dart';
import 'package:party/models/user.dart';
import 'package:uuid/uuid.dart';

class EventController {
  static const String EVENTS = "events";
  static const String EVENT_REVIEWS = "event_reviews";
  static const String EVENT_INVITES = "event_invites";
  static const String INVITES = "invites";
  static const String REVIEWS = "reviews";

  static Future<AppResponse<List<Event>>> getUserEvents(String userId) async {
    try {
      QuerySnapshot snapshot = await FirebaseController.firestore
          .collection(EVENTS)
          .where("host.id", isEqualTo: userId)
          .getDocuments();
      return AppResponse.named(
          error: null,
          data: snapshot.documents
              .map((val) => Event.fromJson(val.data))
              .toList());
    } catch (e) {
      log("Error", error: e);
      return AppResponse.named(error: "Error occurred", data: null);
    }
  }

  static Stream<AppResponse<List<Event>>> getNearByEvents(
      LatLng position) async* {
    await for (var snap in FirebaseController.firestore
        .collection(EVENTS)
        .where("approved", isEqualTo: true)
        .where("performanceAt",
            isGreaterThanOrEqualTo: DateTime.now().toIso8601String())
        .snapshots()) {
      try {
        List<Event> result =
            snap.documents.map((val) => Event.fromJson(val.data)).toList();
        yield AppResponse.named(
            error: null,
            data: result
                .where((element) =>
                    getDistance(
                        position,
                        LatLng(element.location.latitude,
                            element.location.longitude)) <
                    100)
                .toList());
      } catch (e) {
        yield AppResponse.named(error: e, data: null);
      }
    }
  }

  static double getDistance(LatLng l1, LatLng l2) {
    var p = 0.017453292519943295;
    var c = Math.cos;
    var a = 0.5 -
        c((l2.latitude - l1.latitude) * p) / 2 +
        c(l1.latitude * p) *
            c(l2.latitude * p) *
            (1 - c((l2.longitude - l1.longitude) * p)) /
            2;
    return 12742 * Math.asin(Math.sqrt(a));
  }

  static Future<AppResponse<List<EventReview>>> getReviews(
      String eventId) async {
    try {
      String currentUserId = await LoginController.getCurrentUserId();

      QuerySnapshot snapshot = await FirebaseController.firestore
          .collection(EVENT_REVIEWS)
          .document(eventId)
          .collection(REVIEWS)
          .getDocuments();
      List<EventReview> eventReviews = snapshot.documents
          .map((val) => EventReview.fromJson(val.data))
          .toList();
      eventReviews = eventReviews
          .map((e) => (e).copyWith(
              EventReview.named(isSelf: currentUserId == e.customerId)))
          .toList();
      AppResponse<List<EventReview>>.named(error: null, data: eventReviews);

      eventReviews = eventReviews
          .where((element) =>
              (element.approved) || (!element.approved && element.isSelf))
          .toList();
      return AppResponse.named(error: null, data: eventReviews);
    } catch (e) {
      log("Error ", error: e);
      return AppResponse.named(error: "Error occurred", data: null);
    }
  }

  static Future<AppResponse<Event>> postEvent(
      Event event, List<File> files) async {
    try {
      AppResponse<User> currentUser = await UserController.getCurrentUser();
      if (!currentUser.isSuccess) {
        return AppResponse<Event>.named(
            error: "You are not logged in", data: null);
      }
      List<String> images = [];
      await Future.wait(files.map((val) => Future(() async {
            String path = await ImageController.storeImage(val, compress: true);
            if (path != null) {
              images.add(path);
            }
          })));
      Event newEvent = event.copyWith(Event.named(
          id: Uuid().v4(), images: images, host: currentUser.data.userMeta));
      FirebaseController.firestore
          .collection(EVENTS)
          .document(newEvent.id)
          .setData(newEvent.toJson());
      return AppResponse<Event>.named(error: null, data: event);
    } catch (e) {
      log("Error", error: e);
      return AppResponse<Event>.named(error: "Error occurred", data: null);
    }
  }

  static Future<AppResponse<bool>> postReview(
    EventReview eventReview,
  ) async {
    AppResponse<User> currentUser = await UserController.getCurrentUser();
    try {
      if (!currentUser.isSuccess) {
        return AppResponse<bool>.named(
            error: "You are not logged in", data: null);
      }
      // Getting old rating of evernt by event id
      // String eventId = eventReview.eventId;
      // DocumentSnapshot eventSnapshot = await FirebaseController.firestore
      //     .collection(EVENTS)
      //     .document(eventId)
      //     .get();
      // if (!eventSnapshot.exists) {
      //   return AppResponse<bool>.named(
      //       error: "Sorry event does not exits", data: null);
      // }
      // Event oldEvent = Event.fromJson(eventSnapshot.data);
      // double oldRating = oldEvent.rating ?? 0;
      // int oldRatingCount = oldEvent.ratingCount ?? 0;
      // int newRatingCount = oldRatingCount + 1;
      // double newRating =
      //     ((oldRating * oldRatingCount) + eventReview.rating) / newRatingCount;
      // FirebaseController.firestore
      //     .collection(EVENTS)
      //     .document(eventId)
      //     .updateData({"ratingCount": newRatingCount, "rating": newRating});
      //
      EventReview newEventReview = eventReview.copyWith(EventReview.named(
          id: currentUser.data.id,
          user: currentUser.data.userMeta,
          approved: false,
          recordedAt: DateTime.now(),
          customerId: currentUser.data.id));
      FirebaseController.firestore
          .collection(EVENT_REVIEWS)
          .document(newEventReview.eventId)
          .collection(REVIEWS)
          .document(newEventReview.id)
          .setData(newEventReview.toJson());

      return AppResponse<bool>.named(error: null, data: true);
    } catch (e) {
      return AppResponse<bool>.named(error: "Error occurred", data: null);
    }
  }

  static Future<AppResponse<EventReview>> getReview(String eventId) async {
    try {
      AppResponse<User> currentUser = await UserController.getCurrentUser();
      if (!currentUser.isSuccess) {
        return AppResponse<EventReview>.named(
            error: "You are not logged in", data: null);
      }

      DocumentSnapshot snapshot = await FirebaseController.firestore
          .collection(EVENT_REVIEWS)
          .document(eventId)
          .collection(REVIEWS)
          .document(currentUser.data.id)
          .get();
      EventReview eventReview = EventReview.fromJson(snapshot.data);
      return AppResponse<EventReview>.named(error: null, data: eventReview);
    } catch (e) {
      log("Error ", error: e);
      return AppResponse<EventReview>.named(
          error: "Error occurred", data: null);
    }
  }

  ///
  /// Invite
  ///

  static Future<AppResponse<bool>> sendInvite(Event event,
      {List<UserMeta> friends,
      bool isInterested = false,
      bool isNotInterested = false}) async {
    try {
      AppResponse<User> currentUser = await UserController.getCurrentUser();
      if (!currentUser.isSuccess) {
        return AppResponse<bool>.named(
            error: "You are not logged in", data: null);
      }
      // Get Old Invite
      EventInvite eventInvite;
      QuerySnapshot snapshot = await FirebaseController.firestore
          .collection(EVENT_INVITES)
          .where(
            "eventId",
            isEqualTo: event.id,
          )
          .where("leader.id", isEqualTo: currentUser.data.id)
          .getDocuments();
      if (snapshot.documents.length > 0) {
        eventInvite = EventInvite.fromJson(snapshot.documents[0].data);
        if (isInterested) {
          List<UserMeta> oldFriends = eventInvite.friends;
          oldFriends.addAll(friends ?? []);
          oldFriends = Set<UserMeta>.from(oldFriends).toList();
          eventInvite = eventInvite.copyWith(EventInvite.named(
              friends: oldFriends.toList(), isInterested: true));
        } else {
          eventInvite = eventInvite.copyWith(EventInvite.named(
            friends: [],
            isNotInterested: true,
            isInterested: false,
          ));
        }
      } else {
        eventInvite = EventInvite.named(
          id: Uuid().v4().toString(),
          eventId: event.id,
          eventName: event.name,
          eventHostId: event.host.id,
          isInterested: isInterested,
          isNotInterested: isNotInterested,
          recordedAt: DateTime.now(),
          leader: currentUser.data.userMeta,
          friends: friends,
        );
      }
      //
      await FirebaseController.firestore
          .collection(EVENT_INVITES)
          .document(eventInvite.id)
          .setData(eventInvite.toJson());
      await FirebaseController.sendNotification(
          title: "I want to party with you.",
          description: "${currentUser.data.name} sent an invite",
          userId: event.host.id);
      return AppResponse.named(error: null, data: true);
    } catch (e) {
      log("Error", error: e);
      return AppResponse.named(error: "Some error occurred", data: null);
    }
  }

  static Future<AppResponse<EventInvite>> getInvite(
    String eventId,
  ) async {
    try {
      String currentUserId = await LoginController.getCurrentUserId();
      if (currentUserId == null) {
        return AppResponse<EventInvite>.named(
            error: "You are not logged in", data: null);
      }
      QuerySnapshot snapshot = await FirebaseController.firestore
          .collection(EVENT_INVITES)
          .where("eventId", isEqualTo: eventId)
          .where("leader.id", isEqualTo: currentUserId)
          .getDocuments();
      if (snapshot.documents.length > 0) {
        return AppResponse.named(
            error: null,
            data: EventInvite.fromJson(snapshot.documents[0].data));
      }
      return AppResponse.named(error: "No invite", data: null);
    } catch (e) {
      return AppResponse.named(error: "Some error occurred", data: null);
    }
  }

  static Future<AppResponse<List<EventInvite>>> getMyInvites() async {
    try {
      String currentUserId = await LoginController.getCurrentUserId();
      if (currentUserId == null) {
        return AppResponse.named(error: "You are not logged in", data: null);
      }
      QuerySnapshot snapshot = await FirebaseController.firestore
          .collection(EVENT_INVITES)
          .where(
            "eventHostId",
            isEqualTo: currentUserId,
          )
          .getDocuments();
      return AppResponse.named(
          error: null,
          data: snapshot.documents
              .map((val) => EventInvite.fromJson(val.data))
              .where((val) => val.isInterested ?? false)
              .toList());
    } catch (e) {
      return AppResponse.named(error: "Some error occurred", data: null);
    }
  }

  static Future<AppResponse<bool>> acceptInvite(EventInvite eventInvite,
      {bool check}) async {
    try {
      AppResponse<User> currentUser = await UserController.getCurrentUser();
      if (!currentUser.isSuccess) {
        return AppResponse<bool>.named(
            error: "You are not logged in", data: null);
      }
      await FirebaseController.firestore
          .collection(EVENT_INVITES)
          .document(eventInvite.id)
          .setData(
              eventInvite.copyWith(EventInvite.named(isGoing: check)).toJson());
      await FirebaseController.sendNotification(
          title:
              "Your party invitation request for ${eventInvite.eventName} has been ${check ? "accepted" : "declined"}",
          description: "${currentUser.data.name} sent a message",
          userId: eventInvite.leader.id);
      await updateViewCount(eventInvite.eventId, eventInvite.leader.id);
      return AppResponse.named(error: null, data: true);
    } catch (e) {
      return AppResponse.named(error: "Some error occurred", data: null);
    }
  }

  static Future<void> updateViewCount(String eventId, String userId) async {
    DocumentSnapshot snapshot = await FirebaseController.firestore
        .collection(EVENTS)
        .document(eventId)
        .get();
    if (snapshot.exists) {
      Event event = Event.fromJson(snapshot.data);
      event = event.copyWith(Event.named(
          attendeeCount: event.attendeeCount + 1,
          attendees: (event.attendees ?? [])..add(userId)));
      await FirebaseController.firestore
          .collection(EVENTS)
          .document("$eventId")
          .setData(event.toJson());
    }
  }
}
