import Store from '../store/vendors';
import * as TYPES from '../constants/types';

export const initialState = Store;

export default function vendorsReducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.FETCH_VENDORS_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        error: null,
        data: [],
      });
    case TYPES.FETCH_VENDORS_SUCCESS:
      return Object.assign({}, state, {
        data: action.data,
        fetching: false,
        error: null,
      });
    case TYPES.FETCH_VENDORS_FAILURE:
      return Object.assign({}, state, {
        error: action.message,
        fetching: false,
        data: [],
      });

    case TYPES.FETCH_VENDORS_CATEGORIES_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        error: null,
        categories: [],
      });
    case TYPES.FETCH_VENDORS_CATEGORIES_SUCCESS:
      return Object.assign({}, state, {
        categories: action.data,
        fetching: false,
        error: null,
      });
    default:
      return state;
  }
}