import Store from '../store/users';
import * as TYPES from '../constants/types';

export const initialState = Store;

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case TYPES.FETCH_USERS_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        error: null,
        data: [],
      });
    case TYPES.FETCH_USERS_SUCCESS:
      return Object.assign({}, state, {
        data: action.data,
        fetching: false,
        error: null,
      });
    case TYPES.FETCH_USERS_FAILURE:
      return Object.assign({}, state, {
        error: action.message,
        fetching: false,
        data: [],
      });

    case TYPES.UPDATE_USERS_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        error: null,
      });
    case TYPES.UPDATE_USERS_SUCCESS:
      return Object.assign({}, state, {
        fetching: false,
        error: null,
      });
    case TYPES.UPDATE_USERS_FAILURE:
      return Object.assign({}, state, {
        error: action.message,
        fetching: false,
      });
    default:
      return state;
  }
}