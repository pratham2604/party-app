import { TEMPLATE_TYPES } from './template/index';

const firebaseConfig = {
  apiKey: "AIzaSyD_ooOKLMIvpfT3syDsyFjO5grpJMBmWQo",
  authDomain: "party-f0fc8.firebaseapp.com",
  databaseURL: "https://party-f0fc8.firebaseio.com",
  projectId: "party-f0fc8",
  storageBucket: "party-f0fc8.appspot.com",
  messagingSenderId: "1007924788527",
  appId: "1:1007924788527:web:986ed67116e00edc69f5da",
  measurementId: "G-8FJT3DTPXY",
};

export default {
  DEFAULT_TEMPLATE: TEMPLATE_TYPES.NAVBAR,
  FIREBASE_CONFIG: firebaseConfig,
};