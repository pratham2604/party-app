import React, { Component } from 'react'
import { Icon, Menu, Sidebar, Container, Header } from 'semantic-ui-react';
import { Link } from 'react-router-dom'

const routes = [{
  title: 'Home',
  to: '/',
  show: true,
  id: 'home'
}, {
  title: 'Admins',
  to: '/admins',
  show: true,
  id: 'admin',
}, {
  title: 'Vendors',
  to: '/vendors',
  show: true,
  id: 'vendor',
}, {
  title: 'Users',
  to: '/users',
  show: true,
  id: 'user',
}, {
  title: 'Events',
  to: '/events',
  show: true,
  id: 'event',
}]

class SidebarMenu extends Component {
  state = {
    show: true,
  }

  hide = () => {
    this.setState({
      show: false,
    });
  }

  toggle = () => {
    this.setState({
      show: !this.state.show,
    });
  } 

  render() {
    const { children, auth, admins, location, signOut, roles } = this.props;
    const { show } = this.state;
    const currentAdmin = admins.find(admin => admin.email === auth.email);
    const currentUser = Object.assign({}, auth, currentAdmin);
    const currentUserRole = (roles || []).find(role => role.id === currentUser.role);
    const { firstName, email } = currentUser;
    const { pathname } = location;
    const activePath = (routes.find(route => route.to === pathname) || {}).title;
    const access = (currentUserRole || {}).access || [];

    return (
      <Sidebar.Pushable style={{backgroundColor: '#f5f5f5'}}>
        <Sidebar
          as={Menu}
          animation='push'
          inverted
          vertical
          visible={show}
        >
          <Menu.Item as='a'>
            <Header as='h2' style={{color: 'white', padding: '1.5em 0.5em'}}>Party App Admin</Header>
          </Menu.Item>
          <Menu.Item as='a'>
            <Header as='h3' style={{color: 'white'}}>Hi, {firstName}</Header>
            <Header as='h4' style={{color: 'white'}}>({email})</Header>
          </Menu.Item>
          {routes.filter(route => {
            const routeAccess = access.find(accessData => accessData.id === route.id) || {};
            return routeAccess.view || routeAccess.edit || route.id === 'home';
          }).map((route, index) => {
            const { to, title } = route;
            return (
              <Menu.Item as={Link} to={to} key={index} active={pathname === to}>
                {title}
              </Menu.Item>
            )
          })}
        </Sidebar>
        <Sidebar.Pusher style={{minHeight: '100vh'}}>
          <Menu fixed='top' inverted>
            <Container>
              <Menu.Item as='a' header onClick={this.toggle}>
                <Icon name="bars" />
              </Menu.Item>
              <Menu.Item as='a' header>
                {activePath}
              </Menu.Item>
              <Menu.Item as='a' position='right' onClick={signOut}>Sign Out</Menu.Item>
            </Container>
          </Menu>
          <Container fluid style={{ margin: '7em 0' }}>
            {children}
          </Container>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    )
  }
}

export default SidebarMenu;
