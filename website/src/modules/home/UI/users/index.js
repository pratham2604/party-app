import React, { Component } from 'react';
import { Button, Segment } from 'semantic-ui-react';
import { MDBDataTable } from 'mdbreact';
import EditModal from './editModal';

const columns = [{
  label: 'User Status',
  field: 'status',
  sort: 'disabled'
}, {
  label: 'First Name',
  field: 'firstName',
}, {
  label: 'LastName',
  field: 'lastName',
}, {
  label: 'Email',
  field: 'email',
  sort: 'asc',
}, {
  label: 'Mobile',
  field: 'mobile',
}, {
  label: 'Facebook ID',
  field: 'facebookId'
}, {
  label: 'Google ID',
  field: 'googleId'
}, {
  label: 'Password',
  field: 'password',
  sort: 'disabled'
}, {
  label: 'Actions',
  field: 'actions',
  sort: 'disabled'
}];

export default class extends Component {
  render() {
    const { users, editData, routesAccess } = this.props;
    const currentRouteAccess = routesAccess.find(access => access.id === 'user');
    const { view, edit } = currentRouteAccess || {};
    if (!view && !edit) {
      return null;
    }

    const rows = users.map(dataRow => {
      const { isDisabled, email, firstName, lastName, mobileNo, facebookId, googleId, password } = dataRow;
      const toggleData = Object.assign({}, dataRow, {
        isDisabled: !dataRow.isDisabled
      });
      return {
        status: isDisabled ? 'Inactive': 'Active',
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobile: mobileNo,
        password: password ? '******' : '',
        facebookId: facebookId,
        googleId: googleId,
        actions: (
          <div style={{display: 'flex'}}>
            <EditModal canEdit={edit} data={dataRow} editData={editData}/>
            {edit &&
              <Button onClick={() => {editData(toggleData, 'user')}}>
                <Button.Content>{isDisabled ? 'Enable': 'Disable'}</Button.Content>
              </Button>
            }
          </div>
        )
      }
    });
    const tableData = {
      columns,
      rows
    };

    return (
      <Segment>
        <MDBDataTable exportToCSV responsive striped bordered hover data={tableData} />
      </Segment>
    )
  }
}