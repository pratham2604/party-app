import React, { Component } from 'react';
import { Button, Modal, Form, Input, TextArea, Select, Image, Rating, Label, Segment } from 'semantic-ui-react';

export default class extends Component {
  constructor(props) {
    super(props);
    const { data } = props;
    this.state = {
      email: data.email || '',
      firstName: data.firstName || '',
      lastName: data.lastName || '',
      mobileNo: data.mobileNo || '',
      password: data.password || '',
      description: data.description || '',
      cost: data.cost || 0,
      category: data.category || '',
      coverImage: data.coverImage || '',
      questions: data.questions || [],
      profilePic: data.profilePic || '',
      rating: data.rating || 0,
      ratingCount: data.ratingCount || 0,
    };
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value
    });
  }

  onChecked = (e, { name, value}) => {
    this.setState({
      [name]: !this.state[name],
    })
  }

  onQAChange = (questions) => {
    this.setState({
      questions,
    });
  }

  handleSubmit = () => {
    const { data, editData } = this.props;
    const { email, password, firstName, lastName, mobileNo, description, cost, category } = this.state;
    const newData = Object.assign({}, data, { email, password, firstName, lastName, mobileNo, description, cost, category }, {
      currency: '₹',
      cost: parseInt(cost),
    });
    editData(newData, 'vendor');
  }

  render() {
    const { email, firstName, lastName, mobileNo, password, description, cost, category } = this.state;
    const { coverImage, questions, profilePic, rating, ratingCount } = this.state;
    const { categories, canEdit } = this.props;
    const options = categories.map(ctg => {
      return Object.assign({}, {
        key: ctg.id,
        value: ctg.id,
        text: ctg.name
      });
    });

    return (
      <Modal trigger={<Button icon='edit' color='blue'/>} centered={false} closeIcon>
        <Modal.Header>Edit Admin</Modal.Header>
        <Modal.Content>
          <Form onSubmit={this.handleSubmit}>
            <Form.Field>
              <Rating icon='star' defaultRating={rating} maxRating={5} />
              <Label>{`Total Reviews: ${ratingCount}`}</Label>
            </Form.Field>
            <Form.Field>
              {profilePic && <Image src={profilePic} size='small' />}
              <label>Profile Pic</label>
              <Input placeholder='Profile Picture URL' name="profilePic" value={profilePic} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              {coverImage && <Image src={coverImage} size='small' />}
              <label>Cover Image</label>
              <Input placeholder='Cover Image URL' name="coverImage" value={coverImage} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>First Name</label>
              <Input placeholder='First Name' name="firstName" value={firstName} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Last Name</label>
              <Input placeholder='Last Name' name="lastName" value={lastName} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Email</label>
              <Input placeholder='Email' name="email" value={email} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Mobile Number</label>
              <Input placeholder='Mobile Number' name="mobileNo" value={mobileNo} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              <Input placeholder='Password' name="password" value={password} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Description</label>
              <TextArea placeholder='Description' name="description" value={description} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Cost</label>
              <Input type="number" placeholder='Cost' name="cost" value={cost} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Category</label>
              <Select placeholder='Select Category' name="category" defaultValue={category} onChange={this.onChange} options={options}/>
            </Form.Field>
            <Form.Field>
              <label>FAQs</label>
              <Segment>
                <Questions questions={questions} onChange={this.onQAChange} />
              </Segment>
            </Form.Field>
            {canEdit && <Button type='submit'>Submit</Button>}
          </Form>
        </Modal.Content>
      </Modal>
    )
  }
}

class Questions extends Component {
  onChange = (questionAnswer, index) => {
    const newSet = this.props.questions.map(qa => Object.assign({}, qa));
    newSet[index] = questionAnswer;
    this.props.onChange(newSet);
  }

  render() {
    const { questions } = this.props;
    const totalQuestions = questions ? questions.length : 0;
    return (
      <div>
        {questions.map((qa, index) => {
          return (
            <QuestionComponent data={qa} index={index} key={index} onChange={this.onChange} />
          );
        })}
        <QuestionComponent data={{}} index={totalQuestions} onChange={this.onChange}/>
      </div>
    )
  }
}

class QuestionComponent extends Component {
  state = {
    question: (this.props.data|| {}).question || '',
    answer: (this.props.data || {}).answer || '',
    show: false,
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value,
    });
  }

  handleSubmit = () => {
    const newQA = Object.assign({}, {
      question: this.state.question,
      answer: this.state.answer,
    });
    this.props.onChange(newQA, this.props.index);
    this.setState({
      show: false,
      question: '',
      answer: '',
    });
  }

  toggle = () => {
    this.setState({
      show: !this.state.show,
    });
  }

  render() {
    const { question, answer, show } = this.state;
    return (
      <Segment>
        {show ?
          <div>
            <Input placeholder='Question' name="question" value={question} onChange={this.onChange}/>
            <Input placeholder='Answer' name="answer" value={answer} onChange={this.onChange}/>
            <Button.Group>
              <Button positive onClick={this.handleSubmit}>Save</Button>
              <Button.Or />
              <Button negative onClick={this.toggle}>Cancel</Button>
            </Button.Group>
          </div> :
          <div>
            {question ?
              <div>
                <Label>{question}</Label>
                <Button onClick={this.toggle}>Edit</Button>
              </div> :
              <Button onClick={this.toggle}>Add New</Button>
            }
          </div>
        }
      </Segment>
    )
  }
}