import React, { Component } from 'react';
import { Button, Modal, Form, Input, TextArea, Select } from 'semantic-ui-react';

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      firstName: '',
      lastName: '',
      mobileNo: '',
      password: '',
      description: '',
      cost: 0,
      category: '',
    };
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value
    });
  }

  onChecked = (e, { name, value}) => {
    this.setState({
      [name]: !this.state[name],
    })
  }

  handleSubmit = () => {
    const { email, password, firstName, lastName, mobileNo, description, cost, category } = this.state;
    const data = Object.assign({}, { email, password, firstName, lastName, mobileNo, description, cost, category }, {
      currency: '₹',
      cost: parseInt(cost),
    });
    this.props.addData(data, 'vendor');
  }

  render() {
    const { email, firstName, lastName, mobileNo, password, description, cost, category } = this.state;
    const { categories } = this.props;
    const options = categories.map(ctg => {
      return Object.assign({}, {
        key: ctg.id,
        value: ctg.id,
        text: ctg.name
      });
    })

    return (
      <Modal trigger={<Button>Add Vendor</Button>} centered={false} closeIcon>
        <Modal.Header>Add Vendor</Modal.Header>
        <Modal.Content>
          <Form onSubmit={this.handleSubmit}>
            <Form.Field>
              <label>First Name</label>
              <Input placeholder='First Name' name="firstName" value={firstName} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Last Name</label>
              <Input placeholder='Last Name' name="lastName" value={lastName} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Email</label>
              <Input placeholder='Email' name="email" value={email} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Mobile Number</label>
              <Input placeholder='Mobile Number' name="mobileNo" value={mobileNo} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              <Input placeholder='Password' name="password" value={password} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Description</label>
              <TextArea placeholder='Description' name="description" value={description} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Cost</label>
              <Input type="number" placeholder='Cost' name="cost" value={cost} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Category</label>
              <Select placeholder='Select Category' name="category" defaultValue={category} onChange={this.onChange} options={options}/>
            </Form.Field>
            <Button type='submit'>Submit</Button>
          </Form>
        </Modal.Content>
      </Modal>
    )
  }
}