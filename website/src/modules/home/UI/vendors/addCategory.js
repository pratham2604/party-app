import React, { Component } from 'react';
import { Button, Modal, Form, Input, Image, Header, Grid } from 'semantic-ui-react';

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      firstName: '',
      lastName: '',
      mobileNo: '',
      password: '',
      description: '',
      cost: 0,
      category: '',
    };
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value
    });
  }

  onChecked = (e, { name, value}) => {
    this.setState({
      [name]: !this.state[name],
    })
  }

  handleSubmit = () => {
    const { email, password, firstName, lastName, mobileNo, description, cost, category } = this.state;
    const data = Object.assign({}, { email, password, firstName, lastName, mobileNo, description, cost, category }, {
      currency: '₹',
      cost: parseInt(cost),
    });
    this.props.addData(data, 'vendor');
  }

  render() {
    const { categories, editData, addData } = this.props;

    return (
      <Modal trigger={<Button>Manage Categories</Button>} centered={false} closeIcon>
        <Modal.Header>Categories</Modal.Header>
        <Modal.Content>
          <Form>
            {categories.map((category, index) => <Category key={index} data={category} editData={editData}/>)}
            <Category data={{}} newId={categories.length + 1} editData={editData} addData={addData} />
          </Form>
        </Modal.Content>
      </Modal>
    )
  }
}

class Category extends Component {
  state = {
    name: this.props.data.name || '',
    image: this.props.data.image || ''
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value
    });
  }

  handleSubmit = () => {
    const { name, image } = this.state;
    const { id } = this.props.data;
    const data = Object.assign({}, { name, image }, {
      id
    });

    if (id) {
      this.props.editData(data, 'category');
    } else {
      data.id = this.props.newId.toString();
      this.props.addData(data, 'category');
      this.setState({
        name: '',
        image: ''
      });
    }
  }

  render() {
    const { name, image } = this.state;
    return (
      <Form.Field>
        <Grid columns={2} stackable>
          <Grid.Column>
            <Header as='h3'>{name}</Header>
          </Grid.Column>
          <Grid.Column>
            <Image src={image} size='small' />
          </Grid.Column>
        </Grid>
        <Grid columns={2} stackable>
          <Grid.Column>
            <Input placeholder='Category' name="name" value={name} onChange={this.onChange}/>
          </Grid.Column>
          <Grid.Column>
            <Input placeholder='Image Link' name="image" value={image} onChange={this.onChange}/>
          </Grid.Column>
        </Grid>
        <Grid columns={2} stackable style={{marginBottom: '2em'}}>
          <Grid.Column>
            <Button onClick={this.handleSubmit}>Save</Button>
          </Grid.Column>
        </Grid>
      </Form.Field>
    )
  }
}