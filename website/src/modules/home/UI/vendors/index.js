import React, { Component } from 'react';
import { Segment, Button } from 'semantic-ui-react';
import EditModal from './editModal';
import AddModal from './addModal';
import AddCategory from './addCategory';
import { MDBDataTable } from 'mdbreact';

const columns = [{
  label: 'Vendor Status',
  field: 'status',
  sort: 'disabled',
}, {
  label: 'First Name',
  field: 'firstName',
}, {
  label: 'Last Name',
  field: 'lastName',
}, {
  label: 'Email',
  field: 'email',
  sort: 'asc',
}, {
  label: 'Contact',
  field: 'contact',
}, {
  label: 'Category',
  field: 'category'
}, {
  label: 'Actions',
  field: 'actions',
  sort: 'disabled',
}];

export default class extends Component {
  render() {
    const { vendors, addData, editData, routesAccess } = this.props;
    const { categories } = this.props;
    const currentRouteAccess = routesAccess.find(access => access.id === 'vendor');
    const { view, edit } = currentRouteAccess || {};
    if (!view && !edit) {
      return null;
    }

    const rows = vendors.map(dataRow => {
      const { isDisabled, email, firstName, lastName, mobileNo, category } = dataRow;
      const categoryData = categories.find((ctg) => ctg.id === category) || {};
      const toggleData = Object.assign({}, dataRow, {
        isDisabled: !dataRow.isDisabled
      });

      return {
        status: isDisabled ? 'Inactive': 'Active',
        firstName,
        lastName,
        email,
        contact: mobileNo,
        category: categoryData.name,
        actions: (
          <div style={{display: 'flex'}}>
            <EditModal canEdit={edit} data={dataRow} editData={editData} categories={categories} />
            {edit &&
              <Button onClick={() => {editData(toggleData, 'vendor')}}>
                <Button.Content>{isDisabled ? 'Enable': 'Disable'}</Button.Content>
              </Button>
            }
          </div>
        )
      }
    });
    const tableData= {
      columns,
      rows
    };

    return (
      <Segment>
        {edit && <AddModal addData={addData} categories={categories} />}
        {edit && <AddCategory categories={categories} editData={editData} addData={addData} />}
        <MDBDataTable exportToCSV responsive striped bordered hover data={tableData} />
      </Segment>
    )
  }
}