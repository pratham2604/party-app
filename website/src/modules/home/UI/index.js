import React, { Component } from 'react';
import { Segment, Grid, Header, Button } from 'semantic-ui-react';
import { Map as LeafletMap, TileLayer, Marker, Popup } from 'react-leaflet';
import { Link } from 'react-router-dom';

const routes = [{
  title: 'Admins',
  to: '/admins',
  show: true,
  id: 'admin',
}, {
  title: 'Vendors',
  to: '/vendors',
  show: true,
  id: 'vendor',
}, {
  title: 'Users',
  to: '/users',
  show: true,
  id: 'user',
}, {
  title: 'Events',
  to: '/events',
  show: true,
  id: 'event',
}];

export default class extends Component {
  render() {
    const { users, vendors, events, admins, auth, roles } = this.props;
    const activeUsersCount = users.filter(user => !user.isDisabled).length;
    const activeVendorsCount = vendors.filter(vendor => !vendor.isDisabled).length;
    const activeEvents = events.filter(event => !event.isDisabled);
    const activeEventsCount = activeEvents.length;

    const currentAdmin = admins.find(admin => admin.email === auth.email);
    const currentUser = Object.assign({}, auth, currentAdmin);
    const currentUserRole = (roles || []).find(role => role.id === currentUser.role);
    const access = (currentUserRole || {}).access || [];

    return (
      <Segment>
        <Grid columns={3} stackable>
          <Grid.Column>
            <Segment>
              <Header as='h3'>Active User</Header>
              <Header as='h1'>{activeUsersCount || 0}</Header>
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment>
              <Header as='h3'>Active Events</Header>
              <Header as='h1'>{activeEventsCount || 0}</Header>
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment>
              <Header as='h3'>Active Vendors</Header>
              <Header as='h1'>{activeVendorsCount || 0}</Header>
            </Segment>
          </Grid.Column>
        </Grid>
        <Grid columns={2} stackable>
          <Grid.Column>
            <Segment>
              <div className="map-container">
                <LeafletMap
                  center={[50, 10]}
                  zoom={1}
                  maxZoom={10}
                  attributionControl={true}
                  zoomControl={true}
                  doubleClickZoom={true}
                  scrollWheelZoom={true}
                  dragging={true}
                  animate={true}
                  easeLinearity={0.35}
                >
                  <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
                  />
                  {activeEvents.map(event => {
                      const { location, id } = event;
                      const { latitude, longitude, city, fullAddress} = location;
                      return (
                        <Marker position={[latitude, longitude]} key={id}>
                            <Popup>
                                <div>Address: {fullAddress}</div>
                                <div>City: {city}</div>
                                <div>Latitude: {latitude}</div>
                                <div>Longitude: {longitude}</div>
                            </Popup>
                        </Marker>
                      );
                  })}
                </LeafletMap>
              </div>
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment>
              <Header as="h3">
                Check data here
              </Header>
              {routes.filter(route => {
                const routeAccess = access.find(accessData => accessData.id === route.id) || {};
                return routeAccess.view || routeAccess.edit || route.id === 'home';
              }).map(route => {
                const { to, title } = route;
                return (
                  <Button as={Link} to={to} key={title} style={{margin: '1em'}}>
                    {title}
                  </Button>
                )
              })}
            </Segment>
          </Grid.Column>
        </Grid>
      </Segment>
    )
  }
}