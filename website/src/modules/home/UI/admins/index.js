import React, { Component } from 'react';
import { Button, Segment } from 'semantic-ui-react';
import EditModal from './editModal';
import AddModal from './addModal';
import ManageRolesModal from './manageRolesModal';
import { MDBDataTable } from 'mdbreact';

const columns = [{
  label: 'Admin Status',
  field: 'status',
  sort: 'disabled',
}, {
  label: 'First Name',
  field: 'firstName',
}, {
  label: 'Last Name',
  field: 'lastName',
}, {
  label: 'Email',
  field: 'email',
  sort: 'asc',
}, {
  label: 'Mobile',
  field: 'mobile',
}, {
  label: 'Password',
  field: 'password',
  sort: 'disabled',
}, {
  label: 'Role',
  field: 'role',
  sort: 'disabled',
}, {
  label: 'Actions',
  field: 'actions',
  sort: 'disabled',
}];

export default class extends Component {
  render() {
    const { auth, admins, addData, editData, roles, routesAccess } = this.props;
    const currentRouteAccess = routesAccess.find(access => access.id === 'admin');
    const { view, edit } = currentRouteAccess || {};
    if (!view && !edit) {
      return null;
    }

    const rows = admins.map(dataRow => {
      const { isDisabled, email, firstName, lastName, mobileNo, role, password } = dataRow;
      const isCurrentUser = email === auth.email;
      const toggleData = Object.assign({}, dataRow, {
        isDisabled: !dataRow.isDisabled
      });
      const roleName = (roles.find(roleOption => roleOption.id === role) || {}).name;
      return {
        status: isDisabled ? 'Inactive': 'Active',
        firstName,
        lastName,
        email,
        mobile: mobileNo,
        password: password ? '******' : '',
        role: roleName || '',
        actions: (
          <div style={{display: 'flex'}}>
            <EditModal canEdit={edit} data={dataRow} editData={editData} isCurrentUser={isCurrentUser} roles={roles} />
            {edit && 
              <Button onClick={() => {editData(toggleData, 'admin')}}>
                <Button.Content>{isDisabled ? 'Enable': 'Disable'}</Button.Content>
              </Button>
            }
          </div>
        )
      }
    });
    const tableData= {
      columns,
      rows
    };

    return (
      <Segment>
        {edit && <AddModal addData={addData} roles={roles}/>}
        {edit && <ManageRolesModal addData={addData} editData={editData} roles={roles} />}
        <MDBDataTable exportToCSV responsive striped bordered hover data={tableData} />
      </Segment>
    )
  }
}