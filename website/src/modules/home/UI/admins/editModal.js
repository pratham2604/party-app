import React, { Component } from 'react';
import { Button, Modal, Form, Input, Select } from 'semantic-ui-react';

export default class extends Component {
  constructor(props) {
    super(props);
    const { data } = props;
    this.state = {
      email: data.email || '',
      firstName: data.firstName || '',
      lastName: data.lastName || '',
      mobileNo: data.mobileNo || '',
      password: data.password || '',
      role: data.role || '',
    };
  }

  onChange = (e, { name, value }) => {
    this.setState({
      [name]: value
    });
  }

  onChecked = (e, { name, value}) => {
    this.setState({
      [name]: !this.state[name],
    })
  }

  handleSubmit = () => {
    const { email, password, firstName, lastName, mobileNo, role } = this.state;
    const data = Object.assign({}, {uid: this.props.data.uid}, { email, password, firstName, lastName, mobileNo, role });
    this.props.editData(data, 'admin');
  }

  render() {
    const { email, firstName, lastName, mobileNo, password, role } = this.state;
    const { roles, canEdit } = this.props;
    const options = roles.map(roleOption => {
      return Object.assign({}, {
        key: roleOption.id,
        value: roleOption.id,
        text: roleOption.name
      });
    });

    return (
      <Modal trigger={<Button icon='edit' color='blue'/>} centered={false} closeIcon>
        <Modal.Header>Edit Admin</Modal.Header>
        <Modal.Content>
          <Form onSubmit={this.handleSubmit}>
            <Form.Field>
              <label>First Name</label>
              <Input placeholder='First Name' name="firstName" value={firstName} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Last Name</label>
              <Input placeholder='Last Name' name="lastName" value={lastName} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Email</label>
              <Input placeholder='Email' name="email" value={email} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Mobile Number</label>
              <Input placeholder='Mobile Number' name="mobileNo" value={mobileNo} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Password</label>
              <Input placeholder='Password' name="password" value={password} onChange={this.onChange}/>
            </Form.Field>
            <Form.Field>
              <label>Role</label>
              <Select placeholder='Select Role' name="role" defaultValue={role} onChange={this.onChange} options={options}/>
            </Form.Field>
            {canEdit && <Button type='submit'>Submit</Button>}
          </Form>
        </Modal.Content>
      </Modal>
    )
  }
}